import React,{ Suspense } from 'react'
import GetSite from '../src/components/queries/getSite'
import { compose, graphql } from 'react-apollo';
import { Auth } from "aws-amplify";
import { SelectDropContext } from '../src/context/SelectContext'
import  { SelectCurrencyContext, itemsData } from '../src/context/CurrencyContext'
import Routes from './routers';
class MainApp extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            siteType:'',
            courierOption:'',
            authorized:false,
            curr:'',
            currency:itemsData.curr,
            domain:'',
            productCount:itemsData.productCount,
            siteOption:'',
            validTo:'',
            validFrom:''
        }
    }

// static getDerivedStateFromProps(nextProps, nextState){
//     if(nextProps.data.getSite === undefined){
//         return null
//     }
//  if(nextProps.data.getSite != undefined && nextProps.data.getSite !== null && (nextProps.data.getSite.siteType === 'JEWEL_EXPO_ADMIN' ) || 
//     (nextProps.data.getSite.siteType === 'JEWEL_EXPO_BASIC' ) ||
//     (nextProps.data.getSite.siteType === 'MARKETING' ) || (nextProps.data.getSite.siteType == 'B2B_ECOM')
//     ){
//         return{
//             siteType: nextProps.data.getSite.siteType,
//             currency:nextProps.data.getSite.basicSiteSetting.curr,
//         }
//     }else if(nextProps.data.getSite != undefined && nextProps.data.getSite !== null && (nextProps.data.getSite.siteType === 'IJF19_RETAILER')){
//         return{
//             siteType: nextProps.data.getSite.siteType
//         }
//     }

// }

componentDidMount(){
    Auth.currentUserInfo()
            .then((data) => {
                // console.log("user",data)
              if(data.attributes['custom:role'] == undefined){
                localStorage.setItem('role','Mer_Admin')
              }else{
                localStorage.setItem('role',data.attributes['custom:role'])
              }
              localStorage.setItem('ph_no',data.attributes['phone_number'])
              localStorage.setItem('siteId',data.attributes['custom:siteId'])
            })
             
            .catch(err => console.log(err));
}
render(){
  //  console.log("main App",this.props)
//   if(this.state.siteType === 'JEWEL_EXPO_ADMIN'){
        return (
            <Suspense fallback={null}>
                    <SelectCurrencyContext.Provider
                        value={{
                        curr:this.state.currency,
                        siteType:this.state.siteType,
                        siteOption:this.state.siteOption
                    }}
                    >
                        <Routes {...this.props} opt={this.context}/>              
                    </SelectCurrencyContext.Provider>
            </Suspense>
        );      
    // }

    //  else {
    //     return null
    // }
}
}
MainApp.contextType = SelectDropContext
const WrapApp = compose(
    // withRouter,
    graphql(GetSite,{
        options: props => ({
            fetchPolicy: "network-only"
        })
    })
   
  )(MainApp)
export default WrapApp