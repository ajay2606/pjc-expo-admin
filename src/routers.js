// import React from 'react'
// import { Route, BrowserRouter as Router } from "react-router-dom";
// import Userlist from '../src/components/userlist'
// import Marchant from '../src/components/manageMarchant'
// import ExhibitorList from '../src/components/exhibitorIndex'
// import { SelectDropContext } from '../src/context/SelectContext'
// import App from './App'
// const Routers =(props)=>(
//     <Router>
//         <React.Fragment>
//             {/* <Route exact path="/" component={ExhibitorList} /> */}
//             <Route exact path="/" render={() => { 
//                     return (
//                         <SelectDropContext.Consumer>
//                         {opt => {
//                             return <ExhibitorList {...props} opt={opt} /> 
//                         }}
//                         </SelectDropContext.Consumer>
//                     )
//                     }} 
//                 />
//             <Route path="/userlist" component={Userlist} />
//             <Route path="/marchant" component={Marchant} />
//         </React.Fragment>
//     </Router>
// );
// export default Routers
import React, { Fragment } from "react";
import { Route, BrowserRouter as Router } from "react-router-dom";
import AuthHOC from "./AuthHOC";
import ExhibitorList from '../src/components/exhibitorIndex'
import JCreateExhibitor from '../src/components/createExhibitor'
import CreateIJWExhibitor from '../src/components/IJWExhibitor/createIJWExhibitor'
import VisitorApproval from '../src/components/IJWExhibitor/jExpo&ijw19VisitorApproval'
import AdminVisitor from '../src/components/visitorTable'

import { SelectDropContext } from '../src/context/SelectContext'



const JewelAdminRoute = (props)=>{
    //console.log("props",props)
    return(
        <Router >
            <Fragment>
            <Route exact path="/" render={() => { 
                    return (
                        <SelectDropContext.Consumer>
                        {opt => {
                            return <ExhibitorList {...props} opt={opt} /> 
                        }}
                        </SelectDropContext.Consumer>
                    )
                    }} 
                />
                <Route exact path="/jAdminExhibitorList" render={() => { 
                    return (
                        <SelectDropContext.Consumer>
                        {opt => {
                            return <ExhibitorList {...props} opt={opt} /> 
                        }}
                        </SelectDropContext.Consumer>
                    )
                    }} 
                />
                <Route exact path="/jAdminVisitorApproval" render={() => { 
                    return (
                             <VisitorApproval {...props}  />
                    )  }} />
                  <Route exact path="/jAdminCreateExhibitor" component={AuthHOC(JCreateExhibitor)}/>
                  <Route exact path="/jAdminCreateIJWExh" component={AuthHOC(CreateIJWExhibitor)}/>
                  <Route exact path="/jAdminVisitorList" render={() => { 
                    return (
                        
                             <AdminVisitor {...props} />)
                          
                }}/>
               
            </Fragment>
        </Router>
    )
}

export default JewelAdminRoute