import React, { Component } from 'react';
import { Auth } from "aws-amplify";


/**
 * Higher-order component (HOC) to wrap restricted pages
 */
export default function(ComposedClass) {
    class AuthCustom extends Component {

        constructor(props){
            super(props)
            this.state = {
              customSiteId:'',
              username:''
            }
            this.getSiteId()
        }

        static getDerivedStateFromProps(nextProps, nextState){
            if(nextState.customSiteId != ''){
                return null
            }
            return{
                customSiteId:nextState.customSiteId
            }
        }

        getSiteId = async () => {
            await Auth.currentUserInfo()
            .then((data) => {
                this.setState({
                    customSiteId:data.attributes['custom:siteId'],
                    customFqdn:data.attributes['custom:fqdnMut'],
                    username:data.username
                })
            })
            .catch(err => console.log(err));
        }
        
        render() {
            const { customSiteId,username } = this.state
            // if(customSiteId == "" || username == ""){
            //     return null
            // }else{
                return <ComposedClass {...this.props} customSiteId={customSiteId} username={username} />
            // }
        }
    }
    return AuthCustom
}