import gql from 'graphql-tag'

export default gql`
query getVisitorsByExh( $exhName: String! $status: String! $skips: Int $limits: Int){
  getVisitorsByExh(exhName: $exhName status: $status  skips: $skips limits: $limits){
        id
    basicVisitor{
      fName
      lName
      companyName
      address{
        city
      }
    }
    ijw19{
      activatedOn
      regNo
    }
    jewExpo{
      activatedOn
      regNo
    }
  }
}
`
