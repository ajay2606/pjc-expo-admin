import gql from 'graphql-tag'
export default gql`
  query getJewelExhibitors($exhName: String!) {
    getJewelExhibitors(exhName: $exhName) {
      id
      companyName
      ownerName
      salesHead
      email
      compTel
      enabled
      otherContact
      moreProductDetails
      target_states
      productsDealingIn
      productsMelting
      branchesIn
      slugCompName
      exhType
      machinery
      allied
      businessAddress {
        addressLineOne
        addressLineTwo
        city
        state
        zip
        country
      }
      latlng {
        lat
        lng
      }
      logo {
        bucket
        region
        key
      }
      ijw19 {
        exhName
        status
        activatedOn
      }
      jewExpo {
        siteId
        fqdn
        subdomain
        domain
        siteType
      }
    }
  }
`;
  