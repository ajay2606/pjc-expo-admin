import gql from 'graphql-tag'

export default gql`
  query getJewelExhibitorByMob($mob: String!, $exhName: String!) {
    getJewelExhibitorByMob(mob: $mob, exhName: $exhName) {
      id
      companyName
      ownerName
      salesHead
      email
      compTel
      otherContact
      moreProductDetails
      target_states
      productsDealingIn
      productsMelting
      branchesIn
      createdAt
      slugCompName
      exhType
      machinery
      allied
      businessAddress {
        addressLineOne
        addressLineTwo
        city
        state
        zip
        country
      }
      latlng {
        lat
        lng
      }
      logo {
        bucket
        region
        key
      }
    }
  }
`;

   
   