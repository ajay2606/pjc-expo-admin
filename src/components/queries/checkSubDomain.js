import gql from 'graphql-tag'
export default gql`
query checkSubdomain($subdomain: String!){
    checkSubdomain(subdomain: $subdomain){
      domain
      siteId
      typeValue
    }
}
`
