import gql from 'graphql-tag'
export default gql`
query getIjfJewelExhibitorByMob($mob: String!){
    getIjfJewelExhibitorByMob(mob: $mob){
        id
        companyName,
        ownerName,
        ownMob
        email,
        tel,
        latlng{
            lat
            lng
        },
        address,
        area,
        city,
        state,
        zip,
        productsDealingIn,
        moreProductDetails,
        zone,
        noOfCoupon,
        exhName,
        couponSeries
    }
  }
`
