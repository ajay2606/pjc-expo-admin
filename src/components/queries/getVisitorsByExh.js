import gql from 'graphql-tag'

export default gql`
query getVisitorsByExh( $exhName: String! $status: String! $skips: Int $limits: Int){
  getVisitorsByExh(exhName: $exhName status: $status  skips: $skips limits: $limits){
        id
    basicVisitor{
      fName
      lName
      companyName
      designation
      telephone
      gstNo
      email
      gender
      address{
        addressLineOne
        addressLineTwo
        city
        state
        zip
        country
      }
      visitorPhoto{
        bucket
        region
        key
      }
      docPhoto{
        bucket
        region
        key
      }
    }
    ijw19{
      exhName
      productsInterestedIn
      status
      activatedOn
      regNo
    }
    jewExpo{
      exhName
      productsInterestedIn
      status
      activatedOn
      regNo
    }
  }
}
`
