import gql from 'graphql-tag'
export default gql`
query getIjfJewelExhibitors($exhName: String!){
  getIjfJewelExhibitors(exhName: $exhName){
        id
    companyName,
    ownerName,
    ownMob
    email,
    tel,
    latlng{
      lat
      lng
    },
    address,
    city,
    state,
    zip,
    productsDealingIn,
    moreProductDetails,
    zone,
    noOfCoupon,
    exhName
    area         
  }
}
  `
  