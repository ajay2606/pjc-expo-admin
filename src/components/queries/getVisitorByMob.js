import gql from 'graphql-tag'
export default gql`
query getVisitorByMob( $mob: String!  $exhName: String!){
    getVisitorByMob(mob: $mob exhName: $exhName ){
      id
      basicVisitor{
        fName
        lName
        companyName
        designation
        telephone
        email
        gender
        address{
          addressLineOne
          addressLineTwo
          city
          state
          zip
          country
        }
        visitorPhoto{
          bucket
          region
          key
        }
        docPhoto{
          bucket
          region
          key
        }
      }
      jewExpo{
        exhName
        productsInterestedIn
        status
        activatedOn
        regNo
      }
      ijw19{
        productsInterestedIn
        status
        activatedOn
        regNo
      }
    }
}
`