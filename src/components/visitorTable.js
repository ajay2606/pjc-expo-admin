import React from 'react'
import { Query,  withApollo } from 'react-apollo';
import { Select, Spin } from 'antd';
import _ from 'lodash';
import App from '../App'
import GetVisitorByExh from '../components/queries/getVisitorsByExh'

import Visitor from '../components/visitorByExh'

// import { graphql, compose,withApollo } from 'react-apollo';

const exhName = [
  {value:"PJC",label:"PJC"},
  {value:"jewExpo",label:"Jewel Expo"},
  // {value:"ijw19",label:"IJW 19"},
  // {value:"ijf19",label:"IJF 19"}
]
let skip = 0
const limit = 20
class VisitorTable extends React.Component{

    constructor(props){
        super(props)
        this.state={
            selectedValue:"PJC",
            visitorData:[]
        }
    }

    render(){
      const exhNames = (
        <div>
               <b>Select Exhibition:-</b>&nbsp;
                <Select
                  // defaultValue={this.props.location.state != undefined && this.props.location.state.exhiName=="ijf19" ? "ijf19" : "jewExpo"}
                  defaultValue={this.props.opt}
                  style={{width:"35%"}}
                  onChange={this.props.onChangeSelect}
                >
                    {exhName.map((c)=>{
                      return(
                        <Select.Option key={c.value} value={c.value} >{c.label}</Select.Option>
                      )
                    })}
                </Select>
        </div>
      );
        return(
          <App  header={'Manage Visitor'} compHeader={exhNames} opt={this.props.opt}>
          <Query query={GetVisitorByExh} variables={{
            exhName:this.props.opt,
            skips: skip,
            status:"APPROVED",
            limits: limit}}
            fetchPolicy="network-only">
            {(
              { loading, error, data, refetch }) => {
              if (loading) return(
                <div style={{ textAlign: "center" }}>
                  <Spin size="large"></Spin>
                </div>
              );
              if (error) return `Error!: ${error}`;
                  return(
                    <Visitor visitorsData={data.getVisitorsByExh} opt={this.props.opt}/>
                  )
            }}
           </Query>
           </App>
                
        )
    }
}
export default withApollo(VisitorTable)