import React, {lazy} from 'react'
import App from '../App'
import { Form, Button, Breadcrumb, Row,Col , Input,Icon,  Modal, Divider,Select } from 'antd';
import { Link } from 'react-router-dom';
import {withApollo} from 'react-apollo';
import { withRouter } from 'react-router-dom';
import GetExibitorMobileNum from '../components/queries/getExhibitorMob'
import GetIJFExibitorMobileNum from '../components/queries/getIjfExhibitorByMob'
import JewelExpoTable from '../components/jewelExpoTable'
import IJFTable from '../components/jjfTable'
import Highlighter from 'react-highlight-words';
const exhName = [
  {value:"PJC",label:"PJC"},
  {value:"jewExpo",label:"Jewel Expo"},
  {value:"ijf19",label:"IJF 19"},
  {value:"ijw19",label:"IJW 19"}
]

const FormItem = Form.Item;
class ExhibitorList extends React.Component{

        constructor(props){
            super(props)
            this.state = {
                visible: false,
                mobileNumber:"",
                exihibitorData:[],
                loading:false,
                viewVisible:false,
                record:'',
                prevOpt:'',
                str:'',
                dataExh:'',
                ijfExh:[]
            }
        }

        showModal = () => {
            this.setState({
              visible: true,
            });
        }

        handleOk = (e) => {
          this.setState({
            visible: false,
          });
        }
        
        handleCancel = (e) => {
          this.props.form.resetFields(['mob'])
          this.setState({
            visible: false,
          });
        }

        enterLoading = () =>{
            this.setState({loading:true})
        }

        handleClick = (mob)=>{
          this.props.opt == "ijf19" ? 
                  this.props.client.query({
                    query:GetIJFExibitorMobileNum,
                    fetchPolicy: 'network-only' ,
                    variables: {
                      mob:this.state.mobileNumber? `${this.props.opt}::${this.state.mobileNumber.trim()}`:`${this.props.opt}::${mob.trim()}`
                    }
                  }).then(({data})=>{
                      this.props.history.push('/jAdminCreateIJFExhibitor',{exhibitorData:data.getIjfJewelExhibitorByMob,mobileNumber:this.state.mobileNumber})
                  }).catch(err => {
                      this.setState({err: JSON.stringify(err) })
                      console.log(`Error : ${JSON.stringify(err)}`)
                  })
          
            :

            this.props.client.query({
                query:GetExibitorMobileNum,
                fetchPolicy: 'network-only' ,
                variables: {
                  mob:this.state.mobileNumber? this.state.mobileNumber.trim():mob.trim(),
                  exhName:this.props.opt
                }
                }).then(({data})=>{
                  console.log("data",data)
                  // this.props.opt == "jewExpo" ? 
                    this.props.history.push('/jAdminCreateExhibitor',{exhibitorData:data.getJewelExhibitorByMob,mobileNumber:this.state.mobileNumber})
                    // this.props.history.push('/jAdminCreateIJWExh',{exhibitorData:data.getJewelExhibitorByMob,mobileNumber:this.state.mobileNumber})
                }).catch(err => {
                    this.setState({err: JSON.stringify(err) })
                    console.log(`Error : ${JSON.stringify(err)}`)
                })
        }
          handleChange = (e)=>{
            this.setState({mobileNumber:e.target.value})
          }
          onView = (record) =>{
            this.setState({viewVisible:!this.state.viewVisible,
            record:record})
          }
          
        handleViewCancel = (e)=>{
            this.setState({viewVisible:!this.state.viewVisible})
        }

        
        handleClickmap = (e,record)=>{
          window.open(`https://www.google.com/maps/search/?api=1&query=${record.lat},${record.lng}`)
        }

        
        handleSearch = (selectedKeys, confirm) => {
          confirm();
          this.setState({ searchText: selectedKeys[0] });
        }

        handleReset = (clearFilters) => {
          clearFilters();
          this.setState({ searchText: '' });
        }

        getColumnSearchProps = (dataIndex) => ({
          filterDropdown: ({
            setSelectedKeys, selectedKeys, confirm, clearFilters,
          }) => (
            <div style={{ padding: 8 }}>
              <Input
                ref={node => { this.searchInput = node; }}
                placeholder={`Search ${dataIndex}`}
                value={selectedKeys[0]}
                onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
                style={{ width: 188, marginBottom: 8, display: 'block' }}
              />
              <Button
                type="primary"
                onClick={() => this.handleSearch(selectedKeys, confirm)}
                icon="search"
                size="small"
                style={{ width: 90, marginRight: 8 }}
              >
                Search
              </Button>
              <Button
                onClick={() => this.handleReset(clearFilters)}
                size="small"
                style={{ width: 90 }}
              >
                Reset
              </Button>
            </div>
          ),
          filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
          onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
          onFilterDropdownVisibleChange: (visible) => {
            if (visible) {
              setTimeout(() => this.searchInput.select());
            }
          },
          render: (text) => (
            <Highlighter
              highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
              searchWords={[this.state.searchText]}
              autoEscape
              textToHighlight={text.toString()}
            />
          ),
        })

    render(){
      // console.log("opt",this.props.opt)
      let exhibitor = this.state.record
      const { getFieldDecorator } = this.props.form;
      const formItemLayout = {
          labelCol: { span: 6 },
          wrapperCol: { span: 16 }
        };
      const exhNames = (
         <div>
           {/* <b>Select Exhibition:</b> &nbsp;
            <Select
              // defaultValue={this.props.location.state != undefined && this.props.location.state.exhiName=="ijf19" ? "ijf19" : "jewExpo"}
              defaultValue={this.props.opt}
              style={{width:"35%"}}
              onChange={this.props.onChangeSelect}
            >
              {exhName.map((c)=>{
                return(
                  <Select.Option key={c.value} value={c.value} >{c.label}</Select.Option>
                )
              })}
            </Select> */}
            </div> 
        );
              //console.log("In exhibitorIndex");
        return(
            <App header="Manage Exhibitor" compHeader={exhNames}>
             <Row gutter={16} style={{marginTop:"20px"}}> 
              <Col xs={{span:24}} sm={{span:24}} md={{span:23}} lg={{span:23}}>
                <Breadcrumb style={{marginBottom:"20px"}}>
                <Link to="/">
                    <Breadcrumb.Item>Home</Breadcrumb.Item>
                    </Link>
                    <Breadcrumb.Item>Exhibitor</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
              <Col xs={{span:24}} sm={{span:24}} md={{span:1}} lg={{span:1}}>
                  <Button type="default" onClick={this.showModal} style={{ marginBottom: 16, float: "right", cursor: "pointer", background:"#389e0d", color:"#fff" }} >
                      Create Exhibitor
                  </Button>
              </Col>
            </Row>
          <Form>
          <Modal
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={null}
        >
        <FormItem label="Mobile Number" {...formItemLayout} >
        {getFieldDecorator('mob', {
                        validateTrigger: ['onChange', 'onBlur'],
                        rules: [
                        //     {
                        // required: true,
                        // message: "Please enter mobile number.",
                        // },
                        {
                            validator:(rule, value, cb)=>{
                                if(isNaN(value.trim()) || value.trim().length != 10){
                                  cb('Please enter 10 digit number only')
                                }
                                cb()
                            }}
                    ],
                    })(
        <Input placeholder="Mobile Number" onChange={this.handleChange} style={{ width: '70%', marginRight: 8 }} />)}
        </FormItem>
            <div> 
        <Row>
            <Col xs={{span:24}} sm={{span:24}} md={{span:20}} lg={{span:20}}>
            <Button style={{marginRight:"8px",float: "right", clear:"left", background:"#389e0d", color:"#fff"}} disabled={this.state.mobileNumber.length != 10 ? true :false} onClick={this.handleClick} type="default">Check</Button>
            </Col>
            <Col xs={{span:24}} sm={{span:24}} md={{span:2}} lg={{span:2}}>
            <Button type="default" onClick={this.handleCancel}  style={{background:"#f44336", color:"#fff"}}>Cancel</Button>
            </Col>
        </Row>
            </div>
        </Modal>
        {this.props.opt== "ijf19" && 
            <IJFTable opt={this.props.opt}/>
         }
                    
        { (this.props.opt == "jewExpo" || this.props.opt == "ijw19") &&
            <JewelExpoTable opt={this.props.opt}/>}
           
         <Modal
         className="exhibitorModal"
                 title= "Exhibitor Details"
                 footer={null}
                 width="55%"
                 visible={this.state.viewVisible}
                 onCancel={this.handleViewCancel}
               >
               <Row gutter={16}>
                <Col span={6} style={{fontWeight:'bold'}} >Owner Name:</Col>
                <Col span={16} >{exhibitor.ownerName}</Col>
              </Row>
              <Row gutter={16}>
                <Col span={6} style={{fontWeight:'bold'}}>Company Name:</Col>
                <Col span={16} >{exhibitor.companyName}</Col>
              </Row>
              <Row gutter={16}>
                <Col span={6} style={{fontWeight:'bold'}}>Owner Number:</Col>
                <Col span={16} >{exhibitor.id}</Col>
              </Row>
             
              <Row gutter={16}>
                <Col span={6} style={{fontWeight:'bold'}}>Sales Head:</Col>
                <Col span={16} >{exhibitor.salesHead}</Col>
              </Row>
              <Row gutter={16}>
                <Col span={6} style={{fontWeight:'bold'}}>Other Contact:</Col>
                <Col span={16} >{exhibitor.otherContact != null?exhibitor.otherContact : '-'}</Col>
              </Row>
              <Row gutter={16}>
                <Col span={6} style={{fontWeight:'bold'}}>Location on map:</Col>
                <Col span={16}>
                <div><img src="https://cdn.pixabay.com/photo/2016/03/22/04/23/map-1272165__340.png"  onClick={(e)=>this.handleClickmap(e,exhibitor.latlng)} style={{cursor:"pointer",width:"30px"}} /> </div>
                </Col>
              </Row>
              <Row gutter={16} >
                <Col span={6} style={{fontWeight:'bold'}}>Products Interested In:</Col>
                <Col span={16}>{exhibitor.productsDealingIn!=undefined && exhibitor.productsDealingIn.map((val, i)=>{
                  return(
                      <span style={{ textTransform: 'capitalize'}}>
                        {val.replace(/(?!^)\_/g, ' ').toLowerCase()}
                        {val.length - 1 != i && ', '}
                      </span>
                  )
                })}</Col>
              </Row>
              <Row gutter={16} >
                <Col span={6} style={{fontWeight:'bold'}}>Product Melting:</Col>
                <Col span={16}>{exhibitor.productsMelting!=undefined && exhibitor.productsMelting.length != 0 ?  exhibitor.productsMelting.map((val, i)=>{
                  return(
                      <span style={{ textTransform: 'capitalize'}}>
                        {val.replace(/(?!^)\_/g, ' ').toLowerCase()}
                        {val.length - 1 != i && ', '}
                      </span>
                  )
                }):'-'}</Col>
              </Row>
              <Row gutter={16} >
                <Col span={6} style={{fontWeight:'bold'}}>Target States:</Col>
                <Col span={14}>{exhibitor.target_states!=undefined && exhibitor.target_states.map((val, i)=>{
                  return(
                      <span style={{ textTransform: 'capitalize'}}>
                        {val.replace(/(?!^)\_/g, ' ').toLowerCase()}
                        {val.length - 1 != i && ', '}
                      </span>
                  )
                })}</Col>
              </Row>
              
              {exhibitor.jewExpo!=undefined && exhibitor.jewExpo.map((val, i)=>{
                if(val.siteType == 'JEWEL_EXPO_BASIC'){
                  return(
                    <Row>
                    <Divider style={{fontSize:"20px"}}>Sites</Divider>
                      <Col span={4} style={{fontWeight:'bold'}}>Basic:</Col>
                      <Col span={16} style={{ textTransform: 'capitalize'}}>
                      <a href={`https://${val.subdomain}.jewelexpo.in/`} target="_blank">
                      {`${val.subdomain}.jewelexpo.in`}
                      </a>
                      </Col>
                    </Row>
                    
                    )
                  }
                  if(val.siteType == 'JEWEL_EXPO_PREMIUM'){
                    return(
                    <Row>
                      <Col span={4} style={{fontWeight:'bold'}}>Premium:</Col>
                      <Col span={16} style={{ textTransform: 'capitalize'}}>
                      <a href={`https://${val.subdomain}.jewelexpo.in/`} target="_blank">
                      {`${val.subdomain}.jewelexpo.in`}
                      </a>
                      </Col>
                    </Row>
                    
                    )
                  }
                })}


              {/* <Row gutter={16}>
                <Col span={6} style={{fontWeight:'bold'}}>Basic</Col>
                <Col span={16}>
                {exhibitor.jewExpo!=undefined && exhibitor.jewExpo.map((val, i)=>{
                  if(val.siteType == 'JEWEL_EXPO_BASIC'){
                    return(
                      <a href={`https://${val.subdomain}.jewelexpo.in/`} target="_blank">
                      {val.subdomain}
                      </a>
                    )
                  }
                })}
                </Col>
              </Row>
              
              <Row gutter={16}>
                <Col span={6} style={{fontWeight:'bold'}}>Premium</Col>
                <Col span={16}>
                {exhibitor.jewExpo!=undefined && exhibitor.jewExpo.map((val, i)=>{
                  if(val.siteType == 'JEWEL_EXPO_PREMIUM'){
                    return(
                      <a href={`https://${val.subdomain}.jewelexpo.in/`} target="_blank">
                      {val.subdomain}
                      </a>
                    )
                  }
                  
                })}
               
                </Col>
              </Row> */}
              {/* <Row gutter={16} >
                <Col span={10} style={{fontWeight:'bold'}}>Notes:</Col>
                <Col span={12} >{app.notes}</Col>
              </Row>
              <Row gutter={16} >
                <Col span={10} style={{fontWeight:'bold'}}>Title:</Col>
                <Col span={12} >{app.title}</Col>
              </Row>                 */}
              </Modal>
        </Form>
            </App>
        )
    }
}
const WrapExhibitorList = Form.create()(ExhibitorList);


export default withApollo(withRouter(WrapExhibitorList))