import { Alert, Breadcrumb, Button, Card, Col, Divider, Form, Icon, Input, Modal, Radio, Row, Select, Spin, Switch, Table, Upload, message } from 'antd';
import { compose, graphql, withApollo } from 'react-apollo';
import _ from 'lodash';
import App from '../App'
import CheckSubDomain from '../components/queries/checkSubDomain'
import CreateJewelExhibitor from '../components/mutations/createDigiExpoExhibitor'
import createDigiExpoExhibitor from '../components/mutations/createDigiExpoExhibitor'
import { Link } from 'react-router-dom';
import MapSearchBox from './mapSearchBox'
import React from 'react'
import UpdateBasicInfoExhibitor from '../components/mutations/updateBasicInfoExhibitor'
import UpdateJewelExhibitor from '../components/mutations/updateJewelExihibitor'
import slugify from 'slugify';

const FormItem = Form.Item;
const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const bucket = "pjc-expo-prod"
const s3_Region = "ap-south-1"
const S3_Url = 'https://s3.ap-south-1.amazonaws.com/pjc-expo-prod/'
const country = [
    { value:'IND', label:'IND'},
    { value:'UK', label:'UK'},
    { value:'USA', label:'USA'},
]

const ExhType = [
  { value: "JEWELLER", label: "JEWELLER" },
  { value: "MACHINERY", label: "MACHINERY" },
  { value: "ALLIED", label: "ALLIED" },
];

const productDealing = [
   {value:'GOLD_CHAINS', label:'GOLDCHAINS'},
   {value:'MANGALSUTRAS',label:'MANGALSUTRAS'},
    {value:'BANGLES',label:'BANGLES'},
    {value:'BRACELETS',label:'BRACELETS'},
    {value:'CASTING_JEWELLERY',label:'CASTING JEWELLERY'},
    {value:'CZ_DIAMOND_JEWELLERY',label:'CZ DIAMONDJEWELLERY'},
    {value:'ANTIQUE_JEWELLERY',label:'ANTIQUE JEWELLERY'},
    {value:'TEMPLE_JEWELLERY',label:'TEMPLE JEWELLERY'},
    {value:'KOLKATA_JEWELLERY',label:'KOLKATA JEWELLERY'},
    {value:'IMPORTED_ITALIAN_JEWELLERY',label:'IMPORTED ITALIAN JEWELLERY'},
    {value:'LIGHTWEIGHT_JEWELLERY',label:'LIGHTWEIGHT JEWELLERY'},
    {value:'DESIGNER_ANTIQUE_JEWELLERY',label:'DESIGNER ANTIQUE JEWELLERY'},
    {value:'PLAIN_GOLD_JEWELLERY',label:'PLAIN GOLD JEWELLERY'},
    {value:'REAL_DIAMOND_JEWELLERY',label:'REAL DIAMOND JEWELLERY'},
    {value:'PLATINUM_JEWELLERY',label:'PLATINUM JEWELLERY'},
    {value:'SILVER_JEWELLERY_925',label:'SILVER JEWELLERY 925'},
    {value:'SILVER_ARTICLES',label:'SILVER ARTICLES'},
    {value:'LOOSE_DIAMONDS',label:'LOOSE DIAMONDS'},
    {value:'DIAMOND_JEWELLERY',label:'DIAMOND JEWELLERY'},
    {value:'GOLD_JEWELLERY',label:'GOLD JEWELLERY'},
    {value:'JADAU_JEWELLERY',label:'JADAU JEWELLERY'},
    {value:'MACHINERY_ALLIED_SECTION',label:'MACHINERY ALLIED SECTION'},
    {value:'GOLD_MOUNTINGS',label:'GOLD MOUNTINGS'},
    {value:'TURKISH_JEWELLERY',label:'TURKISH JEWELLERY'},
    {value:'NOSE_PINS',label:'NOSE PINS'},
    {value:'MACHINERY',label:'MACHINERY'},
    {value:'TOOLS',label:'TOOLS'},
    {value:'PUBLICATIONS',label:'PUBLICATIONS'},
    {value:'EDUCATION_INSTITUTE',label:'EDUCATION INSTITUTE'},
    {value:'LABORATORY',label:'LABORATORY'}
]

const productDealingMachinery = [
  { value: "MACHINERY", label: "MACHINERY" },
  { value: "PACKAGING", label: "PACKAGING" },
  { value: "TOOLS", label: "TOOLS" },
];

const productDealingAllied = [
  { value: "INSURANCE", label: "INSURANCE" },
  { value: "LOGISTICS", label: "LOGISTICS" },
  { value: "STALL_DESIGNER_FABRICATOR", label: "STALL DESIGNER & FABRICATOR" },
  { value: "VISUAL_MERCHANDISER", label: "VISUAL MERCHANDISER" },
  { value: "ARCHITECT", label: "ARCHITECT" },
  { value: "INTERIOR_DESIGNER", label: "INTERIOR DESIGNER" },
  { value: "STALL_DESIGNER", label: "STALL DESIGNER" },
  { value: "DIGITAL_MARKETING_AGENCY", label: "DIGITAL MARKETING AGENCY" },
  { value: "ADVERTISING_AGENCY", label: "ADVERTISING AGENCY" },
  { value: "CREATIVE_DESIGNING_AGENCY", label: "CREATIVE DESIGNING AGENCY" },
  { value: "PRODUCTION_AGENCY", label: "PRODUCTION AGENCY" },
  { value: "MODELLING_AGENCY", label: "MODELLING AGENCY" },
  { value: "HOST_HOSTESS_AGENCY", label: "HOST / HOSTESS AGENCY" },
  { value: "PRODUCT_PHOTO_SHOOT_AGENCY", label: "PRODUCT PHOTO-SHOOT AGENCY" },
  { value: "INVENTORY_MANAGEMENT_SOFTWARE", label: "INVENTORY MANAGEMENT SOFTWARE" },
  { value: "WEDDING_PLANNERS", label: "WEDDING PLANNERS" },
  { value: "EVENT_MANAGEMENT", label: "EVENT MANAGEMENT" },
  { value: "BANKING", label: "BANKING" },
  { value: "EDUCATION_INSTITUTE", label: "EDUCATION INSTITUTE" },
  { value: "BUSINESS_CONSULTANT", label: "BUSINESS CONSULTANT" },
  { value: "WEBSITE_APP_DEVELOPERS", label: "WEBSITE & APP DEVELOPERS" },
  { value: "HALLMARKING_INSTITUTE", label: "HALLMARKING INSTITUTE" },
  { value: "GEMSTONE_GRADING_LABORATORY", label: "GEMSTONE GRADING LABORATORY" },
  { value: "PACKAGING_DISPLAY", label: "PACKAGING & DISPLAY" },
  { value: "FASHION_CHOREOGRAPHER", label: "FASHION CHOREOGRAPHER" },
];

const state = [
    {value:'JAMMU_KASHMIR',label:'JAMMU KASHMIR'},
    {value:'HIMACHAL_PRADESH',label:'HIMACHAL PRADESH'},
    {value:'PUNJAB',label:'PUNJAB'},
    {value:'DELHI',label:'DELHI'},
    {value:'RAJASTHAN',label:'RAJASTHAN'},
    {value:'UTTARAKHAND',label:'UTTARAKHAND'},
    {value:'UTTAR_PRADESH',label:'UTTAR PRADESH'},
    {value:'WEST_BENGAL',label:'WEST BENGAL'},
    {value:'BIHAR',label:'BIHAR'},
    {value:'JHARKHAND',label:'JHARKHAND'},
    {value:'ODISHA',label:'ODISHA'},
    {value:'ASSAM',label:'ASSAM'},
    {value:'ARUNACHAL_PRADESH',label:'ARUNACHAL PRADESH'},
    {value:'MEGHALAYA',label:'MEGHALAYA'},
    {value:'TRIPURA',label:'TRIPURA'},
    {value:'MIZORAM',label:'MIZORAM'},
    {value:'MANIPUR',label:'MANIPUR'},
    {value:'NAGALAND',label:'NAGALAND'},
    {value:'MAHARASHTRA',label:'MAHARASHTRA'},
    {value:'GUJARAT',label:'GUJARAT'},
    {value:'GOA',label:'GOA'},
    {value:'CHHATTISGARH',label:'CHHATTISGARH'},
    {value:'MADHYA_PRADESH',label:'MADHYA PRADESH'},
    {value:'ANDHRA_PRADESH',label:'ANDHRA PRADESH'},
    {value:'HARYANA',label:'HARYANA'},
    {value:'KERALA',label:'KERALA'},
    {value:'SIKKIM',label:'SIKKIM'},
    {value:'TAMIL_NADU',label:'TAMIL NADU'},
    {value:'TELANGANA',label:'TELANGANA'},
    {value:'KARNATAKA',label:'KARNATAKA'},
]

const productMelting = [
    {value:'MELTING_75',label:'MELTING 75'},
    {value:'MELTING_84',label:'MELTING 84'},
    {value:'MELTING_92',label:'MELTING 92'},
    
]

class CreateExhibitor extends React.Component{
    constructor(props){
        super(props);
        this.state={
            visible:false,
            visibleMap: false,
            lng: this.props.location.state != undefined && this.props.location.state.exhibitorData!=null &&  this.props.location.state.exhibitorData.latlng != null ?  this.props.location.state.exhibitorData.latlng.lng:'',
            previewVisible: false,
            PreviewVisible:false,
            PreviewImage: '',
            previewImage: '',
            lat: this.props.location.state != undefined && this.props.location.state.exhibitorData!=null && this.props.location.state.exhibitorData.latlng != null ?  this.props.location.state.exhibitorData.latlng.lat:'',
            value1:'',
            basicType:'',
            premiumType:'',
            siteLogo:this.props.location.state != undefined && this.props.location.state.exhibitorData!=null && this.props.location.state.exhibitorData.logo !=null  ? {
                key: this.props.location.state.exhibitorData.logo.key,
                uid:this.props.location.state.exhibitorData.logo.key ,
                bucket: this.props.location.state.exhibitorData.logo.bucket,
                region: this.props.location.state.exhibitorData.logo.region ,
                url:  `${S3_Url}${this.props.location.state.exhibitorData.logo.key}`
              } : {},
            partnerPhotos:[],
            loading:false,
            removedsiteLogo: false,
            submit:false,
            domain:"",
            aState: [],
            eType:false,
            showBasic:false,
            showPremium:false,
            disButton:false,
            type:'',
            pLoading:false,
            onCreate:false,
          onUpdate: false,
            targetState: [
              {value:'ALL_STATES', label:"ALL STATES"},
              {value:'JAMMU_KASHMIR',label:'JAMMU KASHMIR'},
              {value:'HIMACHAL_PRADESH',label:'HIMACHAL PRADESH'},
              {value:'PUNJAB',label:'PUNJAB'},
              {value:'DELHI',label:'DELHI'},
              {value:'RAJASTHAN',label:'RAJASTHAN'},
              {value:'UTTARAKHAND',label:'UTTARAKHAND'},
              {value:'UTTAR_PRADESH',label:'UTTAR PRADESH'},
              {value:'WEST_BENGAL',label:'WEST BENGAL'},
              {value:'BIHAR',label:'BIHAR'},
              {value:'JHARKHAND',label:'JHARKHAND'},
              {value:'ODISHA',label:'ODISHA'},
              {value:'ASSAM',label:'ASSAM'},
              {value:'ARUNACHAL_PRADESH',label:'ARUNACHAL PRADESH'},
              {value:'MEGHALAYA',label:'MEGHALAYA'},
              {value:'TRIPURA',label:'TRIPURA'},
              {value:'MIZORAM',label:'MIZORAM'},
              {value:'MANIPUR',label:'MANIPUR'},
              {value:'NAGALAND',label:'NAGALAND'},
              {value:'MAHARASHTRA',label:'MAHARASHTRA'},
              {value:'GUJARAT',label:'GUJARAT'},
              {value:'GOA',label:'GOA'},
              {value:'CHHATTISGARH',label:'CHHATTISGARH'},
              {value:'MADHYA_PRADESH',label:'MADHYA PRADESH'},
              {value:'ANDHRA_PRADESH',label:'ANDHRA PRADESH'},
              {value:'HARYANA',label:'HARYANA'},
              {value:'KERALA',label:'KERALA'},
              {value:'SIKKIM',label:'SIKKIM'},
              {value:'TAMIL_NADU',label:'TAMIL NADU'},
              {value:'TELANGANA',label:'TELANGANA'},
              {value:'KARNATAKA',label:'KARNATAKA'},
          ],
          ExhibitorType: '',
            exhType:'',
          statusExh:this.props.location.state != undefined && this.props.location.state.exhibitorData!=null && this.props.location.state.exhibitorData.enabled !=null ? this.props.location.state.exhibitorData.enabled:null
          
        }
    }

  componentWillReceiveProps() {
    let eType=this.props.location.state != undefined && this.props.location.state.exhibitorData != null && this.props.location.state.exhibitorData.exhType != null ? this.props.location.state.exhibitorData.exhType:"";
    //console.log("In CWR",this.props.location.state.exhibitorData);
    this.setState({
    exhType:eType
  })
  }
    
    getLatLong = (position) => {
        this.setState({
            lat:position.lat(),
            lng:position.lng()
        })
    }

    handlePartnerImg = (info) => {
      console.log("Img", info);
      // if (this.state.collateralImg.length == 0) {
      this.setState({
        partnerPhotos: info.fileList,
        loading: false,
        // fileListdocPhoto:[]
      });
      // }
    };
   
    handlePartnerPreview = (file) => {
      this.setState({
      PreviewImage: file.url || file.thumbUrl,
      PreviewVisible: true,
      });
  }
  
    handlePartnerRemove = (file) => {
      console.log("remove", file);
      // let i = this.state.partnerPhotos.indexOf(file);
      // //console.log("Index", i);
      // let j = this.state.partnerPhotos[i];
      // this.state.partnerPhotos.splice(i, 1);
                  // let j = [];
                  // j = this.state.collateralCaption.map((p) => {
                  //     return p;
                  //   });
                  //console.log("Index of", j);
                  //console.log("After Delete", this.state.collateralData);
      // this.setState({
      //   partnerImg: [],
      //   partnerPhotos: [],
      //   collateralCaption: [],
      //   removedimage: true,
      //   imageErr: false,
      // });
      // return true;
    };

    createImg = () => {
      console.log("In createImg");
      let partnerPhotos = [];
      if ( this.state.Exhibitordata != null ) {
        //console.log("In createImg props true");
        // partnerPhotos = this.state.Exhibitordata.marketingCollateral.map((p) => {
        //   return p;
        // });
        return
        if (partnerPhotos != null) {
          //console.log("In IF Data:::", collPhotos);
          let newImg = [];
          let partnerData = [];
          partnerPhotos.map((val) => {
            //console.log("ColData::", val.doc);
            //console.log("ColData::", val.name);
            newImg.push({
              bucket: val.doc.bucket,
              region: val.doc.region,
              key: val.doc.key,
              uid: val.doc.key,
              url: `https://${val.doc.bucket}.s3.${val.doc.region}.amazonaws.com/${val.doc.key}`,
            });
            partnerData.push({
              name: val.caption,
            });
          });
          //console.log("ColData::", collateralData);
          this.setState({
            partnerPhotos: newImg,
            partnerData: partnerData,
          });
          //console.log("New Img", newImg);
        }
      }
    };

      handleRemove = ()=> {
        this.setState({ siteLogo: {}, removedsiteLogo: true });
        // this.props.form.resetFields(['siteLogo'])
        return true;
      }

      handlePreview = (file) => {
        this.setState({
        previewImage: file.url || file.thumbUrl,
        previewVisible: true,
        });
    }

    siteHandleChange = ({ file }) => {
      // this.setState({ siteLogo : file })
      this.setState((prevState, props) => {
        if (
          Object.keys(prevState.siteLogo).length === 0 &&
          prevState.removedsiteLogo === false
        ) {
          return {
            siteLogo: file,
            removedsiteLogo: false
          };
        } else {
          return {
            removedsiteLogo: false
          };
        }
      });
    }

    CheckDomain = async (val) =>{
        return await this.props.client.query({
            query:CheckSubDomain,
            variables: {
                subdomain:val,
                fetchPolicy: 'network-only' 
            }
        })
        
        
        // .then(({data})=>{
        //     console.log(data)
        //     this.setState({domain:data.checkSubdomain},()=>{
        //         this.check(this.state.domain)
        //     })
        // })
    }

    // check = (val)=>{
    //     this.props.form.validateFields( (errors, values) => {
    //         console.log(errors)
    //         if (val != null) {
    //             console.log('in')
    //             this.props.form.setFields({
    //                 subdomain: {
    //                     value: values.subdomain,
    //                     errors: [new Error("Domain already exists")]
    //                 }
    //             });
    //             return true
    //         }

    //     });
    // }
    
    beforeUpload = (file) =>{
        var fr = new FileReader;
  
        fr.onload = () => { // file is loaded
            var img = new Image;
            img.onload = () => {
                
                if (img.width != 600 && img.height != 200) {
  
                  this.setState({
                    favLogoErr:true
                  })
                  this.props.form.setFields({
                    
                    siteLogo:{
                      value:this.state.siteLogo,
                      errors:[new Error('Please Upload image with (600 px * 200 px) dimensions.')]
                    }
                  })
                  return false
                }else{
                  this.setState({
                    favLogoErr:false
                  })
                }
              };
            img.src = fr.result; // is the data URL because called with readAsDataURL
        };
  
        fr.readAsDataURL(file); // I'm using a <input type="file"> for demonstrating
      }
    
    onChangeOption = (e) =>{
        this.setState({value1:e.target.value})
    }

    onBasicSite = ()=>{
      this.setState({showBasic:true,
        
      })
    }
    
    handleOpen = () => this.setState({ visibleMap: !this.state.visibleMap })
    handleCancel = () => this.setState({ visible:!this.state.visible, previewVisible:false,previewImage:false})
    handleModalCancel  = () => this.setState({ visible:!this.state.visible, PreviewVisible:false,PreviewImage:false})
    
    enterLoading = () => {
        this.setState({ loading: true});
    };

    // updateLoading = () => {
    //   console.log('in update loading')
    //   this.setState({ pLoading:true});
    // };
    
    messageClose = () => {
      this.setState({loading:false,onCreate:true})
      this.props.history.push('/jAdminExhibitorList');
    }
    messageUpdate = ()=>{
      this.props.history.push('/jAdminExhibitorList');
      // this.setState({loading:false,type:val,pLoading:false})
    }

    checkSubDomainExists =  (val) => {
          return this.props.client.query({
            query:CheckSubDomain,
            fetchPolicy: 'network-only',
            variables: {
                subdomain:val,
            }
        })
        // .then(({data})=>{
        //     console.log("in")
        //     console.log(data.checkSubdomain)
        //     if(data.checkSubdomain !== null) {
        //         console.log(data.checkSubdomain)
        //         this.props.form.setFields({
        //             subdomain: {
        //                 value: val,
        //                 errors: [new Error("Domains already exists")]
        //             }
        //         });
        //         return false
        //     }else{
        //         return true
        //     }
        // })
    }

    setType = ()=>{
      this.setState({basicType:"JEWEL_EXPO_BASIC"})
    }

    setPreType = ()=>{
      this.setState({premiumType:"JEWEL_EXPO_PREMIUM"})
    }



    onCancel = ()=>{
      this.props.history.push('/jAdminExhibitorList');
    }

    goBack = () =>{
      this.props.history.push('/jAdminExhibitorList');
    }

    selectChange = (value)=>{
        let allstate =  this.state.targetState
        let states = this.state.targetState
        
        if(value=="ALL_STATES"){
          let allState = states.splice(0,1)
          this.setState({targetState:allState,aState:allstate})
        }else{
          this.setState({aState:allstate})
        }
        
    }

    onDeselect = (value)=>{
      this.setState({targetState:this.state.aState})
    }

    onPremium = ()=>{
      this.setState({
        showPremium:true,
      })
    }

  createJewelExhibitor = (values) => {
     console.log("IN CREATEJEWELEXHI")
    // return false
     console.log("In Create", values);
    // if (values.exhType == "JEWELLER") {
    //   console.log("In Create If JEWELLER")
    //return
      this.props.createDigiExpoExhibitor({
        variables: {
          //exhType: values.exhType,
          ownMob: values.ownMob.trim(),
          companyName: values.companyName.trim(),
          ownerName: values.ownerName.trim(),
          email: values.email.trim(),
          salesHead: values.salesHead.trim(),
          compTel: values.compTel == "" ? undefined : values.compTel,
          businessAddress: {
            addressLineOne: values.addressLineOne,
            addressLineTwo: values.addressLineTwo,
            city: values.city,
            state: values.state,
            zip: values.pinCode,
            country: values.countryCode
          },
          latlng: values.addressOnMap,
          productsDealingIn: values.productDealing,
          target_states: values.stateTarget,
          otherContact: values.otherContact == "" ? undefined : values.otherContact,
          logo: values.siteLogo,
          productsMelting: values.productMelting.length != 0 ? values.productMelting : undefined,
          moreProductDetails: values.productProfile,
          slugCompName: values.slugCompName,
          branchesIn: values.branchesIn.length != 0 ? values.branchesIn : undefined,
          //createdAt: '',
          enabled: this.state.statusExh,
          status: "APPROVED",
          exhName:"PJC",
          //subdomain: domain[0].subDomain.trim(),
          //siteType: domain[0].siteType, 
          //machinery: values.productDealing,
          //allied: values.productDealing,
        }
      }).then(({ data }) => {
        // if (domain.length == 2) {
        //   this.updateJewelExhibitor(values, domain)
        // } else {
          console.log("Create successfull", data);
          message.success("Exhibitor created successfully", () => this.messageClose());
        // }
      }).catch(res => {
        console.log(
          `Catch: handleFormSubmit: error: ${JSON.stringify(res)}`
        );
      });
   // }

    if (values.exhType == "MACHINERY") {
      console.log("In Create If MACHINERY");
      this.props.createJewelExhibitor({
        variables: {
          exhType: values.exhType,
          ownMob: values.ownMob.trim(),
          companyName: values.companyName.trim(),
          ownerName: values.ownerName.trim(),
          email: values.email.trim(),
          salesHead: values.salesHead.trim(),
          compTel: values.compTel == "" ? undefined : values.compTel,
          businessAddress: {
            addressLineOne: values.addressLineOne,
            addressLineTwo: values.addressLineTwo,
            city: values.city,
            state: values.state,
            zip: values.pinCode,
            country: values.countryCode
          },
          latlng: values.addressOnMap,
          //productsDealingIn: values.productDealing,
          machinery: values.productDealing,
          //allied: values.productDealing,
          target_states: values.stateTarget,
          otherContact: values.otherContact == "" ? undefined : values.otherContact,
          logo: values.siteLogo,
          //productsMelting: values.productMelting.length != 0 ? values.productMelting : undefined,
          //siteType: domain[0].siteType,
          moreProductDetails: values.productProfile,
          slugCompName: values.slugCompName,
          branchesIn: values.branchesIn.length != 0 ? values.branchesIn : undefined,
          //subdomain: domain[0].subDomain.trim(),
          enabled: this.state.statusExh
        }
      }).then(({ data }) => {
        // if (domain.length == 2) {
        //   this.updateJewelExhibitor(values, domain)
        // } else {
          console.log("Create successfull", data.createJewelExhibitor);
          message.success("Exhibitor created successfully", () => this.messageClose());
        //}
      }).catch(res => {
        console.log(
          `Catch: handleFormSubmit: error: ${JSON.stringify(res)}`
        );
      });
    }

        if (values.exhType == "ALLIED") {
      console.log("In Create If ALLIED");
      this.props.createJewelExhibitor({
        variables: {
          exhType: values.exhType,
          ownMob: values.ownMob.trim(),
          companyName: values.companyName.trim(),
          ownerName: values.ownerName.trim(),
          email: values.email.trim(),
          salesHead: values.salesHead.trim(),
          compTel: values.compTel == "" ? undefined : values.compTel,
          businessAddress: {
            addressLineOne: values.addressLineOne,
            addressLineTwo: values.addressLineTwo,
            city: values.city,
            state: values.state,
            zip: values.pinCode,
            country: values.countryCode
          },
          latlng: values.addressOnMap,
          //productsDealingIn: values.productDealing,
          //machinery: values.productDealing,
          allied: values.productDealing,
          target_states: values.stateTarget,
          otherContact: values.otherContact == "" ? undefined : values.otherContact,
          logo: values.siteLogo,
          //productsMelting: values.productMelting.length != 0 ? values.productMelting : undefined,
          //siteType: domain[0].siteType,
          moreProductDetails: values.productProfile,
          slugCompName: values.slugCompName,
          branchesIn: values.branchesIn.length != 0 ? values.branchesIn : undefined,
          //subdomain: domain[0].subDomain.trim(),
          enabled: this.state.statusExh
        }
      }).then(({ data }) => {
        // if (domain.length == 2) {
        //   this.updateJewelExhibitor(values, domain)
        // } else {
          console.log("Create successfull", data.createJewelExhibitor);
          message.success("Exhibitor created successfully", () => this.messageClose());
       // }
      }).catch(res => {
        console.log(
          `Catch: handleFormSubmit: error: ${JSON.stringify(res)}`
        );
      });
    }
  }

  updateJewelExhibitor = (values, exhData) => {
    console.log("In Update func",values,exhData);
      // return false
    // if (values.exhType == "JEWELLER") {
    //   console.log("In Update If JEWELLER")
      this.props.updateJewelExhibitor({
        variables: {
          exhType: values.exhType,
          ownMob: values.ownMob.trim(),
          companyName: values.companyName.trim(),
          ownerName: values.ownerName.trim(),
          email: values.email.trim(),
          salesHead: values.salesHead.trim(),
          compTel: values.compTel == "" ? undefined : values.compTel,
          businessAddress: {
            addressLineOne: values.addressLineOne,
            addressLineTwo: values.addressLineTwo == null ? null : values.addressLineTwo,
            city: values.city,
            state: values.state,
            zip: values.pinCode,
            country: values.countryCode
          },
          latlng: values.addressOnMap,
          target_states: values.stateTarget,
          otherContact: values.otherContact == "" ? undefined : values.otherContact,
          logo: values.siteLogo,
          productsDealingIn: values.productDealing,
          //machinery: values.productDealing,
          //allied: values.productDealing,
          //siteType: exhData != null ? domain[0].siteType : domain[0].siteType == "JEWEL_EXPO_BASIC" ? "JEWEL_EXPO_PREMIUM" : 'JEWEL_EXPO_BASIC',
          moreProductDetails: values.productProfile,
          slugCompName: values.slugCompName,
          productsMelting: values.productMelting.length != 0 ? values.productMelting : undefined,
          branchesIn: values.branchesIn.length != 0 ? values.branchesIn : undefined,
          //subdomain: exhData != null ? domain[0].subDomain.trim() : domain[0].siteType == "JEWEL_EXPO_BASIC" ? values.premiumSubDomain.trim() : values.basicSubDomain.trim(),
          enabled: this.state.statusExh
        }
      }).then(({ data }) => {
        message.success("Exhibitor updated successfully", this.messageUpdate);
        console.log("Update successfull func", data.updateJewelExhibitor);
      }).catch(res => {
        console.log(`Catch: handleFormSubmit: error: ${JSON.stringify(res)}`);
      });
   // }
     if (values.exhType == "MACHINERY") {
      console.log("In Update If MACHINERY");
      this.props.updateJewelExhibitor({
        variables: {
          exhType: values.exhType,
          ownMob: values.ownMob.trim(),
          companyName: values.companyName.trim(),
          ownerName: values.ownerName.trim(),
          email: values.email.trim(),
          salesHead: values.salesHead.trim(),
          compTel: values.compTel == "" ? undefined : values.compTel,
          businessAddress: {
            addressLineOne: values.addressLineOne,
            addressLineTwo: values.addressLineTwo == null ? null : values.addressLineTwo,
            city: values.city,
            state: values.state,
            zip: values.pinCode,
            country: values.countryCode
          },
          latlng: values.addressOnMap,
          target_states: values.stateTarget,
          otherContact: values.otherContact == "" ? undefined : values.otherContact,
          logo: values.siteLogo,
          //productsDealingIn: values.productDealing,
          machinery: values.productDealing,
          //allied: values.productDealing,
          //siteType: exhData != null ? domain[0].siteType : domain[0].siteType == "JEWEL_EXPO_BASIC" ? "JEWEL_EXPO_PREMIUM" : 'JEWEL_EXPO_BASIC',
          moreProductDetails: values.productProfile,
          slugCompName: values.slugCompName,
          //productsMelting: values.productMelting.length != 0 ? values.productMelting : undefined,
          branchesIn: values.branchesIn.length != 0 ? values.branchesIn : undefined,
         // subdomain: exhData != null ? domain[0].subDomain.trim() : domain[0].siteType == "JEWEL_EXPO_BASIC" ? values.premiumSubDomain.trim() : values.basicSubDomain.trim(),
          enabled: this.state.statusExh
        }
      }).then(({ data }) => {
        message.success("Exhibitor updated successfully", this.messageUpdate);
        console.log("Update successfull func", data.updateJewelExhibitor);
      }).catch(res => {
        console.log(
          `Catch: handleFormSubmit: error: ${JSON.stringify(res)}`);
      });
    }
    if (values.exhType == "ALLIED") {
      console.log("In Update If ALLIED");
      this.props.updateJewelExhibitor({
        variables: {
          exhType: values.exhType,
          ownMob: values.ownMob.trim(),
          companyName: values.companyName.trim(),
          ownerName: values.ownerName.trim(),
          email: values.email.trim(),
          salesHead: values.salesHead.trim(),
          compTel: values.compTel == "" ? undefined : values.compTel,
          businessAddress: {
            addressLineOne: values.addressLineOne,
            addressLineTwo: values.addressLineTwo == null ? null : values.addressLineTwo,
            city: values.city,
            state: values.state,
            zip: values.pinCode,
            country: values.countryCode
          },
          latlng: values.addressOnMap,
          target_states: values.stateTarget,
          otherContact: values.otherContact == "" ? undefined : values.otherContact,
          logo: values.siteLogo,
          //productsDealingIn: values.productDealing,
          //machinery: values.productDealing,
          allied: values.productDealing,
          //siteType: exhData != null ? domain[0].siteType : domain[0].siteType == "JEWEL_EXPO_BASIC" ? "JEWEL_EXPO_PREMIUM" : 'JEWEL_EXPO_BASIC',
          moreProductDetails: values.productProfile,
          slugCompName: values.slugCompName,
          //productsMelting: values.productMelting.length != 0 ? values.productMelting : undefined,
          branchesIn: values.branchesIn.length != 0 ? values.branchesIn : undefined,
         // subdomain: exhData != null ? domain[0].subDomain.trim() : domain[0].siteType == "JEWEL_EXPO_BASIC" ? values.premiumSubDomain.trim() : values.basicSubDomain.trim(),
          enabled: this.state.statusExh
        }
      }).then(({ data }) => {
        message.success("Exhibitor updated successfully", this.messageUpdate);
        console.log("Update successfull func", data.updateJewelExhibitor);
      }).catch(res => {
        console.log(`Catch: handleFormSubmit: error: ${JSON.stringify(res)}`);
      });
    }
    }

  handleUpdate = () => {
    // console.log("In Submit Create");
    this.props.form.validateFields((err, values) => {
      // return false
      console.log("values", values.exhType,values);
      if (!err) {
        if (Object.keys(values.siteLogo).length == 0) {
          this.props.form.setFields({
            siteLogo: {
              value: values.siteLogo,
              errors: [new Error(" Image is required")]
            }
          });
          return true;
        }
      
        values.slugCompName = slugify(values.companyName, {
          lower: true
        })

        values.addressOnMap = this.state.lat != "" ? {
          lat: this.state.lat,
          lng: this.state.lng
        } : undefined

        if (Object.keys(this.state.siteLogo).length != 0 && !this.state.siteLogo.hasOwnProperty("key")) {
          values.siteLogo = {
            bucket: bucket,
            region: s3_Region,
            localUri: this.state.siteLogo.originFileObj,
            key: `PJC/site/${this.state.siteLogo.originFileObj.name}`,
            mimeType: this.state.siteLogo.type
          };
          
        } else {
          const { uid, url, bucket, key, region } = this.state.siteLogo
          values.siteLogo = {
            bucket,
            key,
            region
          }
        }

        if (values.productProfile == "") {
          delete values.productProfile
        }
      
        this.enterLoading()
        // if (values.exhType == "JEWELLER" || values.exhType == " ") {
        //   console.log("In hUpdate If JEWELLER")
          this.props.updateBasicInfoExhibitor({
            variables: {
             // exhType: values.exhType,
              id: values.ownMob.trim(),
              companyName: values.companyName.trim(),
              ownerName: values.ownerName.trim(),
              email: values.email.trim(),
              salesHead: values.salesHead.trim(),
              compTel: values.compTel == "" ? undefined : values.compTel,
              businessAddress: {
                addressLineOne: values.addressLineOne,
                addressLineTwo: values.addressLineTwo == null ? null : values.addressLineTwo,
                city: values.city,
                state: values.state,
                zip: values.pinCode,
                country: values.countryCode
              },
              latlng: values.addressOnMap,
              target_states: values.stateTarget,
              otherContact: values.otherContact == "" ? undefined : values.otherContact,
              productsDealingIn: values.productDealing,
              //machinery: values.productDealing,
              //allied: values.productDealing,
              moreProductDetails: values.productProfile,
              slugCompName: values.slugCompName,
              productsMelting: values.productMelting.length != 0 ? values.productMelting : undefined,
              branchesIn: values.branchesIn.length != 0 ? values.branchesIn : undefined,
              exhName: "PJC",
              logo: values.siteLogo,
              enabled: this.state.statusExh
            }
          }).then(({ data }) => {
            console.log("In Update props", data);
            this.setState({ loading: false })
            message.success("Exhibitor updated successfully");
          }).catch(res => {
            console.log(
              `Catch: handleFormSubmit: error: ${JSON.stringify(res)}`
            );
          });
       // }
         if (values.exhType == "MACHINERY") {
          console.log("In hUpdate If MACHINERY");
          this.props.updateBasicInfoExhibitor({
            variables: {
              exhType: values.exhType,
              id: values.ownMob.trim(),
              companyName: values.companyName.trim(),
              ownerName: values.ownerName.trim(),
              email: values.email.trim(),
              salesHead: values.salesHead.trim(),
              compTel: values.compTel == "" ? undefined : values.compTel,
              businessAddress: {
                addressLineOne: values.addressLineOne,
                addressLineTwo: values.addressLineTwo == null ? null : values.addressLineTwo,
                city: values.city,
                state: values.state,
                zip: values.pinCode,
                country: values.countryCode
              },
              latlng: values.addressOnMap,
              target_states: values.stateTarget,
              otherContact: values.otherContact == "" ? undefined : values.otherContact,
              //productsDealingIn: values.productDealing,
              machinery: values.productDealing,
              //allied: values.productDealing,
              moreProductDetails: values.productProfile,
              slugCompName: values.slugCompName,
              //productsMelting: values.productMelting.length != 0 ? values.productMelting : undefined,
              branchesIn: values.branchesIn.length != 0 ? values.branchesIn : undefined,
              exhName: "PJC",
              logo: values.siteLogo,
              enabled: this.state.statusExh
            }
          }).then(({ data }) => {
            console.log("In Update props", data);
            this.setState({ loading: false })
            message.success("Exhibitor updated successfully");
          }).catch(res => {
            console.log(
              `Catch: handleFormSubmit: error: ${JSON.stringify(res)}`
            );
          });
        }
         if (values.exhType == "ALLIED") {
          console.log("In hUpdate If ALLIED")
          this.props.updateBasicInfoExhibitor({
            variables: {
              exhType: values.exhType,
              id: values.ownMob.trim(),
              companyName: values.companyName.trim(),
              ownerName: values.ownerName.trim(),
              email: values.email.trim(),
              salesHead: values.salesHead.trim(),
              compTel: values.compTel == "" ? undefined : values.compTel,
              businessAddress: {
                addressLineOne: values.addressLineOne,
                addressLineTwo: values.addressLineTwo == null ? null : values.addressLineTwo,
                city: values.city,
                state: values.state,
                zip: values.pinCode,
                country: values.countryCode
              },
              latlng: values.addressOnMap,
              target_states: values.stateTarget,
              otherContact: values.otherContact == "" ? undefined : values.otherContact,
              //productsDealingIn: values.productDealing,
              //machinery: values.productDealing,
              allied: values.productDealing,
              moreProductDetails: values.productProfile,
              slugCompName: values.slugCompName,
              //productsMelting: values.productMelting.length != 0 ? values.productMelting : undefined,
              branchesIn: values.branchesIn.length != 0 ? values.branchesIn : undefined,
              exhName: "PJC",
              logo: values.siteLogo,
              enabled: this.state.statusExh
            }
          }).then(({ data }) => {
            console.log("In Update props", data);
            this.setState({ loading: false })
            message.success("Exhibitor updated successfully");
          }).catch(res => {
            console.log(
              `Catch: handleFormSubmit: error: ${JSON.stringify(res)}`
            );
          });
        }

      }
      })
  
      }

     handleSubmit = (e) => {
       e.preventDefault();
       this.props.form.validateFields(async(err, values) => {
        //  let domain = []
        //  if(values.basicSubDomain != "" && values.basicSubDomain != undefined){
        //    let obj = {subDomain : values.basicSubDomain,siteType:"JEWEL_EXPO_BASIC"}
        //    domain.push(obj)
        //  }
        //  if(values.premiumSubDomain != "" && values.premiumSubDomain != undefined){
        //   let obj = {subDomain : values.premiumSubDomain,siteType:"JEWEL_EXPO_PREMIUM"}
        //    domain.push(obj)
        //  }
         values.addressOnMap = this.state.lat != "" ? {
              lat:this.state.lat,
              lng:this.state.lng
          }:undefined
        //  values.subdomain=values.subdomain != undefined ? values.subdomain.toLowerCase().trim().split(" ")[0] : ""
        //  let checkSub = ''
        //  let premiumCheck = ''
        // if(values.basicSubDomain != "" && values.basicSubDomain != undefined){
        //   checkSub = await this.checkSubDomainExists(values.basicSubDomain)
        // }
        // if(values.premiumSubDomain != "" && values.premiumSubDomain != undefined){
        //   premiumCheck = await this.checkSubDomainExists(values.premiumSubDomain)
        // }
        //  if (values.addressOnMap.lat == "" && values.addressOnMap.lng == "") {
        //     this.props.form.setFields({
        //         addressOnMap: {
        //         value: values.addressOnMap,
        //         errors: [new Error("Location is required")]
        //       }
        //     });
        //     return true;
        //   }else{
        //     this.props.form.setFields({
        //         addressOnMap: {
        //         value: values.addressOnMap,
        //         errors:""
        //       }
        //     });
        //   }

        if (!err) {
         

              if(Object.keys(values.siteLogo).length == 0) {
                this.props.form.setFields({
                  siteLogo: {
                    value: values.siteLogo,
                        errors: [new Error("Image is required")]
                      }
                    });
                    return true;
                }

                
                // if( checkSub != "" && null !== checkSub.data.checkSubdomain){
                //   this.props.form.setFields({
                //     basicSubDomain: {
                //           value: values.basicSubDomain,
                //           errors: [new Error("Domain already exists")]
                //     }
                //   });
                //   return true
                // }else{
                //   this.props.form.setFields({
                //     basicSubDomain: {
                //           value: values.basicSubDomain,
                //           errors: ''
                //     }
                //   });
                // }

                // if(premiumCheck != "" &&  null !== premiumCheck.data.checkSubdomain ){
                //     this.props.form.setFields({
                //       premiumSubDomain: {
                //           value: values.premiumSubDomain,
                //           errors: [new Error("Domain already exists")]
                //       }
                //     });
                //     return true
                //   }else{
                //     this.props.form.setFields({
                //       premiumSubDomain: {
                //           value: values.premiumSubDomain,
                //           errors: ''
                //       }
                //     });
                //   }
                
                values.slugCompName = slugify(values.companyName, {
                    lower: true
                })
                /* main image */
                    
                // if(Object.keys(this.state.siteLogo).length != 0 && this.state.siteLogo.constructor == Object){
                //     // console.log(JSON.stringify(this.state.siteLogo.originFileObj))
                //     values.siteLogo = {
                //         bucket: bucket,
                //         region: s3_Region,
                //         localUri: this.state.siteLogo.originFileObj,
                //         key:  `${this.props.customSiteId}/site/${this.state.siteLogo.originFileObj.name}`,
                //         mimeType: this.state.siteLogo.type
                //     }
                // }
                  console.log("this.props.customSiteId..",this.props.customSiteId)
                if (Object.keys(this.state.siteLogo).length != 0 && !this.state.siteLogo.hasOwnProperty("key")) {
                    values.siteLogo = {
                      bucket: bucket,
                      region: s3_Region,
                      localUri: this.state.siteLogo.originFileObj,
                      key: `PJC/site/${this.state.siteLogo.originFileObj.name}`,
                      mimeType: this.state.siteLogo.type
                    };
                    
                  }else{
                    const { uid, url, bucket, key, region } = this.state.siteLogo
                    values.siteLogo = {
                      bucket,
                      key,
                      region
                    }
                  }
                console.log("Logo::",values.siteLogo);

                  if(values.productProfile == ""){
                      delete values.productProfile
                  }
                  
                // if(values.subdomain != null){
                //     this.props.form.setFields({
                //         subdomain: {
                //             value: values.subdomain,
                //             errors: [new Error("Domain already exists")]
                //         }
                //     });
                //     return false
                // }
                // return false
                this.enterLoading()
                if(this.props.location.state && this.props.location.state.exhibitorData && this.props.location.state.exhibitorData.ownerName != null){

                  this.handleUpdate(values,this.props.location.state.exhibitorData)
                }else{
                  this.createJewelExhibitor(values)
                }
                }else{
                    console.log("Error", err)
                }
            })
        }

        statusExh=(e)=>{
          // console.log('statusExh',e)
          this.setState({
            statusExh:e.target.value
          })
        }
    render(){
      //console.log("In Create",this.props.location.state.exhibitorData.exhType);
      const children = [];
      const targetStateSorted = _.sortBy(this.state.targetState, ['label'])
      const productDealingSorted = _.sortBy(productDealing, ['label'])
      const stateSorted = _.sortBy(state, ['label'])
      let exhibitorData =this.props.location.state != undefined && this.props.location.state.exhibitorData != null ? this.props.location.state.exhibitorData : ""
      //console.log("EXHI Data.. : ",this.props.customSiteId)
      //console.log("Logo",this.state.siteLogo)
      //let ExhiType = this.props.location.state != undefined && this.props.location.state.exhibitorData != null && this.props.location.state.exhibitorData.exhType != null ? this.props.location.state.exhibitorData.exhType:"";
      //console.log("In Create Exh", ExhibitorType);
      let basic = exhibitorData != "" && exhibitorData.jewExpo != null && exhibitorData.jewExpo.map((val) => {
        if(val.siteType == "JEWEL_EXPO_BASIC"){
          return val.siteType
        }
      })
      basic = basic && basic.filter(function( element ) {
        return element !== undefined;
      });
      basic = basic.toString()
  
      let premium = exhibitorData != ""  && exhibitorData.jewExpo != null && exhibitorData.jewExpo.map((val)=>{
        if(val.siteType == "JEWEL_EXPO_PREMIUM"){
          return val.siteType
        }
      })
      premium =premium && premium.filter(function( element ) {
        return element !== undefined;
      });
      premium = premium.toString()
        const uploadButton = (
            <div>
              <Icon type="plus" />
              <div>Upload</div>
            </div>
          );
        const { getFieldDecorator, getFieldValue } = this.props.form;
        const { siteLogo,partnerPhotos, previewVisible,previewImage} =this.state
        const formItemLayout = {
          labelCol: { span: 12 },
          wrapperCol: { span: 16 }
      };
      //console.log("In Create 1");
        return (
          <App header="Create Exhibitor">
            <Form onSubmit={this.handleSubmit}>
              <Row gutter={16}>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 4 }}
                  lg={{ span: 4 }}
                >
                  <Breadcrumb style={{ marginBottom: "20px" }}>
                    <Link to="/">
                      <Breadcrumb.Item>Home</Breadcrumb.Item>
                    </Link>
                    <Breadcrumb.Item>Create Exhibitor</Breadcrumb.Item>
                  </Breadcrumb>
                </Col>
                <Col span={4} offset={16} style={{ marginBottom: "10px" }}>
                  {/* {exhibitorData.jewExpo != null &&
                    exhibitorData.jewExpo.length >= 1 && ( */}
                      <div>
                        <Button
                          type="primary"
                          style={{ background: "#389e0d", color: "#fff" }}
                          //onClick={this.handleUpdate}
                          loading={this.state.loading}
                          htmlType="submit"
                        >
                          Save
                        </Button>
                        <Button
                          style={{
                            background: "#f44336",
                            color: "#fff",
                            marginLeft: "15px",
                          }}
                          onClick={this.goBack}
                        >
                          Cancel
                        </Button>
                      </div>
                    {/* // )} */}
                </Col>
                {/* <Col xs={{span:24}} sm={{span:24}} md={{span:2}} lg={{span:2}}>
            {basic == "JEWEL_EXPO_BASIC" && premium == "JEWEL_EXPO_PREMIUM" &&  <Button type="primary"  style={{background:"#389e0d", color:"#fff", marginBottom: 16, marginRight:8}}  htmlType="submit">Update</Button>}
          </Col> */}
              </Row>
              {/* <Row gutter={24}>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="Exhibitor Type">
                    {getFieldDecorator("exhType", {
                      validateTrigger: ["onChange", "onBlur"],
                      // initialValue:
                      //   exhibitorData.exhType != null
                      //     ? exhibitorData.exhType
                      //     : "",
                      initialValue: exhibitorData.exhType,
                      rules: [
                        {
                          required: true,
                          message: "Please select type",
                        },
                      ],
                    })(
                      <Select
                        placeholder="Please Select Exhibitor Type"
                        onSelect={(val) =>
                          this.setState({ ExhibitorType: val })
                        }
                        //disabled={exhibitorData.exhType != null}
                      >
                        {ExhType.map((c) => {
                          return (
                            <Option key={c.value} value={c.value}>
                              {c.label}
                            </Option>
                          );
                        })}
                      </Select>
                    )}
                  </FormItem>
                </Col>
              </Row> */}
              <Row gutter={24}>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="Owner Name">
                    {getFieldDecorator("ownerName", {
                      trigger: "onBlur",
                      valuePropName: "defaultValue",
                      initialValue: exhibitorData.ownerName,
                      rules: [
                        {
                          required: true,
                          message: "Please enter owner name.",
                        },
                      ],
                    })(
                      // <Tooltip
                      //     title="Enter unique product name"
                      //     placement="topLeft"
                      // >
                      <Input
                        placeholder="Owner name"
                        style={{ width: "100%", marginRight: 8 }}
                      />
                      // </Tooltip>
                    )}
                  </FormItem>
                </Col>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="Company Name">
                    {getFieldDecorator("companyName", {
                      trigger: "onBlur",
                      valuePropName: "defaultValue",
                      initialValue: exhibitorData.companyName,
                      rules: [
                        {
                          required: true,
                          message: "Please enter company name.",
                        },
                      ],
                    })(
                      // <Tooltip
                      //     title="Enter unique product name"
                      //     placement="topLeft"
                      // >
                      <Input
                        placeholder="Company name"
                        style={{ width: "100%", marginRight: 8 }}
                      />
                      // </Tooltip>
                    )}
                  </FormItem>
                </Col>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="Owner Mobile Number">
                    {getFieldDecorator("ownMob", {
                      trigger: "onBlur",
                      valuePropName: "defaultValue",
                      initialValue:
                        this.props.location.state &&
                        this.props.location.state.mobileNumber != "" &&
                        this.props.location.state.mobileNumber != undefined
                          ? this.props.location.state.mobileNumber.trim()
                          : exhibitorData.id,
                      rules: [
                        {
                          required: true,
                          message: "Please enter mobile number.",
                        },
                      ],
                    })(
                      // <Tooltip
                      //     title="Enter unique product name"
                      //     placement="topLeft"
                      // >
                      <Input
                        placeholder="Mobile number"
                        disabled
                        style={{ width: "100%", marginRight: 8 }}
                      />
                      // </Tooltip>
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={24}>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="Email Id">
                    {getFieldDecorator("email", {
                      initialValue: exhibitorData.email,
                      trigger: "onBlur",
                      valuePropName: "defaultValue",
                      rules: [
                        {
                          required: true,
                          message: "Please enter email.",
                        },
                        {
                          validator: (rule, value, cb) => {
                            if (value) {
                              if (
                                !value.match(
                                  /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                                )
                              ) {
                                cb("Please enter valid Email address");
                              }
                            }
                            cb();
                          },
                        },
                      ],
                    })(
                      <Input
                        placeholder="Email Id"
                        style={{ width: "100%", marginRight: 8 }}
                      />
                    )}
                  </FormItem>
                </Col>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="Sales Head">
                    {getFieldDecorator("salesHead", {
                      trigger: "onBlur",
                      valuePropName: "defaultValue",
                      initialValue: exhibitorData.salesHead,
                      rules: [
                        {
                          required: true,
                          message: "Please enter sales head.",
                        },
                      ],
                    })(
                      <Input
                        placeholder="Sales head"
                        style={{ width: "100%", marginRight: 8 }}
                      />
                    )}
                  </FormItem>
                </Col>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="Other Contact Number">
                    {getFieldDecorator("otherContact", {
                      initialValue: exhibitorData.otherContact,
                      trigger: "onBlur",
                      valuePropName: "defaultValue",
                      //   rules: [{
                      //   required: true,
                      //   message: "Please enter Other contact",
                      //   }],
                    })(
                      <Input
                        placeholder="Other Contact"
                        style={{ width: "100%", marginRight: 8 }}
                      />
                    )}
                  </FormItem>
                </Col>
                {/* {this.props.location.state.exhibitorData.jewExpo.map((valr)=>{
              return(
                <FormItem label='Site Type' >
                <RadioGroup onChange={this.onChangeOption} value={valr.siteType}>
                <Radio value={"JEWEL_EXPO_BASIC"}>JEWEL EXPO BASIC</Radio>
                <Radio value={"JEWEL_EXPO_PREMIUM"}>JEWEL EXPO PREMIUM</Radio>
                </RadioGroup>
                </FormItem>
              )})} */}
                {/* <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
            <FormItem label='Site Type' >
                     <RadioGroup onChange={this.onChangeOption} value={this.state.value1}>
                     <Radio value={"JEWEL_EXPO_BASIC"}>JEWEL EXPO BASIC</Radio>
                     <Radio value={"JEWEL_EXPO_PREMIUM"}>JEWEL EXPO PREMIUM</Radio>
                   </RadioGroup>
           </FormItem>
           </Col> */}
              </Row>
              <Row gutter={24}>
                {/* <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
            <FormItem label='Sub Domain' >
                    {getFieldDecorator('subdomain', {
                        trigger: 'onBlur',
                        valuePropName: 'defaultValue',
                        initialValue:"",
                        rules: [{
                            required: true,
                            message: "Please enter Sub domain.",
                        }
                        // ,{
                        // validator:(rule, value, cb)=>{
                        //     if(this.state.submit){
                        //       cb('location required')
                        //     }
                        //     cb()
                        // }}
                        ],
                    })(
                        <Input placeholder="Sub domain" style={{ width: '100%', marginRight: 8 }} />
                    )}
            </FormItem>
            </Col>
          */}
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="Company Tel Number.">
                    {getFieldDecorator("compTel", {
                      initialValue: exhibitorData.compTel,
                      trigger: "onBlur",
                      valuePropName: "defaultValue",

                      //   rules: [{
                      //   required: true,
                      //   message: "Please enter Company Tel no.",
                      //   }],
                    })(
                      <Input
                        placeholder="Company Tel no"
                        style={{ width: "100%", marginRight: 8 }}
                      />
                    )}
                  </FormItem>
                </Col>
                {/* {(this.state.exhType != null && this.state.exhType == "JEWELLER") ||
                this.state.ExhibitorType == "JEWELLER" ? ( */}
                  <Col
                    xs={{ span: 24 }}
                    sm={{ span: 24 }}
                    md={{ span: 8 }}
                    lg={{ span: 8 }}
                  >
                    <FormItem label="Product Melting">
                      {getFieldDecorator("productMelting", {
                        validateTrigger: ["onChange", "onBlur"],
                        initialValue:
                          exhibitorData.productsMelting != null
                            ? exhibitorData.productsMelting
                            : [],
                        //   rules: [{
                        //   required: true,
                        //   message: "Please enter product melting.",
                        //   }],
                      })(
                        <Select
                          mode="multiple"
                          // onChange={this.selectChange}
                          placeholder="Please select"
                        >
                          {productMelting.map((c) => {
                            return (
                              <Option key={c.value} value={c.value}>
                                {c.label}
                              </Option>
                            );
                          })}
                        </Select>
                      )}
                    </FormItem>
                  </Col>
                {/* ) : (
                  <div></div>
                )} */}
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="Branches In">
                    {getFieldDecorator("branchesIn", {
                      validateTrigger: ["onChange", "onBlur"],
                      initialValue:
                        exhibitorData.branchesIn != null
                          ? exhibitorData.branchesIn
                          : [],
                      //   rules: [{
                      //   required: true,
                      //   message: "Please enter branches in.",
                      //   }],
                    })(
                      // <Select
                      //   mode="tags"
                      //   // onChange={this.selectChange}
                      //   placeholder="Please select"
                      // >
                      //   {children}
                      // </Select>
                      <Input
                      placeholder="Branches In"
                      style={{ width: "100%", marginRight: 8 }}
                    />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={24}>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="Address Line 1">
                    {getFieldDecorator("addressLineOne", {
                      trigger: "onBlur",
                      valuePropName: "defaultValue",
                      initialValue:
                        exhibitorData.businessAddress != null
                          ? exhibitorData.businessAddress.addressLineOne
                          : "",
                      //   initialValue:this.props.data && this.props.data.footerAddress ?  this.props.data.footerAddress.addressLineOne : "",
                      rules: [
                        {
                          required: true,
                          message: "Please enter Address Line 1",
                        },
                      ],
                    })(<TextArea rows={3} placeholder="Address Line 1" />)}
                  </FormItem>
                </Col>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="Address Line 2">
                    {getFieldDecorator("addressLineTwo", {
                      trigger: "onBlur",
                      valuePropName: "defaultValue",
                      initialValue:
                        exhibitorData.businessAddress != null
                          ? exhibitorData.businessAddress.addressLineTwo
                          : "",
                      //   initialValue:this.props.data && this.props.data.footerAddress ?  this.props.data.footerAddress.addressLineTwo : "",
                      rules: [
                        {
                          required: false,
                          message: "Please enter Address Line 2",
                        },
                      ],
                    })(<TextArea rows={3} placeholder="Address Line 2" />)}
                  </FormItem>
                </Col>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 4 }}
                  lg={{ span: 4 }}
                >
                  <FormItem label="PinCode">
                    {getFieldDecorator("pinCode", {
                      initialValue:
                        exhibitorData.businessAddress != null
                          ? exhibitorData.businessAddress.zip
                          : "",
                      trigger: "onBlur",
                      valuePropName: "defaultValue",
                      rules: [
                        {
                          required: true,
                          message: "Please enter pincode",
                        },
                      ],
                    })(
                      <Input
                        placeholder="PinCode"
                        style={{ width: "100%", marginRight: 8 }}
                      />
                    )}
                  </FormItem>
                </Col>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 4 }}
                  lg={{ span: 4 }}
                >
                  <FormItem label="Address Pin on map">
                    {getFieldDecorator("addressOnMap", {
                      validateTrigger: ["onChange", "onBlur"],
                      initialValue: {
                        lat: this.state.lat,
                        lng: this.state.lng,
                      },
                      //     rules: [
                      //                     {
                      // validator:(rule, value, cb)=>{
                      //     if(this.state.lat == ""){
                      //       cb('location required')
                      //     }
                      //     cb()
                      // }}
                      // ],
                    })(
                      <div>
                        <div onClick={this.handleOpen}>
                          <img
                            src="https://cdn.pixabay.com/photo/2016/03/22/04/23/map-1272165__340.png"
                            width="40"
                            height="40"
                          />
                        </div>
                        <Modal
                          footer={null}
                          width="200"
                          title="Select your Location on the Map"
                          visible={this.state.visibleMap}
                          onOk={this.handleOpen}
                          onCancel={this.handleCancel}
                        >
                          {" "}
                          <MapSearchBox
                            handleInputChange={this.handleInputChange}
                            getLatLong={this.getLatLong}
                            handleCloseX={this.handleClose}
                            businessAddress={this.state.businessAddress}
                            lat={this.state.lat}
                            lng={this.state.lng}
                          />
                        </Modal>
                      </div>
                    )}
                  </FormItem>
                  {this.state.lat && (
                    <div style={{ color: "green" }}>
                      <b>Location Updated...</b>
                    </div>
                  )}
                  {this.state.lat == "" && this.state.error && (
                    <div style={{ background: "red" }}>
                      <div
                        style={{ padding: "0px", margin: "0px", color: "#fff" }}
                      >
                        <p className="header">
                          Please select the Location Pin on Map.
                        </p>
                      </div>
                    </div>
                  )}
                </Col>
              </Row>
              <Row gutter={16}>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="City">
                    {getFieldDecorator("city", {
                      trigger: "onBlur",
                      valuePropName: "defaultValue",
                      initialValue:
                        exhibitorData.businessAddress != null
                          ? exhibitorData.businessAddress.city
                          : "",
                      rules: [
                        {
                          required: true,
                          message: "Please enter city",
                        },
                      ],
                    })(
                      <Input
                        placeholder="City"
                        style={{ width: "100%", marginRight: 8 }}
                      />
                    )}
                  </FormItem>
                </Col>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="State">
                    {getFieldDecorator("state", {
                      validateTrigger: ["onChange", "onBlur"],
                      initialValue:
                        exhibitorData.businessAddress != null
                          ? exhibitorData.businessAddress.state
                          : "MAHARASHTRA",
                      rules: [
                        {
                          required: true,
                          message: "Please enter state",
                        },
                      ],
                    })(
                      <Select placeholder="Please select">
                        {stateSorted.map((c) => {
                          return (
                            <Option key={c.value} value={c.value}>
                              {c.label}
                            </Option>
                          );
                        })}
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="Country Code">
                    {getFieldDecorator("countryCode", {
                      validateTrigger: ["onChange", "onBlur"],
                      initialValue:
                        exhibitorData.businessAddress != null
                          ? exhibitorData.businessAddress.country
                          : "IND",
                      rules: [
                        {
                          required: true,
                          message: "Please enter country code.",
                        },
                      ],
                    })(
                      <Select>
                        {country.map((c) => {
                          return (
                            <Option key={c.value} value={c.value}>
                              {c.label}
                            </Option>
                          );
                        })}
                      </Select>
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={16}>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="Products dealing in">
                    {getFieldDecorator("productDealing", {
                      validateTrigger: ["onChange", "onBlur"],
                      initialValue: exhibitorData.productsDealingIn
                        ? exhibitorData.productsDealingIn
                        // : exhibitorData.machinery
                        // ? exhibitorData.machinery
                        // : exhibitorData.allied
                        // ? exhibitorData.allied
                        : [],
                      rules: [
                        {
                          required: true,
                          message: "Please enter products dealing in.",
                        },
                      ],
                    })(
                      <Select mode="multiple" placeholder="Please select">
                        {/* {this.state.ExhibitorType == "JEWELLER" ? ( */}
                          {productDealingSorted.map((c) => {
                            return (
                              <Option key={c.value} value={c.value}>
                                {c.label}
                              </Option>
                            );
                          })
                        }
                        {/* ) : this.state.ExhibitorType == "MACHINERY" ? (
                          productDealingMachinery.map((c) => {
                            return (
                              <Option key={c.value} value={c.value}>
                                {c.label}
                              </Option>
                            );
                          })
                        ) : this.state.ExhibitorType == "ALLIED" ? (
                          productDealingAllied.map((c) => {
                            return (
                              <Option key={c.value} value={c.value}>
                                {c.label}
                              </Option>
                            );
                          })
                        ) : (
                          <Option value="">
                            Please Select Exhibitor Type First
                          </Option>
                        )} */}
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="Target State">
                    {getFieldDecorator("stateTarget", {
                      validateTrigger: ["onChange", "onBlur"],
                      initialValue: exhibitorData.target_states
                        ? exhibitorData.target_states
                        : [],
                      rules: [
                        {
                          required: true,
                          message: "Please enter target state.",
                        },
                      ],
                    })(
                      <Select
                        mode="multiple"
                        // onChange={this.selectChange}
                        // onDeselect={this.onDeselect}
                        placeholder="Please select"
                      >
                        {targetStateSorted.map((c) => {
                          return (
                            <Option key={c.value} value={c.value}>
                              {c.label}
                            </Option>
                          );
                        })}
                      </Select>
                    )}
                  </FormItem>
                </Col>

                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 8 }}
                >
                  <FormItem label="Add short Product Profile">
                    {getFieldDecorator("productProfile", {
                      trigger: "onBlur",
                      valuePropName: "defaultValue",
                      initialValue: exhibitorData.moreProductDetails,
                    })(<TextArea rows={3} placeholder="Product Profile" />)}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={16}>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 8 }}
                  lg={{ span: 10 }}
                >
                  <FormItem label="Site Logo (Upload image with (600 px * 200 px) dimensions)">
                    {getFieldDecorator("siteLogo", {
                      validateTrigger: ["onChange", "onBlur"],
                      initialValue: exhibitorData.logo ? exhibitorData.logo : "",
                      rules: [
                        {
                          required: true,
                          message: "Images are required",
                        },
                      ],
                    })(
                      <Upload
                        listType="picture-card"
                        data={siteLogo ? siteLogo : ""}
                        // defaultFileList={ [siteLogo]}
                        defaultFileList={Object.keys(siteLogo).length ? [siteLogo] : null}
                        onPreview={this.handlePreview}
                        onChange={this.siteHandleChange}
                        beforeUpload={this.beforeUpload}
                        onRemove={this.handleRemove}
                      >
                        {Object.keys(siteLogo).length != 0
                          ? null
                          : uploadButton}
                      </Upload>
                    )}

                    <Modal
                      visible={previewVisible}
                      footer={null}
                      onCancel={this.handleCancel}
                    >
                      <div>
                        <img
                          id="sitelogo"
                          key="sitelogo"
                          style={{ width: "100%" }}
                          src={previewImage}
                        />
                      </div>
                    </Modal>
                  </FormItem>
                </Col>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 6 }}
                  lg={{ span: 6 }}
                  xl={{ span: 6 }}
                >
                  <FormItem label="Status">
                    <Radio.Group
                      onChange={this.statusExh}
                      defaultValue={true}
                      value={this.state.statusExh}
                    >
                      <Radio value={true}>Active</Radio>
                      <Radio value={false}>InActive</Radio>
                    </Radio.Group>
                  </FormItem>
                </Col>
                {/* <Col xs={{span:24}} sm={{span:24}} md={{span:16}} lg={{span:16}}>
                    <Row>
                      <Col xs={{span:24}} sm={{span:24}} md={{span:5}} lg={{span:5}}>
                        <Button type="danger" ghost  onClick={this.onBasicSite}>Create Basic Site</Button>
                      </Col>
                      <Col xs={{span:24}} sm={{span:24}} md={{span:11}} lg={{span:11}}>
                      {this.state.showBasic && <FormItem label='Enter Sub Domain' {...formItemLayout} >
                              {getFieldDecorator('subdomain', {
                                  trigger: 'onBlur',
                                  valuePropName: 'defaultValue',
                                  initialValue:"",
                                  rules: [{
                                      required: true,
                                      message: "Please enter Sub domain.",
                                  }
                                  ],
                              })(
                                  <Input placeholder="Sub domain" style={{ width: '60%', marginLeft: 8 }} />
                              )}
                      </FormItem>}
                      </Col>
                    </Row>
                    <div style={{marginTop:"60px"}}>
                      <Button type="danger" ghost>Create Premium Site</Button>
                    </div>
                </Col>
         
                <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
                 
                </Col> */}
              </Row>
              {!this.state.loading && (
                  <div>
                    <Row gutter={24}>
                      <Col
                        xs={{ span: 24 }}
                        sm={{ span: 24 }}
                        md={{ span: 4 }}
                        lg={{ span: 4 }}
                      >
                      {partnerPhotos.length > 0 ? (
                          <Form.Item label="Upload Partner/Member Photos">
                            {getFieldDecorator("partnerPhoto", {
                              validateTrigger: ["onChange", "onBlur"],
                              initialValue: partnerPhotos,
                            })(
                              <Upload
                                listType="picture-card"
                                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                                fileList={partnerPhotos}
                                //defaultFileList={}
                                onPreview={this.handlePartnerPreview}
                                onChange={this.handlePartnerImg}
                                onRemove={this.handlePartnerRemove}
                              >
                                {Object.keys(partnerPhotos).length >= 3
                                  ? null
                                  : uploadButton}
                              </Upload>
                            )}

                            <Modal
                              visible={this.state.PreviewVisible}
                              footer={null}
                              onCancel={this.handleModalCancel}
                            >
                              <img
                                alt="inside page image"
                                style={{ width: "50%" }}
                                src={this.state.PreviewImage}
                              />
                            </Modal>
                          </Form.Item>
                        ) : (
                          <Form.Item label="Upload Partner/Member Photos">
                            {getFieldDecorator("partnerPhoto")(
                              <Upload
                                listType="picture-card"
                                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                                // defaultFileList={Object.keys(image).length ? [image] : null}
                                fileList={partnerPhotos}
                                onPreview={this.handlePartnerPreview}
                                onChange={this.handlePartnerImg}
                                onRemove={this.handlePartnerRemove}
                                // beforeUpload={this.beforeUploadInsideImage}
                              >
                                {Object.keys(partnerPhotos).length >= 3
                                  ? null
                                  : uploadButton}
                              </Upload>
                            )}

                            <Modal
                              visible={this.state.PreviewVisible}
                              footer={null}
                              onCancel={this.handleModalCancel}
                            >
                              <img
                                alt="inside page image"
                                style={{ width: "50%" }}
                                src={this.state.PreviewImage}
                              />
                            </Modal>
                          </Form.Item>
                        )}
                      </Col>

                      <Col>
                      {this.state.partnerPhotos.map((val, index) => {
                        return (
                          //  <Row gutter={24}>
                          <>
                          <Col
                            xs={{ span: 24 }}
                            sm={{ span: 24 }}
                            md={{ span: 10 }}
                            lg={{ span: 10 }}
                            style={{ marginTop: 20 }}
                          >
                            <FormItem
                              label={`${index + 1} Partner Name `}
                            >
                              {getFieldDecorator(
                                `partnerName[${index}]`,
                                {
                                  trigger: "onBlur",
                                  valuePropName: "defaultValue",
                                  // initialValue: this.state.collateralData[index]
                                  //   ? this.state.collateralData[index].name
                                  //   : "",
                                  rules: [
                                    {
                                      required: true,
                                      message: "Please enter Parner Name",
                                    },
                                  ],
                                }
                              )(
                                <Input
                                  placeholder="Partner Name"
                                  style={{ width: "100%", marginLeft: "2%" }}
                                />
                              )}
                            </FormItem>
                                </Col>

                                 <Col
                                 xs={{ span: 24 }}
                                 sm={{ span: 24 }}
                                 md={{ span: 10 }}
                                 lg={{ span: 10 }}
                                 style={{ marginTop: 20 }}
                               >
                            <FormItem
                              label={`${index + 1} Partner Position `}
                            >
                              {getFieldDecorator(
                                `partnerPosition[${index}]`,
                                {
                                  trigger: "onBlur",
                                  valuePropName: "defaultValue",
                                  // initialValue: this.state.collateralData[index]
                                  //   ? this.state.collateralData[index].name
                                  //   : "",
                                  rules: [
                                    {
                                      required: true,
                                      message: "Please enter Partner Position",
                                    },
                                  ],
                                }
                              )(
                                <Input
                                  placeholder="Partner Position"
                                  style={{ width: "100%", marginLeft: "2%" }}
                                />
                              )}
                            </FormItem>
                          </Col>
                          </>
                        );
                      })}
                      </Col>
                      {/* <Col>
                     {this.state.partnerPhotos.map((val, index) => {
                        return (
                          //  <Row gutter={24}>
                          <Col
                            xs={{ span: 24 }}
                            sm={{ span: 24 }}
                            md={{ span: 12 }}
                            lg={{ span: 12 }}
                            style={{ marginTop: 20 }}
                          >
                            <FormItem
                              label={`Partner Position ${index + 1}`}
                            >
                              {getFieldDecorator(
                                `partnerPosition[${index}]`,
                                {
                                  trigger: "onBlur",
                                  valuePropName: "defaultValue",
                                  // initialValue: this.state.collateralData[index]
                                  //   ? this.state.collateralData[index].name
                                  //   : "",
                                  rules: [
                                    {
                                      required: true,
                                      message: "Please enter Caption",
                                    },
                                  ],
                                }
                              )(
                                <Input
                                  placeholder="Partner Position"
                                  style={{ width: "40%", marginLeft: "2%" }}
                                />
                              )}
                            </FormItem>
                          </Col>
                        
                        );
                      })}
                      </Col> */}
                    </Row>
                  </div>
                )}

              {/* <div style={{float:"right"}}>
            <Button type="default" onClick={this.messageClose}  style={{background:"#f44336", color:"#fff", float:"right"}}>Cancel</Button>
            <Button type="primary"  style={{background:"#389e0d", color:"#fff", marginBottom: 16, marginRight:8, float:"right"}} loading={this.state.loading} htmlType="submit">Save</Button>
            </div> */}
              {/* <div
                style={{
                  background: "#ECECEC",
                  padding: "30px",
                  marginBottom: "20px",
                }}
              >
                <Card
                  title={
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <div>Create Site</div>
                      {!(
                        exhibitorData.jewExpo != null &&
                        exhibitorData.jewExpo.length == 2
                      ) && (
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                          }}
                        >
                          <div>
                            <Button
                              type="primary"
                              style={{ background: "#389E0D", color: "#fff" }}
                              disabled={
                                this.state.showPremium == true ||
                                this.state.showBasic == true
                                  ? false
                                  : true
                              }
                              loading={this.state.loading}
                              htmlType="submit"
                            >
                              Create & Save
                            </Button>
                          </div>
                          <div>
                            <Button
                              style={{
                                background: "#F44336",
                                color: "#fff",
                                marginLeft: "5px",
                              }}
                              onClick={this.goBack}
                            >
                              Cancel
                            </Button>
                          </div>
                        </div>
                      )}
                    </div>

                    // <Row>
                    // <Col span={19}>Create Site</Col>
                    // {!(exhibitorData.jewExpo != null && exhibitorData.jewExpo.length == 2)  &&
                    //   <div>
                    //     <Col span={3}>
                    //       <Button  type="primary"  style={{background:"#389e0d", color:"#fff"}} disabled={this.state.showPremium || this.state.showBasic ? false : true} loading={this.state.loading} htmlType="submit">Create & Save</Button>
                    //     </Col>
                    //     <Col span={2}>
                    //       <Button  style={{background:"#f44336", color:"#fff"}} onClick={this.goBack}>Cancel</Button>
                    //     </Col>
                    //   </div>
                    // }
                    // </Row>
                  }
                  bordered={false}
                  style={{ width: "100%", height: "250px" }}
                >
                  <Row gutter={24}>
                    <Col span={12}>
                      <Row style={{ marginBottom: "20px" }}>
                        <Button
                          type="primary"
                          ghost
                          onClick={this.onBasicSite}
                          disabled={
                            basic == "JEWEL_EXPO_BASIC" ||
                            this.state.sType == "JEWEL_EXPO_BASIC"
                              ? true
                              : false
                          }
                        >
                          Create Basic Site
                        </Button>
                        {this.state.showBasic && (
                          <div>
                            <Col
                              xs={{ span: 24 }}
                              sm={{ span: 24 }}
                              md={{ span: 8 }}
                              lg={{ span: 8 }}
                            >
                              <FormItem label="Enter Sub Domain">
                                {getFieldDecorator("basicSubDomain", {
                                  trigger: "onBlur",
                                  valuePropName: "defaultValue",
                                  initialValue: "",
                                  // rules: [{
                                  //     required: true,
                                  //     message: "Please enter Sub domain.",
                                  // }
                                  // ],
                                })(
                                  <Input
                                    placeholder="Sub domain"
                                    disabled={
                                      this.state.sType == "JEWEL_EXPO_BASIC"
                                        ? true
                                        : false
                                    }
                                    onChange={this.setType}
                                    style={{ width: "100%", marginRight: 8 }}
                                  />
                                )}
                              </FormItem>
                            </Col>

                           
                          </div>
                        )}
                        <div style={{ marginTop: "20px" }}>
                          {exhibitorData.jewExpo != null &&
                            exhibitorData.jewExpo.map((val, i) => {
                              if (val.siteType == "JEWEL_EXPO_BASIC") {
                                return (
                                  <Row>
                                    <Col
                                      span={4}
                                      style={{ fontWeight: "bold" }}
                                    >
                                      Basic Site:
                                    </Col>
                                    <Col span={16}>
                                      <a
                                        href={`https://${val.subdomain}.jewelexpo.in/`}
                                        target="_blank"
                                      >
                                        {`${val.subdomain}.jewelexpo.in`}
                                      </a>
                                    </Col>
                                  </Row>
                                );
                              }
                            })}
                        </div>
                      </Row>
                    </Col>
                    <Col span={2}>
                      <Divider
                        type="vertical"
                        style={{ height: "150px", fontWeight: "bold" }}
                      />
                    </Col>
                    <Col span={10}>
                      <Row style={{ marginBottom: "20px" }}>
                        <Button
                          type="primary"
                          ghost
                          onClick={this.onPremium}
                          disabled={
                            premium == "JEWEL_EXPO_PREMIUM" ||
                            this.state.sType == "JEWEL_EXPO_PREMIUM"
                              ? true
                              : false
                          }
                        >
                          Create Premium Site
                        </Button>

                        {this.state.showPremium && (
                          <div>
                            <Col
                              xs={{ span: 24 }}
                              sm={{ span: 24 }}
                              md={{ span: 8 }}
                              lg={{ span: 8 }}
                            >
                              <FormItem label="Enter Sub Domain">
                                {getFieldDecorator("premiumSubDomain", {
                                  trigger: "onBlur",
                                  valuePropName: "defaultValue",
                                  initialValue: "",
                                  // rules: [{
                                  //     required: true,
                                  //     message: "Please enter Sub domain.",
                                  // }
                                  // ],
                                })(
                                  <Input
                                    placeholder="Sub domain"
                                    disabled={
                                      this.state.sType == "JEWEL_EXPO_PREMIUM"
                                        ? true
                                        : false
                                    }
                                    onChange={this.setPreType}
                                    style={{ width: "100%", marginRight: 8 }}
                                  />
                                )}
                              </FormItem>
                            </Col>
                          </div>
                        )}
                        <div style={{ marginTop: "20px" }}>
                          {exhibitorData.jewExpo != null &&
                            exhibitorData.jewExpo.map((val, i) => {
                              if (val.siteType == "JEWEL_EXPO_PREMIUM") {
                                return (
                                  <Row>
                                    <Col
                                      span={5}
                                      style={{ fontWeight: "bold" }}
                                    >
                                      Premium Site:
                                    </Col>
                                    <Col span={16}>
                                      <a
                                        href={`https://${val.subdomain}.jewelexpo.in/`}
                                        target="_blank"
                                      >
                                        {`${val.subdomain}.jewelexpo.in`}
                                      </a>
                                    </Col>
                                  </Row>
                                );
                              }
                            })}
                        </div>
                      </Row>
                    </Col>
                  </Row>
                </Card>
              </div> */}
            </Form>
          </App>
        );
    }
}
const WrapCreateExhibitor = Form.create()(CreateExhibitor);
const wrapExhibitor = compose(
    withApollo,
    graphql(CreateJewelExhibitor,{ name:'createJewelExhibitor' }),
    graphql(UpdateJewelExhibitor,{ name:'updateJewelExhibitor' }),
    graphql(UpdateBasicInfoExhibitor,{ name:'updateBasicInfoExhibitor' }),
    graphql(createDigiExpoExhibitor,{ name:'createDigiExpoExhibitor'}),
    // graphql(CheckSubDomain,{
    //     options: props => ({
    //         variables: {
    //             subdomain:val,
    //           fetchPolicy: 'network-only' 
    //         },
    //         fetchPolicy: "network-only"
    //     })
    // }),
)(WrapCreateExhibitor)
export default wrapExhibitor