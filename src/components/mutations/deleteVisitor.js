import gql from 'graphql-tag'

export default gql`
mutation deleteJewelVisitor( $id: String!){
    deleteJewelVisitor(id: $id)
}
`