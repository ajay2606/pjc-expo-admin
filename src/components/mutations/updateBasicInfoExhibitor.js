import gql from 'graphql-tag'
export default gql`
  mutation updateExhibitorInfo(
    $id: ID!
    $companyName: String!
    $slugCompName: String!
    $ownerName: String!
    $salesHead: String!
    $email: String!
    $compTel: String
    $businessAddress: GenericAddressInput
    $latlng: LatLngInput
    $otherContact: String
    $moreProductDetails: String
    $target_states: [STATES]
    $productsDealingIn: [PRODUCT_PROFILE]
    $productsMelting: [MELTING_PRODUCT_PROFILE]
    $branchesIn: [String]
    $logo: S3ObjectInput
    $exhName: String!
    $enabled: Boolean
    $exhType: EXH_TYPE
    $machinery: [MACHINERY]
    $allied: [ALLIED]
  ) {
    updateExhibitorInfo(
      input: {
        id: $id
        companyName: $companyName
        slugCompName: $slugCompName
        ownerName: $ownerName
        salesHead: $salesHead
        email: $email
        compTel: $compTel
        businessAddress: $businessAddress
        latlng: $latlng
        otherContact: $otherContact
        moreProductDetails: $moreProductDetails
        target_states: $target_states
        productsDealingIn: $productsDealingIn
        productsMelting: $productsMelting
        branchesIn: $branchesIn
        logo: $logo
        exhName: $exhName
        enabled: $enabled
        exhType: $exhType
        machinery: $machinery
        allied: $allied
      }
    ) {
      id
      companyName
      slugCompName
      ownerName
      salesHead
      email
      compTel
      enabled
      businessAddress {
        addressLineOne
        addressLineTwo
        city
        state
        zip
        country
      }
      latlng {
        lat
        lng
      }
      otherContact
      moreProductDetails
      target_states
      productsDealingIn
      productsMelting
      branchesIn
      exhType
      machinery
      allied
      logo {
        bucket
        region
        key
      }
      ijw19 {
        exhName
        status
        activatedOn
      }
      jewExpo {
        siteId
        fqdn
      }
    }
  }
`;
