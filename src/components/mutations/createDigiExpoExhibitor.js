import gql from 'graphql-tag'
export default gql`
mutation createDigiExpoExhibitor(
  $ownMob: String!
  $companyName: String!
  $slugCompName: String!
  $ownerName: String!
  $salesHead: String!
  $email: String!
  $compTel: String
  $businessAddress: GenericAddressInput
  $latlng: LatLngInput
  $otherContact: String
  $moreProductDetails: String
  $target_states: [STATES]
  $productsDealingIn: [PRODUCT_PROFILE]
  $productsMelting: [MELTING_PRODUCT_PROFILE]
  $branchesIn: [String]
  $logo: S3ObjectInput
  $createdAt: Int 
  $exhName: String!
  $status: String! 
  $enabled: Boolean
  $prodLimit: Int	
  $partners: [PartnerInput!]
){
createDigiExpoExhibitor(input:{
  ownMob: $ownMob
  companyName: $companyName
  slugCompName: $slugCompName
  ownerName: $ownerName
  salesHead: $salesHead
  email: $email
  compTel: $compTel
  businessAddress: $businessAddress
  latlng: $latlng
  otherContact: $otherContact
  moreProductDetails: $moreProductDetails
  target_states: $target_states
  productsDealingIn: $productsDealingIn
  productsMelting: $productsMelting
  branchesIn: $branchesIn
  logo: $logo
  createdAt: $createdAt
  exhName: $exhName
  status: $status
  enabled: $enabled
  prodLimit: $prodLimit
  partners: $partners
}){
  companyName
  slugCompName
  ownerName
  salesHead
  email
  compTel
  otherContact
  moreProductDetails
  target_states
  productsDealingIn
  productsMelting
  createdAt
  branchesIn
  partners
  createdAt
    latlng{
    lat
    lng
  }
  businessAddress{
    addressLineOne
    addressLineTwo
    city
    state
    zip
    country
  }
}
}
`