import gql from 'graphql-tag'

export default gql`
  mutation createJewelExhibitor(
    $ownMob: String!
    $companyName: String!
    $ownerName: String!
    $salesHead: String!
    $email: String!
    $compTel: String
    $businessAddress: GenericAddressInput
    $latlng: LatLngInput
    $otherContact: String
    $moreProductDetails: String
    $target_states: [STATES]
    $productsDealingIn: [PRODUCT_PROFILE]
    $productsMelting: [MELTING_PRODUCT_PROFILE]
    $branchesIn: [String]
    $slugCompName: String!
    $subdomain: String!
    $logo: S3ObjectInput
    $siteType: SITE_TYPE!
    $exhType: EXH_TYPE
    $machinery: [MACHINERY]
    $allied: [ALLIED]
  ) {
    createJewelExhibitor(
      input: {
        ownMob: $ownMob
        companyName: $companyName
        ownerName: $ownerName
        salesHead: $salesHead
        email: $email
        compTel: $compTel
        businessAddress: $businessAddress
        latlng: $latlng
        otherContact: $otherContact
        moreProductDetails: $moreProductDetails
        target_states: $target_states
        productsDealingIn: $productsDealingIn
        productsMelting: $productsMelting
        branchesIn: $branchesIn
        slugCompName: $slugCompName
        subdomain: $subdomain
        logo: $logo
        siteType: $siteType
        exhType: $exhType
        machinery: $machinery
        allied: $allied
      }
    ) {
      companyName
      ownerName
      salesHead
      email
      compTel
      otherContact
      moreProductDetails
      target_states
      productsDealingIn
      productsMelting
      branchesIn
      slugCompName
      exhType
      machinery
      allied
      businessAddress {
        addressLineOne
        addressLineTwo
        city
        state
        zip
        country
      }
      latlng {
        lat
        lng
      }
      logo {
        bucket
        region
        key
      }
    }
  }
`;

  