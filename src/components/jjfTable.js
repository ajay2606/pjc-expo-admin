import React from 'react'
import { Query,withApollo,compose,graphql} from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { Table, Input, Icon,Button,Popconfirm,message ,Tooltip} from 'antd'
import ijf19 from '../components/queries/getIJFEhibitor'
import Highlighter from 'react-highlight-words';
import { Link } from 'react-router-dom';
import DeleteExhibitor from '../components/mutations/deleteIjfExhibitor'

class JewelExpoTable extends React.Component{
    
    constructor(props){
        super(props)
        this.state = {
          searchText:'',
          ijfExh:[]
        }
     
    }

    static getDerivedStateFromProps(nextProps){
      if(nextProps.data.loading){
        return false
      }
      return{
        ijfExh:nextProps.data.getIjfJewelExhibitors
      }
    }

    getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({
          setSelectedKeys, selectedKeys, confirm, clearFilters,
        }) => (
          <div style={{ padding: 8 }}>
            <Input
              ref={node => { this.searchInput = node; }}
              placeholder={`Search ${dataIndex}`}
              value={selectedKeys[0]}
              onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
              onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
              style={{ width: 188, marginBottom: 8, display: 'block' }}
            />
            <Button
              type="primary"
              onClick={() => this.handleSearch(selectedKeys, confirm)}
              icon="search"
              size="small"
              style={{ width: 90, marginRight: 8 }}
            >
              Search
            </Button>
            <Button
              onClick={() => this.handleReset(clearFilters)}
              size="small"
              style={{ width: 90 }}
            >
              Reset
            </Button>
          </div>
        ),
        filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: (visible) => {
          if (visible) {
            setTimeout(() => this.searchInput.select());
          }
        },
        render: (text) => (
          <Highlighter
            highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
            searchWords={[this.state.searchText]}
            autoEscape
            textToHighlight={text.toString()}
          />
        ),
    })

       
    handleSearch = (selectedKeys, confirm) => {
        confirm();
        this.setState({ searchText: selectedKeys[0] });
    }

    handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: '' });
    }

    deletePage = (mob) =>{
      let array1 = this.state.ijfExh;
      let i = array1.findIndex(x => x.ownMob == mob)
      array1.splice(i, 1);
      this.setState({
          ijfExh: array1
      })
    }    

    handleDelete = (mob) =>{
      // this.enterLoading()
      this.props.deleteExhibitor({
          variables:{
              mob:`${this.props.opt}::${mob.trim()}`
          }
      })
      .then(({ data }) => {
        this.deletePage(mob)
        message.success('Deleted Successfully')
      })
      .catch(res => {
          console.log(`Catch: handleFormSubmit: error: ${JSON.stringify(res, null, 2)}`)            
      });
    }


    
    render(){
        const Editicon = <span>Edit</span>;
        const Delete  = <span>Delete</span>; 
        const columns = [
            {
              title: 'Company Name',
              key: 'companyName',
              dataIndex:'companyName',
              ...this.getColumnSearchProps('companyName'),
              // render : (data)=>{
              //   return(
              //     <Row>
              //       <Col span={10}>
              //       <img src={process.env.S3_URL + '' + data.logo.key} style={{width:"80px",height:"40px"}} />
              //       </Col>
              //       <div style={{marginTop:"8px"}}>
              //       {data.companyName}
              //       </div>
              //     </Row>
                   
              //   )
              // }
            },
            {
              title: 'Owner Name',
              dataIndex: 'ownerName',
              key: 'ownerName',
              ...this.getColumnSearchProps('ownerName'),
        
            }, {
              title: 'Owner Number',
              dataIndex: 'ownMob',
              key: 'ownMob',
              ...this.getColumnSearchProps('ownMob'),
            },
            {
              title: 'Email',
              dataIndex: 'email',
              key: 'email',
            },
            {
              title:'Action',
              key:'action',
              render: (record)=>{
                  return(
                    <div>

                      {/* <a onClick={()=>this.onView(record)}>
                        View
                      </a>&nbsp;
                      <Divider type="vertical"/> */}
                      <Link to={{pathname:"/jAdminCreateIJFExhibitor",state:{exhibitorData:record}}}>
                      <Tooltip placement="top" title={Editicon}>
                          <Button icon="edit" type="default"  style={{cursor: "pointer", background:"#389e0d", color:"#fff" }}/>
                      </Tooltip>
                      </Link>
                      <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.ownMob)}>
                      <Tooltip placement="top" title={Delete}>
                          <Button icon="delete" type= "danger"  style={{marginLeft:"5px",cursor: "pointer", background:"#ff0000", color:"#fff" }}/>
                      </Tooltip>
                      </Popconfirm>
                   </div>
                  )
              }
            }
          ];
        return(
              <div>
                  <Table 
                  columns={columns}
                  loading={this.props.data.loading}
                  dataSource={this.state.ijfExh}
                  rowKey='id'
                  />
              </div>
        )
    }
}


const wrapIndex =  compose(
    withApollo,
    graphql(ijf19 ,{
        options: props => ({
            variables:{exhName:props.opt},
            fetchPolicy: "network-only"
        })
    }),
    graphql(DeleteExhibitor,{
      name:'deleteExhibitor'
    })
  )(JewelExpoTable)

export default withRouter(wrapIndex)