import React from 'react'
import { Route, BrowserRouter as Router } from "react-router-dom";
import Userlist from './userlist'
import Marchant from './manageMarchant'
import App from '../App'
const Routers =()=>(
    <Router>
        <React.Fragment>
            <Route exact path="/" component={App} />
            <Route path="/userlist" component={Userlist} />
            <Route path="/marchant" component={Marchant} />
        </React.Fragment>
    </Router>
);
export default Routers