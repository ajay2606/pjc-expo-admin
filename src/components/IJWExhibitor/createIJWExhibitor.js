import React from 'react'
import App from '../../App'
import { Form, Button, Spin, Switch, Alert, Breadcrumb, Row,Col ,Icon, Table, Input, message, Select, Modal,Radio,Upload, Card, Divider} from 'antd';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import {  graphql, compose, withApollo } from 'react-apollo';
import MapSearchBox from '../mapSearchBox'
import createDigiExpoExhibitor from '../../components/mutations/createDigiExpoExhibitor'
import UpdateIJWExhibitor from '../../components/mutations/updateBasicInfoExhibitor'
import slugify from 'slugify';
const FormItem = Form.Item;
const { TextArea } = Input;
const RadioGroup = Radio.Group;
const bucket = process.env.BUCKET
const s3_Region = process.env.S3_REGION
const S3_Url = process.env.S3_URL;
const country = [
    { value:'IND', label:'IND'},
    { value:'UK', label:'UK'},
    { value:'USA', label:'USA'},
]
const { Option } = Select;

const productDealing = [
   {value:'GOLD_CHAINS', label:'GOLDCHAINS'},
   {value:'MANGALSUTRAS',label:'MANGALSUTRAS'},
    {value:'BANGLES',label:'BANGLES'},
    {value:'BRACELETS',label:'BRACELETS'},
    {value:'CASTING_JEWELLERY',label:'CASTING JEWELLERY'},
    {value:'CZ_DIAMOND_JEWELLERY',label:'CZ DIAMONDJEWELLERY'},
    {value:'ANTIQUE_JEWELLERY',label:'ANTIQUE JEWELLERY'},
    {value:'TEMPLE_JEWELLERY',label:'TEMPLE JEWELLERY'},
    {value:'KOLKATA_JEWELLERY',label:'KOLKATA JEWELLERY'},
    {value:'IMPORTED_ITALIAN_JEWELLERY',label:'IMPORTED ITALIAN JEWELLERY'},
    {value:'LIGHTWEIGHT_JEWELLERY',label:'LIGHTWEIGHT JEWELLERY'},
    {value:'DESIGNER_ANTIQUE_JEWELLERY',label:'DESIGNER ANTIQUE JEWELLERY'},
    {value:'PLAIN_GOLD_JEWELLERY',label:'PLAIN GOLD JEWELLERY'},
    {value:'REAL_DIAMOND_JEWELLERY',label:'REAL DIAMOND JEWELLERY'},
    {value:'PLATINUM_JEWELLERY',label:'PLATINUM JEWELLERY'},
    {value:'SILVER_JEWELLERY_925',label:'SILVER JEWELLERY 925'},
    {value:'SILVER_ARTICLES',label:'SILVER ARTICLES'},
    {value:'LOOSE_DIAMONDS',label:'LOOSE DIAMONDS'},
    {value:'DIAMOND_JEWELLERY',label:'DIAMOND JEWELLERY'},
    {value:'GOLD_JEWELLERY',label:'GOLD JEWELLERY'},
    {value:'JADAU_JEWELLERY',label:'JADAU JEWELLERY'},
    {value:'MACHINERY_ALLIED_SECTION',label:'MACHINERY ALLIED SECTION'},
    {value:'GOLD_MOUNTINGS',label:'GOLD MOUNTINGS'},
    {value:'TURKISH_JEWELLERY',label:'TURKISH JEWELLERY'},
    {value:'NOSE_PINS',label:'NOSE PINS'},
    {value:'MACHINERY',label:'MACHINERY'},
    {value:'TOOLS',label:'TOOLS'},
    {value:'PUBLICATIONS',label:'PUBLICATIONS'},
    {value:'EDUCATION_INSTITUTE',label:'EDUCATION INSTITUTE'},
    {value:'LABORATORY',label:'LABORATORY'}
]


const state = [
    {value:'JAMMU_KASHMIR',label:'JAMMU KASHMIR'},
    {value:'HIMACHAL_PRADESH',label:'HIMACHAL PRADESH'},
    {value:'PUNJAB',label:'PUNJAB'},
    {value:'DELHI',label:'DELHI'},
    {value:'RAJASTHAN',label:'RAJASTHAN'},
    {value:'UTTARAKHAND',label:'UTTARAKHAND'},
    {value:'UTTAR_PRADESH',label:'UTTAR PRADESH'},
    {value:'WEST_BENGAL',label:'WEST BENGAL'},
    {value:'BIHAR',label:'BIHAR'},
    {value:'JHARKHAND',label:'JHARKHAND'},
    {value:'ODISHA',label:'ODISHA'},
    {value:'ASSAM',label:'ASSAM'},
    {value:'ARUNACHAL_PRADESH',label:'ARUNACHAL PRADESH'},
    {value:'MEGHALAYA',label:'MEGHALAYA'},
    {value:'TRIPURA',label:'TRIPURA'},
    {value:'MIZORAM',label:'MIZORAM'},
    {value:'MANIPUR',label:'MANIPUR'},
    {value:'NAGALAND',label:'NAGALAND'},
    {value:'MAHARASHTRA',label:'MAHARASHTRA'},
    {value:'GUJARAT',label:'GUJARAT'},
    {value:'GOA',label:'GOA'},
    {value:'CHHATTISGARH',label:'CHHATTISGARH'},
    {value:'MADHYA_PRADESH',label:'MADHYA PRADESH'},
    {value:'ANDHRA_PRADESH',label:'ANDHRA PRADESH'},
    {value:'HARYANA',label:'HARYANA'},
    {value:'KERALA',label:'KERALA'},
    {value:'SIKKIM',label:'SIKKIM'},
    {value:'TAMIL_NADU',label:'TAMIL NADU'},
    {value:'TELANGANA',label:'TELANGANA'},
    {value:'KARNATAKA',label:'KARNATAKA'},
]

const productMelting = [
    {value:'MELTING_75',label:'MELTING 75'},
    {value:'MELTING_84',label:'MELTING 84'},
    {value:'MELTING_92',label:'MELTING 92'},
    
]
class CreateExhibitor extends React.Component{
    constructor(props){
        super(props);
        this.state={
            visible:false,
            lng: this.props.location.state != undefined && this.props.location.state.exhibitorData!=null &&  this.props.location.state.exhibitorData.latlng != null ?  this.props.location.state.exhibitorData.latlng.lng:'',
            previewVisible: false,
            previewImage: '',
            lat: this.props.location.state != undefined && this.props.location.state.exhibitorData!=null && this.props.location.state.exhibitorData.latlng != null ?  this.props.location.state.exhibitorData.latlng.lat:'',
            loading:false,
            removedsiteLogo: false,
            submit:false,
            domain:"",
            aState:[],
            targetState: [
              {value:'ALL_STATES', label:"ALL STATES"},
              {value:'JAMMU_KASHMIR',label:'JAMMU KASHMIR'},
              {value:'HIMACHAL_PRADESH',label:'HIMACHAL PRADESH'},
              {value:'PUNJAB',label:'PUNJAB'},
              {value:'DELHI',label:'DELHI'},
              {value:'RAJASTHAN',label:'RAJASTHAN'},
              {value:'UTTARAKHAND',label:'UTTARAKHAND'},
              {value:'UTTAR_PRADESH',label:'UTTAR PRADESH'},
              {value:'WEST_BENGAL',label:'WEST BENGAL'},
              {value:'BIHAR',label:'BIHAR'},
              {value:'JHARKHAND',label:'JHARKHAND'},
              {value:'ODISHA',label:'ODISHA'},
              {value:'ASSAM',label:'ASSAM'},
              {value:'ARUNACHAL_PRADESH',label:'ARUNACHAL PRADESH'},
              {value:'MEGHALAYA',label:'MEGHALAYA'},
              {value:'TRIPURA',label:'TRIPURA'},
              {value:'MIZORAM',label:'MIZORAM'},
              {value:'MANIPUR',label:'MANIPUR'},
              {value:'NAGALAND',label:'NAGALAND'},
              {value:'MAHARASHTRA',label:'MAHARASHTRA'},
              {value:'GUJARAT',label:'GUJARAT'},
              {value:'GOA',label:'GOA'},
              {value:'CHHATTISGARH',label:'CHHATTISGARH'},
              {value:'MADHYA_PRADESH',label:'MADHYA PRADESH'},
              {value:'ANDHRA_PRADESH',label:'ANDHRA PRADESH'},
              {value:'HARYANA',label:'HARYANA'},
              {value:'KERALA',label:'KERALA'},
              {value:'SIKKIM',label:'SIKKIM'},
              {value:'TAMIL_NADU',label:'TAMIL NADU'},
              {value:'TELANGANA',label:'TELANGANA'},
              {value:'KARNATAKA',label:'KARNATAKA'},
          ]
          
        }
    }

    
    getLatLong = (position) => {
        this.setState({
            lat:position.lat(),
            lng:position.lng()
        })
    }

      handleRemove = ()=> {
        this.setState({ siteLogo: {}, removedsiteLogo: true });
        // this.props.form.resetFields(['siteLogo'])
        return true;
      }

      handlePreview = (file) => {
        this.setState({
        previewImage: file.url || file.thumbUrl,
        previewVisible: true,
        });
    }
    siteHandleChange = ({ file }) => {
      // this.setState({ siteLogo : file })
      this.setState((prevState, props) => {
        if (
          Object.keys(prevState.siteLogo).length === 0 &&
          prevState.removedsiteLogo === false
        ) {
          return {
            siteLogo: file,
            removedsiteLogo: false
          };
        } else {
          return {
            removedsiteLogo: false
          };
        }
      });
    }
    
    beforeUpload = (file) =>{
        var fr = new FileReader;
  
        fr.onload = () => { // file is loaded
            var img = new Image;
            img.onload = () => {
                
                if (img.width != 600 && img.height != 200) {
  
                  this.setState({
                    favLogoErr:true
                  })
                  this.props.form.setFields({
                    
                    siteLogo:{
                      value:this.state.siteLogo,
                      errors:[new Error('Please Upload image with (600 px * 200 px) dimensions.')]
                    }
                  })
                  return false
                }else{
                  this.setState({
                    favLogoErr:false
                  })
                }
              };
            img.src = fr.result; // is the data URL because called with readAsDataURL
        };
  
        fr.readAsDataURL(file); // I'm using a <input type="file"> for demonstrating
      }
    
    onChangeOption = (e) =>{
        this.setState({value1:e.target.value})
    }

    onBasicSite = ()=>{
      this.setState({showBasic:true,
        
      })
    }
    
    handleOpen = () => this.setState({ visible: !this.state.visible })
    handleCancel = () => this.setState({visible:false})

    enterLoading = () => {
        this.setState({ loading: true});
    };

    // updateLoading = () => {
    //   console.log('in update loading')
    //   this.setState({ pLoading:true});
    // };
    
    messageClose = (val) => {
      this.setState({loading:false,onCreate:true,sType:val})
      this.props.history.push('/jAdminExhibitorList');
    }
    messageUpdate = ()=>{
      this.props.history.push('/jAdminExhibitorList');
      // this.setState({loading:false})
    }

   

    onCancel = ()=>{
      this.props.history.push('/jAdminExhibitorList');
    }

   
    selectChange = (value)=>{
        let allstate =  this.state.targetState
        let states = this.state.targetState
        
        if(value=="ALL_STATES"){
          let allState = states.splice(0,1)
          this.setState({targetState:allState,aState:allstate})
        }else{
          this.setState({aState:allstate})
        }
        
    }

    onDeselect = (value)=>{
      this.setState({targetState:this.state.aState})
    }

   

     handleSubmit = (e) => {
       e.preventDefault();
       this.props.form.validateFields((err, values) => {
        //  if (values.addressOnMap.lat == "" && values.addressOnMap.lng == "") {
        //     this.props.form.setFields({
        //         addressOnMap: {
        //         value: values.addressOnMap,
        //         errors: [new Error("Location is required")]
        //       }
        //     });
        //     return true;
        //   }else{
        //     this.props.form.setFields({
        //         addressOnMap: {
        //         value: values.addressOnMap,
        //         errors:""
        //       }
        //     });
        //   }
            if (!err) {
                values.slugCompName = slugify(values.companyName, {
                    lower: true
                })

                values.addressOnMap = this.state.lat != "" ? {
                  lat:this.state.lat,
                  lng:this.state.lng
                }:undefined

                  if(values.productProfile == ""){
                      delete values.productProfile
                  }

                  if(values.compTel == ""){
                    delete values.compTel
                  }

                
                this.enterLoading()
                if(this.props.location.state && this.props.location.state.exhibitorData && this.props.location.state.exhibitorData.ijw19 != null){
                    // return false
                    this.props.updateIJWExhibitor({
                        variables:{
                        id:values.ownMob.trim(),
                        companyName:values.companyName.trim(),
                        ownerName:values.ownerName.trim(),
                        email:values.email.trim(),
                        salesHead:values.salesHead.trim(),
                        compTel:values.compTel,
                        businessAddress:{
                            addressLineOne:values.addressLineOne,
                            addressLineTwo:values.addressLineTwo,
                            city: values.city,
                            state: values.state,
                            zip: values.pinCode,
                            country: values.countryCode
                        },
                        latlng: values.addressOnMap,
                        target_states:values.stateTarget,
                        otherContact:values.otherContact == "" ? undefined : values.otherContact,
                        productsDealingIn:values.productDealing,
                        moreProductDetails:values.productProfile,
                        slugCompName:values.slugCompName,
                        productsMelting:values.productMelting.length != 0 ? values.productMelting : undefined,
                        branchesIn:values.branchesIn.length != 0 ? values.branchesIn : undefined,
                        exhName:'pjc',
                        logo:undefined
                        }
                    }).then(({data})=>{
                            message.success("Exhibitor updated successfully",this.messageUpdate());
                    }) .catch(res => {
                        console.log(
                            `Catch: handleFormSubmit: error: ${JSON.stringify(res, null, 2)}`
                            );
                        });


                }else{
                    // return false
                    console.log("valuse in create",values)
                    this.props.createDigiExpoExhibitor({
                        variables:{
                        ownMob:values.ownMob.trim(),
                        companyName:values.companyName.trim(),
                        ownerName:values.ownerName.trim(),
                        email:values.email.trim(),
                        salesHead:values.salesHead.trim(),
                        compTel:values.compTel,
                        businessAddress:{
                            addressLineOne:values.addressLineOne,
                            addressLineTwo:values.addressLineTwo,
                            city: values.city,
                            state: values.state,
                            zip: values.pinCode,
                            country: values.countryCode
                        },
                        latlng: values.addressOnMap ,
                        productsDealingIn:values.productDealing,
                        target_states:values.stateTarget,
                        otherContact:values.otherContact == "" ? undefined : values.otherContact,
                        productsMelting:values.productMelting.length != 0 ? values.productMelting : undefined,
                        moreProductDetails:values.productProfile,
                        slugCompName:values.slugCompName,
                        branchesIn:values.branchesIn.length != 0 ? values.branchesIn : undefined,
                        createdAt:undefined,
                        exhName:'pjc',
                        status:"NEW"
                        }
                    }).then(({data})=>{
                            message.success("Exhibitor created successfully",this.messageClose());
                    }) .catch(res => {
                        console.log(
                            `Catch: handleFormSubmit: error: ${JSON.stringify(res, null, 2)}`
                            );
                        });
                }
                }else{
                    console.log("Error", err)
                }
            })
        }


    render(){
      const children = [];
      const targetStateSorted = _.sortBy(this.state.targetState, ['label'])
      const productDealingSorted = _.sortBy(productDealing, ['label'])
      const stateSorted = _.sortBy(state, ['label'])
      let exhibitorData =this.props.location.state != undefined && this.props.location.state.exhibitorData != null ? this.props.location.state.exhibitorData : ""
        const uploadButton = (
            <div>
              <Icon type="plus" />
              <div>Upload</div>
            </div>
          );
        const { getFieldDecorator, getFieldValue } = this.props.form;
        const { siteLogo, previewVisible,previewImage} =this.state
        const formItemLayout = {
          labelCol: { span: 12 },
          wrapperCol: { span: 16 }
      };
      console.log("In Create 5");
        return(
            <App header="Create Exhibitor">
          <Form onSubmit={this.handleSubmit}>
            <Row gutter={16}> 
            <Col xs={{span:24}} sm={{span:24}} md={{span:20}} lg={{span:20}}>
            <Breadcrumb style={{marginBottom:"20px"}}>
            <Link to="/">
                <Breadcrumb.Item>Home</Breadcrumb.Item>
            </Link>
                <Breadcrumb.Item>Create Exhibitor</Breadcrumb.Item>
            </Breadcrumb>
            </Col>
            <Col span={4}>
              <Button type="default"  style={{background:"#389e0d", color:"#fff", marginBottom: 16, marginRight:8}} loading={this.state.loading} htmlType="submit">Save</Button>
              <Button type="default" onClick={this.onCancel}  style={{background:"#f44336", color:"#fff"}}>Cancel</Button>
            </Col>
          {/* <Col xs={{span:24}} sm={{span:24}} md={{span:2}} lg={{span:2}}>
            {basic == "JEWEL_EXPO_BASIC" && premium == "JEWEL_EXPO_PREMIUM" &&  <Button type="primary"  style={{background:"#389e0d", color:"#fff", marginBottom: 16, marginRight:8}}  htmlType="submit">Update</Button>}
          </Col> */}
          </Row>
            <Row gutter={24}>
            <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
            <FormItem label='Owner Name' >
                    {getFieldDecorator('ownerName', {
                        trigger: 'onBlur',
                        valuePropName: 'defaultValue',
                        initialValue:exhibitorData.ownerName,
                        rules: [{
                        required: true,
                        message: "Please enter owner name.",
                        }],
                    })(
                        // <Tooltip
                        //     title="Enter unique product name"
                        //     placement="topLeft"
                        // >
                            <Input placeholder="Owner name" style={{ width: '100%', marginRight: 8 }} />
                        // </Tooltip>
                    )}
            </FormItem>
            </Col>
            <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
            <FormItem label='Company Name' >
                    {getFieldDecorator('companyName', {
                        trigger: 'onBlur',
                        valuePropName: 'defaultValue',
                        initialValue:exhibitorData.companyName,
                        rules: [{
                        required: true,
                        message: "Please enter company name.",
                        }],
                    })(
                        // <Tooltip
                        //     title="Enter unique product name"
                        //     placement="topLeft"
                        // >
                            <Input placeholder="Company name" style={{ width: '100%', marginRight: 8 }} />
                        // </Tooltip>
                    )}
            </FormItem>
            </Col>
            <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
            <FormItem label='Mobile Number' >
                    {getFieldDecorator('ownMob', {
                        trigger: 'onBlur',
                        valuePropName: 'defaultValue',
                        initialValue: this.props.location.state && this.props.location.state.mobileNumber != "" && this.props.location.state.mobileNumber != undefined? this.props.location.state.mobileNumber.trim(): exhibitorData.id,
                        rules: [{
                        required: true,
                        message: "Please enter mobile number.",
                        }],
                    })(
                        // <Tooltip
                        //     title="Enter unique product name"
                        //     placement="topLeft"
                        // >
                            <Input placeholder="Mobile number" disabled style={{ width: '100%', marginRight: 8 }} />
                        // </Tooltip>
                    )}
            </FormItem>
            </Col>
            </Row>
            <Row gutter={24}>
            <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}> 
            <FormItem label='Email Id' >
                    {getFieldDecorator('email', {
                        initialValue:exhibitorData.email,
                        trigger: 'onBlur',
                        valuePropName: 'defaultValue',
                        rules: [{
                        required: true,
                        message: "Please enter email.",
                        }],
                    })(
                        <Input placeholder="Email Id" style={{ width: '100%', marginRight: 8 }} />
                    )}
            </FormItem>
            </Col>
            <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
            <FormItem label='Sales Head' >
                    {getFieldDecorator('salesHead', {
                        trigger: 'onBlur',
                        valuePropName: 'defaultValue',
                        initialValue:exhibitorData.salesHead,
                        rules: [{
                        required: true,
                        message: "Please enter sales head.",
                        }],
                    })(
                        <Input placeholder="Sales head" style={{ width: '100%', marginRight: 8 }} />
                    )}
            </FormItem>
            </Col>
            <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
            <FormItem label='Other Contact Number'  >
                      {getFieldDecorator('otherContact', {
                           initialValue:exhibitorData.otherContact,
                           trigger: 'onBlur',
                           valuePropName: 'defaultValue',
                        //   rules: [{
                        //   required: true,
                        //   message: "Please enter Other contact",
                        //   }],
                      })(
                        <Input placeholder="Other Contact" style={{ width: '100%', marginRight: 8 }} />
                      )}
              </FormItem>
            </Col>
            {/* {this.props.location.state.exhibitorData.jewExpo.map((valr)=>{
              return(
                <FormItem label='Site Type' >
                <RadioGroup onChange={this.onChangeOption} value={valr.siteType}>
                <Radio value={"JEWEL_EXPO_BASIC"}>JEWEL EXPO BASIC</Radio>
                <Radio value={"JEWEL_EXPO_PREMIUM"}>JEWEL EXPO PREMIUM</Radio>
                </RadioGroup>
                </FormItem>
              )})} */}
            {/* <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
            <FormItem label='Site Type' >
                     <RadioGroup onChange={this.onChangeOption} value={this.state.value1}>
                     <Radio value={"JEWEL_EXPO_BASIC"}>JEWEL EXPO BASIC</Radio>
                     <Radio value={"JEWEL_EXPO_PREMIUM"}>JEWEL EXPO PREMIUM</Radio>
                   </RadioGroup>
           </FormItem>
           </Col> */}
           
            </Row>
            <Row gutter={24}>
            {/* <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
            <FormItem label='Sub Domain' >
                    {getFieldDecorator('subdomain', {
                        trigger: 'onBlur',
                        valuePropName: 'defaultValue',
                        initialValue:"",
                        rules: [{
                            required: true,
                            message: "Please enter Sub domain.",
                        }
                        // ,{
                        // validator:(rule, value, cb)=>{
                        //     if(this.state.submit){
                        //       cb('location required')
                        //     }
                        //     cb()
                        // }}
                        ],
                    })(
                        <Input placeholder="Sub domain" style={{ width: '100%', marginRight: 8 }} />
                    )}
            </FormItem>
            </Col>
          */}
            <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
            <FormItem label='Company Tel Number.'  >
                      {getFieldDecorator('compTel', {
                           initialValue:exhibitorData.compTel,
                           trigger: 'onBlur',
                            valuePropName: 'defaultValue',

                        //   rules: [{
                        //   required: true,
                        //   message: "Please enter Company Tel no.",
                        //   }],
                      })(
                        <Input placeholder="Company Tel no" style={{ width: '100%', marginRight: 8 }} />
                      )}
              </FormItem>
            </Col>
            <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
            
            <FormItem label='Product Melting'  >
                      {getFieldDecorator('productMelting', {
                          validateTrigger: ['onChange', 'onBlur'],
                          initialValue:exhibitorData.productsMelting != null ? exhibitorData.productsMelting: [],
                        //   rules: [{
                        //   required: true,
                        //   message: "Please enter product melting.",
                        //   }],
                      })(
                        <Select  
                        mode="multiple"
                        // onChange={this.selectChange}
                        placeholder="Please select">
                          {productMelting.map((c)=>{
                            return(
                              <Option key={c.value} value={c.value} >{c.label}</Option>
                            )
                          })}
                        </Select>
                      )}
              </FormItem> 
            </Col>

             <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
            
            <FormItem label='Branches In'  >
                      {getFieldDecorator('branchesIn', {
                          validateTrigger: ['onChange', 'onBlur'],
                          initialValue:exhibitorData.branchesIn != null ? exhibitorData.branchesIn : []                                                                                                                                                                                                                                                                                                                                           ,
                        //   rules: [{
                        //   required: true,
                        //   message: "Please enter branches in.",
                        //   }],
                      })(
                        <Select  
                        mode="tags"
                        // onChange={this.selectChange}
                        placeholder="Please Input">
                          {children}
                        </Select>
                      )}
              </FormItem> 
            </Col>
           
            
            </Row>
            <Row gutter={24}>
            <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
            <FormItem label='Address Line 1' >
                      {getFieldDecorator('addressLineOne', {
                          trigger: 'onBlur',
                          valuePropName: 'defaultValue',
                          initialValue:exhibitorData.businessAddress!= undefined ?exhibitorData.businessAddress.addressLineOne:"",
                        //   initialValue:this.props.data && this.props.data.footerAddress ?  this.props.data.footerAddress.addressLineOne : "",
                          rules: [{
                          required: true,
                          message: "Please enter Address Line 1",
                          }],
                      })(
                          
                              <TextArea rows={3} placeholder="Address Line 1" />
                      )}
              </FormItem>
              </Col>
              <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
              <FormItem label='Address Line 2' >
                      {getFieldDecorator('addressLineTwo', {
                           trigger: 'onBlur',
                           valuePropName: 'defaultValue',
                          initialValue:exhibitorData.businessAddress!= undefined ?exhibitorData.businessAddress.addressLineTwo:"",
                        //   initialValue:this.props.data && this.props.data.footerAddress ?  this.props.data.footerAddress.addressLineTwo : "",
                          rules: [{
                          required: true,
                          message: "Please enter Address Line 2",
                          }],
                      })(
                          
                            <TextArea rows={3} placeholder="Address Line 2"  />
                      )}
              </FormItem>
              </Col>
              <Col xs={{span:24}} sm={{span:24}} md={{span:4}} lg={{span:4}}>
            
            <FormItem label='PinCode'  >
                      {getFieldDecorator('pinCode', {
                          initialValue:exhibitorData.businessAddress!= undefined ?  exhibitorData.businessAddress.zip :"",
                          trigger: 'onBlur',
                          valuePropName: 'defaultValue',
                          rules: [{
                          required: true,
                          message: "Please enter pincode",
                          }],
                      })(
                        <Input placeholder="PinCode" style={{ width: '100%', marginRight: 8 }} />
                      )}
              </FormItem>
              </Col>
              <Col xs={{span:24}} sm={{span:24}} md={{span:4}} lg={{span:4}}>
              <FormItem label="Address Pin on map" >
                        {
                            getFieldDecorator('addressOnMap', {
                                validateTrigger: ['onChange', 'onBlur'],
                                initialValue: {
                                    lat:this.state.lat,
                                    lng:this.state.lng
                                },
                            //     rules: [
                            //                     {
                            // validator:(rule, value, cb)=>{
                            //     if(this.state.lat == ""){
                            //       cb('location required')
                            //     }
                            //     cb()
                            // }}
                            // ],
                            })(
                                <div>
                                <div  onClick={this.handleOpen} ><img src="https://cdn.pixabay.com/photo/2016/03/22/04/23/map-1272165__340.png" width="40" height="40"/></div>
                            <Modal
                            footer={null}
                             width='200'
                                title="Select your Location on the Map"
                                visible={this.state.visible}
                                onOk={this.handleOpen}
                                onCancel={this.handleCancel}
                                >  <MapSearchBox
                                        handleInputChange={this.handleInputChange}
                                        getLatLong={this.getLatLong}
                                        handleCloseX={this.handleClose}
                                        businessAddress={this.state.businessAddress}
                                        lat={this.state.lat}
                                        lng={this.state.lng}

                                    />
                                
                            </Modal>
                            </div>
                            )
                        }
                    </FormItem>
                    {this.state.lat &&
                    <div style={{color:"green"}}><b>Location Updated...</b></div> 
                  }
                  {this.state.lat == '' &&
                    this.state.error && (
                        <div  style={{ background: 'red' }}>
                            <div  style={{padding: '0px',margin: '0px',color: '#fff' }}>
                                <p className="header">Please select the Location Pin on Map.</p>
                            </div>
                        </div>
                    )}
              </Col> 
            </Row>
            <Row gutter={16}>
                 <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
            <FormItem label='City'>
                      {getFieldDecorator('city', {
                          trigger: 'onBlur',
                          valuePropName: 'defaultValue',
                          initialValue:exhibitorData.businessAddress!= undefined ? exhibitorData.businessAddress.city:"",
                          rules: [{
                          required: true,
                          message: "Please enter city",
                          }],
                      })(
                        <Input placeholder="City" style={{ width: '100%', marginRight: 8 }} />
                      )}
              </FormItem>
              </Col>
              <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
              <FormItem label='State'  >
                      {getFieldDecorator('state', {
                          validateTrigger: ['onChange', 'onBlur'],
                          initialValue:exhibitorData.businessAddress!= undefined ? exhibitorData.businessAddress.state:"MAHARASHTRA",
                          rules: [{
                          required: true,
                          message: "Please enter state",
                          }],
                      })(
                        <Select  
                        placeholder="Please Select">
                          {stateSorted.map((c)=>{
                            return(
                              <Option key={c.value} value={c.value} >{c.label}</Option>
                            )
                          })}
                        </Select>
                      )}
              </FormItem>
            </Col>
            <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
          
              <FormItem label='Country Code'  >
                      {getFieldDecorator('countryCode', {
                          validateTrigger: ['onChange', 'onBlur'],
                          initialValue:exhibitorData.businessAddress!= undefined ? exhibitorData.businessAddress.country:"IND",
                          rules: [{
                          required: true,
                          message: "Please enter country code.",
                          }],
                      })(
                        <Select >
                          {country.map((c)=>{
                            return(
                              <Option key={c.value} value={c.value} >{c.label}</Option>
                            )
                          })}
                        </Select>
                      )}
              </FormItem>
            </Col>
            </Row>
            <Row gutter={16}>
            <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
            
            <FormItem label='Products dealing in'  >
                      {getFieldDecorator('productDealing', {
                          validateTrigger: ['onChange', 'onBlur'],
                          initialValue:exhibitorData.productsDealingIn? exhibitorData.productsDealingIn : [],
                          rules: [{
                          required: true,
                          message: "Please enter products dealing in.",
                          }],
                      })(
                        <Select  
                         mode="multiple"
                        // size={size}
                        placeholder="Please Select">
                          {productDealingSorted.map((c)=>{
                            return(
                              <Option  key={c.value} value={c.value} >{c.label}</Option>
                            )
                          })}
                        </Select>
                      )}
              </FormItem> 
            </Col>
            <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
            
            <FormItem label='Target State'  >
                      {getFieldDecorator('stateTarget', {
                          validateTrigger: ['onChange', 'onBlur'],
                          initialValue:exhibitorData.target_states ? exhibitorData.target_states : [],
                          rules: [{
                          required: true,
                          message: "Please enter target state.",
                          }],
                      })(
                        <Select  
                        mode="tags"
                        // onChange={this.selectChange}
                        // onDeselect={this.onDeselect}
                        placeholder="Please select">
                          {targetStateSorted.map((c)=>{
                            return(
                              <Option key={c.value} value={c.value} >{c.label}</Option>
                            )
                          })}
                        </Select>
                      )}
              </FormItem> 
            </Col>

             <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
              <FormItem label='Add short Product Profile' >
                      {getFieldDecorator('productProfile', {
                          trigger: 'onBlur',
                          valuePropName: 'defaultValue',
                          initialValue:exhibitorData.moreProductDetails,
                      })(
                            <TextArea rows={3} placeholder="Product Profile"  />
                      )}
              </FormItem>
              </Col>
           
           
              </Row>
              {/* <Row gutter={16}>
                <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
                <FormItem label="Site Logo (Upload image with (600 px * 200 px) dimensions)">
                    {getFieldDecorator('siteLogo', {
                      validateTrigger: ['onChange','onBlur'],
                      initialValue: siteLogo ? siteLogo : "",
                      rules: [{
                        required: true,
                        message: "Images are required"
                      }]
                    })(
                    <Upload
                      listType="picture-card"
                      data={siteLogo ? siteLogo : ''}
                      // defaultFileList={ [siteLogo]}
                    defaultFileList={Object.keys(siteLogo).length ? [siteLogo] : null}

                      onPreview={this.handlePreview}
                      onChange={this.siteHandleChange}
                      beforeUpload={this.beforeUpload}
                      onRemove={this.handleRemove}
                    >
                    {Object.keys(siteLogo).length != 0 ? null : uploadButton}
                    </Upload>
                    )}
                  
                    <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                    <div>
                    <img id="sitelogo" key="sitelogo" style={{ width: '100%' }} src={previewImage} />
                    </div>
                    </Modal>
                  </FormItem> 
                </Col> */}
                {/* <Col xs={{span:24}} sm={{span:24}} md={{span:16}} lg={{span:16}}>
                    <Row>
                      <Col xs={{span:24}} sm={{span:24}} md={{span:5}} lg={{span:5}}>
                        <Button type="danger" ghost  onClick={this.onBasicSite}>Create Basic Site</Button>
                      </Col>
                      <Col xs={{span:24}} sm={{span:24}} md={{span:11}} lg={{span:11}}>
                      {this.state.showBasic && <FormItem label='Enter Sub Domain' {...formItemLayout} >
                              {getFieldDecorator('subdomain', {
                                  trigger: 'onBlur',
                                  valuePropName: 'defaultValue',
                                  initialValue:"",
                                  rules: [{
                                      required: true,
                                      message: "Please enter Sub domain.",
                                  }
                                  ],
                              })(
                                  <Input placeholder="Sub domain" style={{ width: '60%', marginLeft: 8 }} />
                              )}
                      </FormItem>}
                      </Col>
                    </Row>
                    <div style={{marginTop:"60px"}}>
                      <Button type="danger" ghost>Create Premium Site</Button>
                    </div>
                </Col>
         
                <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
                 
                </Col> */}
              {/* </Row> */}
            
            {/* <div style={{float:"right"}}>
            <Button type="default" onClick={this.messageClose}  style={{background:"#f44336", color:"#fff", float:"right"}}>Cancel</Button>
            <Button type="primary"  style={{background:"#389e0d", color:"#fff", marginBottom: 16, marginRight:8, float:"right"}} loading={this.state.loading} htmlType="submit">Save</Button>
            </div> */}
            </Form>
            </App>
        )
    }
}
const WrapCreateExhibitor = Form.create()(CreateExhibitor);
const wrapExhibitor = compose(
    withApollo,
    graphql(createDigiExpoExhibitor,{
        name:'createDigiExpoExhibitor'
    }),
    graphql(UpdateIJWExhibitor,{
        name:'updateIJWExhibitor'
    }),
    // graphql(CheckSubDomain,{
    //     options: props => ({
    //         variables: {
    //             subdomain:val,
    //           fetchPolicy: 'network-only' 
    //         },
    //         fetchPolicy: "network-only"
    //     })
    // }),
)(WrapCreateExhibitor)
export default wrapExhibitor