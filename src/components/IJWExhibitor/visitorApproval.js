import { Breadcrumb, Button, Col, Modal, Popconfirm, Row, Spin, Table, message , Tooltip } from 'antd';
import { compose, graphql, withApollo } from 'react-apollo';

import DeleteVisitor from '../mutations/deleteVisitor'
import GetVisitorByExh from '../queries/getVisitorsByExh'
import InfiniteScroll from 'react-infinite-scroller';
import { Link } from 'react-router-dom';
import React from 'react'
import VisitorUpdateMutate from '../mutations/approveVisitor'
import moment from 'moment';

let skip = 0
const limit = 10
class VisitorApproval extends React.Component{
    
    constructor(props){
        super(props)
        this.state = {
            visitorsData:this.props.visitorsData,
            loading:false,
            hasMore:true,
            loading:false,
            dataLength:10,
            view:false,
            visdata:{},
            inLoad:false
        }
    }

    enterLoading = ()=>{
        this.setState({
            loading:true
        })
    }
    
    
    handleInfiniteScroll = () => {
        skip = skip + limit 
        // return false
        this.setState({
            loading: true,
        });
        if (limit != this.state.dataLength) {
            skip=0
            this.setState({
                hasMore: false,
                loading: false,
            });
            return;
        }
          this.getData()
      }
    
      getData =  () => {
        this.props.client.query({
            query:GetVisitorByExh,
            variables: {
                exhName:"PJC",
                limits:limit,
                skips: skip,
                status:"NEW",
            }
        }).then(({data})=>{
            console.log("GetVisitorByExh :: ",data)
              const val=this.state.visitorsData.concat(data.getVisitorsByExh)
              this.setState({visitorsData:val,dataLength:data.getVisitorsByExh.length, loading:false})
            
        }).catch(err => {
            this.setState({err: JSON.stringify(err) })
            console.log(`Error : ${JSON.stringify(err)}`)
        })
    }

    
    showModalView = (data) => {
        this.setState({
         view: true,
         visdata:data
        });
    }

    
    handleCancelView = (e) => {
        this.setState({
          view: false,
        });
    }
    
    enterLoad = ()=>{
        this.setState({inLoad:true})
    }
    handleClick = (values) =>{
         console.log('values',values)
        // return false
        this.enterLoad()
        this.props.visitorUpdateMutate({
            variables:{
                userMob:values.id,
                exhName: this.props.opt ? this.props.opt : "ijw19",
                status:"APPROVED"
            } 
        })
        .then(({ data }) => {
            let visitorData = this.state.visitorsData
            let visitor = visitorData.filter((val)=>{
                    return val.id != values.id
           })
           this.setState({visitorsData:visitor,inLoad:false})
            message.success('Visitor Approved')
        })
        .catch(res => {
            console.log(`Catch: handleFormSubmit: error: ${JSON.stringify(res, null, 2)}`)            
        });
    }

    handleDelete = (mob) =>{
        this.props.deleteVisitor({
            variables:{
                // siteId: this.props.customSiteId,
                id:mob
            }
        })
        .then(({ data }) => {
            console.log(`Then: handleFormSubmit: success: ${JSON.stringify(data, null, 2)}`)
            if(data.deleteJewelVisitor){
                let arr = this.state.visitorsData
                let i =  arr.findIndex((val)=>val.id == mob)
                arr.splice(i,1)
                this.setState({visitorsData:arr})
                message.success('Deleted Successfully')
            }
        })
        .catch(res => {
            console.log(`Catch: handleFormSubmit: error: ${JSON.stringify(res, null, 2)}`)            
        });
      }
  
  
    render() {
        console.log("In VA");
        const Editicon = <span>Edit</span>;
        let visitorsData =  this.state.visdata.basicVisitor != undefined ? this.state.visdata.basicVisitor : ""
        const columns = [
            this.props.opt == "ijw19" ? 
            //this.props.opt == "PJC" ? 
            {
              title: 'Date',
              key: 'ijw19',
              dataIndex:'ijw19.activatedOn',
              width:150,
              sorter: (a, b) => {
                  return a.createdAt -  b.createdAt
                },
                render: (date) => {
                  var dateString = moment.unix(date).format("DD/MM/YYYY");
                  return(
                      <span>{dateString}</span>
                  )
              }
            }: {
                title: 'Date',
                key: 'jewExpo',
                dataIndex:'jewExpo.activatedOn',
                width:150,
                sorter: (a, b) => {
                    return a.createdAt -  b.createdAt
                    },
                render: (date) => {
                var dateString = moment.unix(date).format("DD/MM/YYYY");
                return(
                    <span>{dateString}</span>
                )
            }
            },
            {
              title: 'First Name',
              dataIndex: 'basicVisitor.fName',
              key: 'fName',
              width:150,
              // ...this.getColumnSearchPropsName('fName'),
              sorter: (a, b) => {
                return a.fName.localeCompare(
                  b.fName
                );
              },
              },{
                title: 'Last Name',
                dataIndex: 'basicVisitor.lName',
                key: 'lname',
                width:150,

              },
               {
                  title: 'Mobile No',
                  dataIndex: 'id',
                  key: 'mobile',
                  width:150,

                },
                // {
                //   title: 'Address',
                //   children: [{
                //     title: 'City',
                //     dataIndex: 'basicVisitor.address.city',
                //     key: 'city',
                //     sorter: (a, b) => {
                //       return a.city.localeCompare(
                //         b.city
                //       );
                //     },
                //   }, {
                //     title: 'State',
                //     dataIndex: 'basicVisitor.address.state',
                //     key: 'state',
                //     sorter: (a, b) => {
                //       return a.state.localeCompare(
                //         b.state
                //       );
                //     },
                //   }],
                   {
                      title: 'Company Name',
                      dataIndex: 'basicVisitor.companyName',
                      key: 'companyName',
                      width:200,
                    },
                    {
                      title:'City',
                      dataIndex:'basicVisitor.address.city',
                      key:'city',
                      width:120,
                    },
                    {
                      title:'Action',
                      key:'action',
                      width:200,
                      render:(data)=>{
                        return(
                            <div>
                                <Button type="primary" ghost icon="check" style={{background:"#389e0d", marginRight:"10px"}} onClick={()=>this.handleClick(data)} />
                                <Button type="primary" icon="eye" ghost onClick={()=>this.showModalView(data)}/>
                                <Link to={{pathname:"/jAdminCreateJewelVisitor",
                                      state:{visitorData:data,mobileNumber:data.id,opt:this.props.opt}
                                  }}>
                                      <Tooltip placement="top" title={Editicon}>
                                     <Button type="primary" ghost icon="edit" style={{ marginLeft:"10px",cursor: "pointer", background:"#389e0d" }}/>
                                     </Tooltip>
                                </Link>
                                <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(data.id)}>
                                 <Button icon="delete" ghost type= "danger"  style={{marginLeft:"10px",cursor: "pointer", background:"#ff0000" }}/>
                                </Popconfirm>
                            </div>
                        )
                      }
                    }
                    
        ]
        console.log('approval',this.props)
        return(
                <>
                {this.state.inLoad ? <div class="loadingCustom">
                  <Spin size="large"></Spin>
                </div>:""} 
               
                <Row gutter={20} style={{height:'650px', overflow:'auto'}}>
                <InfiniteScroll
                  initialLoad={false}
                  loadMore={this.handleInfiniteScroll}
                  hasMore={!this.state.loading && this.state.hasMore}
                  useWindow={false}
                >
                  <Table columns={columns}
                    loading={this.state.loading}
                    dataSource={this.state.visitorsData}
                    pagination={false}
                    rowKey="id"
                  />
                    </InfiniteScroll>
                    
                <Modal
                title={<div>{visitorsData.email}
                       </div>
                }
                visible={this.state.view}
                width="90%"
                onOk={this.handleOk}
                onCancel={this.handleCancelView}
                footer={null}
              >
              
              <Row>
                <Col span={6}>
                <h3 style={{margin:0,textTransform: 'capitalize',marginBottom:"10px"}}>{visitorsData.fName}&nbsp;{visitorsData.lName}</h3>
                {visitorsData.visitorPhoto != null ?
                <img src={`https://s3.${visitorsData.visitorPhoto.region}.amazonaws.com/${visitorsData.visitorPhoto.bucket}/${visitorsData.visitorPhoto.key}`} width="170px" height="250px"/>:
                <div style={{width:'150px',height:"200px",border: '1px solid #ddd',padding:'43px',textAlign:'center', fontWeight:'bold'}}>No Photo Available</div>
                }
                </Col>
                
                <Col span={8}>
                {visitorsData.docPhoto != null && <div>
                <h3 style={{margin:0,marginTop:'25px'}}>Document Type</h3>
                <div>GST Certificate / Shop Act License / Association Membership Certificate</div>
                <a href={`https://s3.${visitorsData.docPhoto.region}.amazonaws.com/${visitorsData.docPhoto.bucket}/${visitorsData.docPhoto.key}`} target="_blank">Open</a></div>}
                <h3 style={{margin:0,marginTop:'25px'}}>Mobile Number</h3>
                <div>{this.state.visdata.id}</div>
                <h3 style={{margin:0,marginTop:'25px'}}>Company Name</h3>
                <div>{visitorsData.companyName}</div>
                <h3 style={{margin:0,marginTop:'25px'}}>Designation</h3>
                <div>{visitorsData.designation}</div>
                <h3 style={{margin:0,marginTop:'25px'}}>GST No.</h3>
                <div>{visitorsData.gstNo!=null && visitorsData.gstNo!=undefined ?visitorsData.gstNo:'--'}</div>   
                </Col>
                <h3 style={{ margin: 0, marginTop: '25px' }}>Address 1</h3>
                <div>{visitorsData.address != undefined ? visitorsData.address.addressLineOne :""}</div>
                <h3 style={{margin:0,marginTop:'25px'}}>Address 2</h3>
                <div>{visitorsData.address != undefined ? visitorsData.address.addressLineTwo :""}</div>
                <Col span={8}>
                <h3 style={{margin:0,marginTop:'25px'}}>City</h3>
                <div>{visitorsData.address != undefined ? visitorsData.address.city :""}</div>
                <h3 style={{margin:0,marginTop:'25px'}}>State</h3>
                <div>{visitorsData.address != undefined ? visitorsData.address.state :""}</div>
                <h3 style={{margin:0,marginTop:'25px'}}>Products Interested In</h3>
                {this.props.opt == "jewExpo" ?
                
                 <div>
                   { this.state.visdata.jewExpo!=null && this.state.visdata.jewExpo.productsInterestedIn != null && this.state.visdata.jewExpo.productsInterestedIn.length != 0  ?
                    <div style={{width: '200px', height: '80px', overflowY: 'auto'}}>
                    {this.state.visdata.jewExpo!=null && this.state.visdata.jewExpo.productsInterestedIn != null && this.state.visdata.jewExpo.productsInterestedIn.length != 0 && this.state.visdata.jewExpo.productsInterestedIn.map((val, i)=>{
                    return(
                        <div style={{ textTransform: 'capitalize'}}>
                            {val.replace(/(?!^)\_/g, ' ').toLowerCase()}
                            {/* {val.length - 1 != i && ', '} */}
                        </div>
                    )
                    })}</div> 
                    : "--"
                   } 
                   </div>
                   :

                   <div>
                    {this.props.opt == "ijw19" && this.state.visdata.ijw19!=null && this.state.visdata.ijw19.productsInterestedIn != null && this.state.visdata.ijw19.productsInterestedIn.length != 0  ?
                    <div style={{width: '200px', height: '80px', overflowY: 'auto'}}>
                    {this.state.visdata.ijw19!=null && this.state.visdata.ijw19.productsInterestedIn != null && this.state.visdata.ijw19.productsInterestedIn.length != 0 && this.state.visdata.ijw19.productsInterestedIn.map((val, i)=>{
                    return(
                        <div style={{ textTransform: 'capitalize'}}>
                            {val.replace(/(?!^)\_/g, ' ').toLowerCase()}
                        </div>
                    )
                    })}</div> 
                    : "--"
                   } 
                   </div>}
                </Col>
              </Row>
              <Row>
              </Row>
              </Modal>
              </Row>
              </>
        )
    }
}

const wrapIndex =  compose(
withApollo,
graphql(VisitorUpdateMutate,{
    name:'visitorUpdateMutate'
}),
graphql(DeleteVisitor,{
name:'deleteVisitor'
})
)(VisitorApproval)
            
export default  wrapIndex