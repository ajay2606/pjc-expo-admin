import { Breadcrumb, Button, Col, Form, Icon, Input, Popconfirm, Row, Table, message ,Tooltip} from 'antd'
import { Query, compose, graphql, withApollo } from 'react-apollo';

import App from '../jewelAdminApp'
import DeleteSeller from '../../mutations/deleteJewelSalesPerson'
import Highlighter from 'react-highlight-words';
import { Link } from 'react-router-dom';
import React from 'react'
import getSeller from  '../../queries/getSellers'
import moment from 'moment';
import { withRouter } from 'react-router-dom';

class SellerTable extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            searchText:'',
            sellerData:[]
        }
    }

    static getDerivedStateFromProps(nextProps){
      if(nextProps.data.loading){
        return null
      }
      return{
        sellerData:nextProps.data.getJewelSalesPersonsByExh
      }
    }

    getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({
          setSelectedKeys, selectedKeys, confirm, clearFilters,
        }) => (
          <div style={{ padding: 8 }}>
            <Input
              ref={node => { this.searchInput = node; }}
              placeholder={`Search ${dataIndex}`}
              value={selectedKeys[0]}
              onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
              onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
              style={{ width: 188, marginBottom: 8, display: 'block' }}
            />
            <Button
              type="primary"
              onClick={() => this.handleSearch(selectedKeys, confirm)}
              icon="search"
              size="small"
              style={{ width: 90, marginRight: 8 }}
            >
              Search
            </Button>
            <Button
              onClick={() => this.handleReset(clearFilters)}
              size="small"
              style={{ width: 90 }}
            >
              Reset
            </Button>
          </div>
        ),
        filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: (visible) => {
          if (visible) {
            setTimeout(() => this.searchInput.select());
          }
        },
        render: (text) => (
          <Highlighter
            highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
            searchWords={[this.state.searchText]}
            autoEscape
            textToHighlight={text.toString()}
          />
        ),
    })

       
    handleSearch = (selectedKeys, confirm) => {
        confirm();
        this.setState({ searchText: selectedKeys[0] });
    }

    handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: '' });
    }

    deletePage = (id) =>{
      let array1 = this.state.sellerData;
      let i = array1.findIndex(x => x.id == id)
      array1.splice(i, 1);
      this.setState({
          sellerData: array1
      })
    }
 
    handleDelete = (id) =>{
        this.props.deleteSeller({
            variables:{
                // siteId: this.props.customSiteId,
               id:id
            }
        })
        .then(({ data }) => {
            console.log(`Then: handleFormSubmit: success: ${JSON.stringify(data, null, 2)}`)
            if(data.deleteJewelSalesPerson){
                this.deletePage(id)
                message.success('Deleted Successfully')
            }
        })
        .catch(res => {
            console.log(`Catch: handleFormSubmit: error: ${JSON.stringify(res, null, 2)}`)            
        });
    }
    
    render(){
      const Editicon = <span>Edit</span>;
        const columns = [
            {
              title: 'Date',
              dataIndex: 'createdAt',
              key: 'createdAt',
              render:(createdAt)=>{
                var dateString = moment.unix(createdAt).format("DD/MM/YYYY");
                return(
                    <span>{dateString}</span>
                )
              }
            //   ...this.getColumnSearchProps('date'),
  
            }, 
            {
              title: 'Seller Name',
              key: 'name',
              dataIndex:'name',
              ...this.getColumnSearchProps('sellerName'),
            },
            {
              title: 'Mobile Number',
              dataIndex: 'id',
              key: 'id',
            },
            {
              title: 'Status',
              dataIndex: 'status',
              key: 'status',
            //   ...this.getColumnSearchProps('status'),
            },
            {
              title:'Action',
              key:'action',
              render: (data)=>{
                  return(
                    <div>
                     <Link to={{pathname:"/jAdminEditSeller", state:{sellerdata: data}}}>
                     <Tooltip placement="top" title={Editicon}>
                        <Button icon="edit" style={{ cursor: "pointer", background:"#389e0d", color:"#fff" }} type="primary" />
                     </Tooltip>
                      </Link>
                      <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(data.id)}>
                            <Button icon="delete" type= "danger"  style={{marginLeft:"5px", cursor: "pointer", background:"#ff0000 ", color:"#fff"}}/>
                      </Popconfirm>
                   </div>
                  )
              }
            }
          ];
        return(
            <App header={"Manage Team"}>
                 <Form>
                    <Row gutter={16}> 
                        <Col xs={{span:24}} sm={{span:24}} md={{span:23}} lg={{span:23}}>
                            <Breadcrumb style={{marginBottom:"20px"}}>
                            <Link to="/">
                                <Breadcrumb.Item>Home</Breadcrumb.Item>
                            </Link>
                                <Breadcrumb.Item>Seller</Breadcrumb.Item>
                            </Breadcrumb>
                        </Col>
                        <Link to={{pathname:'/jAdminCreateSeller'}}>
                            <Col xs={{span:24}} sm={{span:24}} md={{span:1}} lg={{span:1}}>
                                <Button type="default" style={{ marginBottom: 16, float: "right", clear:"left", cursor: "pointer", background:"#389e0d", color:"#fff" }} >
                                    Create Seller
                                </Button>
                            </Col>
                        </Link>
                    </Row>
                <div>
                    <Table 
                        columns={columns}
                        loading={this.props.data.loading}
                        dataSource={this.state.sellerData}
                        rowKey='id'
                    />
                </div>
                </Form>
            </App>    
        )   
    }
}

const sellerForm  = Form.create()(SellerTable)
const wrapIndex =  compose(
    withApollo,
    graphql(getSeller ,{
        options: props => ({
            variables:{exhName:"ijw19"},
            fetchPolicy: "network-only"
        })
    }),
    graphql(DeleteSeller,{
      name:"deleteSeller"
    })
  )(sellerForm)

export default withRouter(wrapIndex)