import React from 'react'
import App from '../../App'
import {  Table, Select,Spin} from 'antd';
import GetVisitorByExh from '../queries/getVisitorsByExh'
import Visitor from '../IJWExhibitor/visitorApproval'
// import { graphql, compose,withApollo } from 'react-apollo';
import { Query,withApollo} from 'react-apollo';
const exhName = [
  {value:"PJC",label:"PJC"},
    {value:"jewExpo",label:"Jewel Expo"},
    // {value:"ijw19",label:"IJW 19"},
    // {value:"ijf19",label:"IJF 19"},
  ]
let skip = 0
const limit = 10
class IJWJwelVisitor extends React.Component{

    constructor(props){
        super(props)
        this.state={
            selectedValue:"PJC",
            visitorData:[]
        }
    }

    render(){
      const exhNames = (
        <div>
                <b>Select Exhibition:&nbsp;</b>
                <Select
                  // defaultValue={this.props.location.state != undefined && this.props.location.state.exhiName=="ijf19" ? "ijf19" : "jewExpo"}
                  defaultValue={this.props.opt}
                  style={{width:"35%"}}
                  onChange={this.props.onChangeSelect}
                >
                    {exhName.map((c)=>{
                      return(
                        <Select.Option key={c.value} value={c.value} >{c.label}</Select.Option>
                      )
                    })}
                </Select>
        </div>
      );
        return(
          <App  header={'Pending Approval'} compHeader={exhNames}>
          <Query query={GetVisitorByExh} variables={{
            exhName:this.props.opt,
            skips: skip,
            status:"NEW",
            limits: limit}}
            fetchPolicy="network-only">
            {(
              { loading, error, data, refetch }) => {
              if (loading) return(
                <div style={{ textAlign: "center" }}>
                  <Spin size="large"></Spin>
                </div>
              );
              if (error) return `Error!: ${error}`;
                  return(
                    <Visitor visitorsData={data.getVisitorsByExh} opt={this.props.opt}/>
                  )
            }}
           </Query>
           </App>
                
        )
    }
}
export default withApollo(IJWJwelVisitor)