import React from 'react'
import { Query,withApollo,compose,graphql} from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { Input,Form, Row,Col, Breadcrumb, Radio, Button,message} from 'antd'
import EditSalesPerson from '../../mutations/editSalesPerson'
import App from '../jewelAdminApp'
import { Link } from 'react-router-dom';
const RadioGroup = Radio.Group;
const FormItem = Form.Item
class EditSeller extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            searchText:'',
            value1:this.props.location.state.sellerdata.status == "ACTIVE" ?  'ACTIVE' : 'INACTIVE',
            loading:false
        }
    }

    onChangeRadio = (e) => {
        this.setState({
          value1: e.target.value,
        });
    }

    enterLoading = ()=>{
        this.setState({loading:true})
    }

    messageClose = ()=>{
        this.setState({loading:false})
    }

    handleSubmit = (e) => {
    
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
               
                this.enterLoading();
                this.props.editSalesPerson({
                    variables:{
                        id:values.mobileNumber.trim(),
                        name:values.sellerName,
                        status: this.state.value1,
                        exhName: "ijw19"
                    }
                }).then(({data})=>{
                        message.success("Updated Successfully", this.messageClose);
                }) .catch(res => {
                    console.log(
                        `Catch: handleFormSubmit: error: ${JSON.stringify(res, null, 2)}`
                        );
                    });
                }else{
                    console.log("Error", err)
                }
            })
        }
    render(){
        let sellerData = this.props.location.state.sellerdata
        const { getFieldDecorator } = this.props.form;
        return(
            <App header={"Edit Seller"}>
                 <Form onSubmit={this.handleSubmit}>
                    <Row gutter={16}> 
                        <Col xs={{span:24}} sm={{span:24}} md={{span:23}} lg={{span:23}}>
                            <Breadcrumb style={{marginBottom:"20px"}}>
                            <Link to="/">
                                <Breadcrumb.Item>Home</Breadcrumb.Item>
                            </Link>
                                <Breadcrumb.Item>Edit Seller</Breadcrumb.Item>
                            </Breadcrumb>
                        </Col>
                    </Row>
                    <div>
                    <Row>
                        <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
                            <FormItem label='Seller Name'  >
                                {getFieldDecorator('sellerName', {
                                    initialValue:sellerData.name,
                                    trigger: 'onBlur',
                                    valuePropName: 'defaultValue',
                                })(
                                    <Input placeholder="Seller Name" style={{ width: '100%', marginRight: 8 }} />
                                )}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}}>
                            <FormItem label='Mobile Number'  >
                                {getFieldDecorator('mobileNumber', {
                                    initialValue:sellerData.id,
                                    trigger: 'onBlur',
                                    valuePropName: 'defaultValue',
                                })(
                                    <Input placeholder="Mobile Number" disabled style={{ width: '100%', marginRight: 8 }} />
                                )}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row>
                    <FormItem label='Status' >
                        <RadioGroup onChange={this.onChangeRadio} value={this.state.value1}>
                            <Radio value="ACTIVE">Active</Radio>
                            <Radio value="INACTIVE">InActive</Radio>
                        </RadioGroup>
                    </FormItem>
                    </Row>
                    <Row>
                    <Button type="primary"  style={{background:"#389e0d", color:"#fff", marginBottom: 16, marginRight:8}} loading={this.state.loading}  htmlType="submit">Save</Button>
                    </Row>
                    </div>
                </Form>
            </App>    
        )   
    }
}

const sellerForm  = Form.create()(EditSeller)
const wrapIndex =  compose(
    withApollo,
    graphql(EditSalesPerson,{
        name:'editSalesPerson'
    }),
    // graphql(jewExpo ,{
    //     options: props => ({
    //         variables:{exhName:props.opt},
    //         fetchPolicy: "network-only"
    //     })
    // }),
  )(sellerForm)

export default withRouter(wrapIndex)