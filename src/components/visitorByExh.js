import { Breadcrumb, Button, Col, DatePicker, Divider, Form, Icon, Input, Modal, Popconfirm, Row, Select, Table, message } from 'antd';
import { compose, graphql, withApollo } from 'react-apollo';
import _ from 'lodash';
import DeleteVisitor from '../components/mutations/deleteVisitor'
import GetVisitorByExh from '../components/queries/getVisitorsByExh'
import GetVisitorByExhDownload from '../components/queries/downloadVisitorList'
import GetVisitorByMob from '../components/queries/getVisitorByMob'
import Highlighter from 'react-highlight-words';
import InfiniteScroll from 'react-infinite-scroller';
import { Link } from 'react-router-dom';
import PapaParse from 'papaparse';
import React from 'react'
import SearchVisitor from '../components/queries/searchJewExpoVisitor'
import moment from 'moment';
import { withRouter } from 'react-router-dom';

const { RangePicker } = DatePicker

let skip = 0
const limit = 20
let skipd = 0
const limitd = 1000
const { Option } = Select;
const FormItem = Form.Item;

const productDealing = [
  {value:'GOLD_CHAINS', label:'GOLDCHAINS'},
  {value:'MANGALSUTRAS',label:'MANGALSUTRAS'},
   {value:'BANGLES',label:'BANGLES'},
   {value:'BRACELETS',label:'BRACELETS'},
   {value:'CASTING_JEWELLERY',label:'CASTING JEWELLERY'},
   {value:'CZ_DIAMOND_JEWELLERY',label:'CZ DIAMONDJEWELLERY'},
   {value:'ANTIQUE_JEWELLERY',label:'ANTIQUE JEWELLERY'},
   {value:'TEMPLE_JEWELLERY',label:'TEMPLE JEWELLERY'},
   {value:'KOLKATA_JEWELLERY',label:'KOLKATA JEWELLERY'},
   {value:'IMPORTED_ITALIAN_JEWELLERY',label:'IMPORTED ITALIAN JEWELLERY'},
   {value:'LIGHTWEIGHT_JEWELLERY',label:'LIGHTWEIGHT JEWELLERY'},
   {value:'DESIGNER_ANTIQUE_JEWELLERY',label:'DESIGNER ANTIQUE JEWELLERY'},
   {value:'PLAIN_GOLD_JEWELLERY',label:'PLAIN GOLD JEWELLERY'},
   {value:'REAL_DIAMOND_JEWELLERY',label:'REAL DIAMOND JEWELLERY'},
   {value:'PLATINUM_JEWELLERY',label:'PLATINUM JEWELLERY'},
   {value:'SILVER_JEWELLERY_925',label:'SILVER JEWELLERY 925'},
   {value:'SILVER_ARTICLES',label:'SILVER ARTICLES'},
   {value:'LOOSE_DIAMONDS',label:'LOOSE DIAMONDS'},
   {value:'DIAMOND_JEWELLERY',label:'DIAMOND JEWELLERY'},
   {value:'GOLD_JEWELLERY',label:'GOLD JEWELLERY'},
   {value:'JADAU_JEWELLERY',label:'JADAU JEWELLERY'},
   {value:'MACHINERY_ALLIED_SECTION',label:'MACHINERY ALLIED SECTION'},
   {value:'GOLD_MOUNTINGS',label:'GOLD MOUNTINGS'},
   {value:'TURKISH_JEWELLERY',label:'TURKISH JEWELLERY'},
   {value:'NOSE_PINS',label:'NOSE PINS'},
   {value:'MACHINERY',label:'MACHINERY'},
   {value:'TOOLS',label:'TOOLS'},
   {value:'PUBLICATIONS',label:'PUBLICATIONS'},
   {value:'EDUCATION_INSTITUTE',label:'EDUCATION INSTITUTE'},
   {value:'LABORATORY',label:'LABORATORY'}
]
class Visitor extends React.Component{

    constructor(props){
      super(props)
      this.state={
          selectedValue:"jewExpo",
          visitorsData:this.props.visitorsData,
          mobileNumber:'',
          visible:false,
          hasMore:true,
          loading:false,
          dataLength:10,
          validFrom:'',
          validTo:'',
          name:'',
          companyName:"",
          city:"",
          onSearch:false,
          view:false,
          searchData:[],
          searchObj:{ "$and":[{'jewExpo': { $exists: true } },{'jewExpo.status':"APPROVED"}]},
          productInterestedIn:[],
          visdata:{},
          parsedCSV:'',
          show:false,
          load:false,
          mob:'',
          pagination: {},
      }
    }

    static getDerivedStateFromProps(nextProps,nextState){
      if(nextProps.opt == "jewExpo"){
          return {
            searchObj:nextState.searchObj
          }
      }else{
          return {
            searchObj:{ "$and":[{'ijw19': { $exists: true } },{'ijw19.status':"APPROVED"}]}
          }
      }
    }

    onChangeSelect = (val)=>{
      this.setState({selectedValue:val})
    }

    showModal = () => {
      this.setState({
        visible: true,
      });
    }

    showModalView = (data) => {
      this.setState({
       view: true,
       visdata:data
      });
    }

    handleClick = ()=>{
      this.props.client.query({
        query:GetVisitorByMob,
        fetchPolicy: 'network-only' ,
        variables: {
          mob:this.state.mobileNumber.trim(),
          exhName:this.props.opt
        }
      }).then(({data})=>{
          this.props.history.push('/jAdminCreateJewelVisitor',{visitorData:data.getVisitorByMob,mobileNumber:this.state.mobileNumber,opt:this.props.opt})
      }).catch(err => {
          this.setState({err: JSON.stringify(err) })
          console.log(`Error : ${JSON.stringify(err)}`)
      })
    }

    handleCancel = (e) => {
      this.props.form.resetFields(['mob'])
      this.setState({
        visible: false,
      });
    }

    handleChange = (e)=>{
      this.setState({mobileNumber:e.target.value})
    }

    // fetch = async() =>{
    //   console.log('fetch')
    //   let arrData = []
    //   let visData =[]
    //   skip = 0
    //   do{
    //     visData = await this.props.client.query({
    //       query:GetVisitorByExh,
    //       fetchPolicy:'network-only',
    //       variables:{
    //         exhName:this.props.opt,
    //         limits:limit,
    //         skips: skip,
    //         status:"APPROVED",
    //       }
    //     })
    //       arrData.push(visData.data.getVisitorsByExh)
    //       skip=skip+limit
    //       console.log(skip)
    //       let visitorData = _.flattenDeep(arrData) 
    //       this.setState({visitorsData:visitorData,dataLength:data.getVisitorsByExh.length, loading:false})
    // }while(limit == visData.data.getVisitorsByExh.length)
    //   console,log('lim')
    // }

    fetch = (params = {}) => {
      // skip=10
      this.setState({ loading: true });
      this.props.client.query({
          query: GetVisitorByExh,
          variables: {
              limits:limit,
              skips: skip,
              exhName:this.props.opt,
              status:"APPROVED",
          }
  
      }).then(({ data }) => {
        const pagination = { ...this.state.pagination };
        // Read total count from server
        // pagination.total = data.totalCount;
        // pagination.total = data.getVisitorsByExh.length + skip;
      if(data.getVisitorsByExh){
          let values = this.state.visitorsData.concat(data.getVisitorsByExh)
            this.setState({visitorsData:values,
              loading: false,
              pagination,
            })
      }
      });
    };

    
    handleTableChange = (pagination, filters, sorter) => {
      skip = skip + limit 
      const pager = { ...this.state.pagination };
      pager.current = pagination.current;
      this.setState({
        pagination: pager,
        loading: true,
      });
      this.fetch({
        results: pagination.pageSize,
        page: pagination.current,
        sortField: sorter.field,
        sortOrder: sorter.order,
        ...filters,
      });
      this.fetch();
    };

    // componentDidMount(){
    //   skip = 10
    //   this.fetch()
        //   this.props.client.query({
        //     query: GetVisitorByExh,
        //     variables: {
        //         limits:200,
        //         skips:0,
        //         exhName:this.props.opt,
        //         status:"APPROVED",
        //     }
        // })
        // .then(({ data }) => {
        //         if(data.getVisitorsByExh){
        //             this.setState({
        //               visitorsData:data.getBuyerList
        //             })
        //         }
        // })
        // .catch(res => {
        //         console.log(`Catch: handleFormSubmit: error: ${JSON.stringify(res, null, 2)}`)
        // });
    // }

    // handleInfiniteScroll = () => {
    //   skip = skip + limit 
    //   // return false
    //   this.setState({
    //   loading: true,
    //   });
    //   if (limit != this.state.dataLength) {
    //       skip=0
    //       this.setState({
    //           hasMore: false,
    //           loading: false,
    //       });
    //       return;
    //   }
    //     this.getData()
    // }
  
    // getData =  () => {
    //   this.props.client.query({
    //       query:GetVisitorByExh,
    //       variables: {
    //           exhName:this.props.opt,
    //           limits:limit,
    //           skips: skip,
    //           status:"APPROVED",
    //       }
    //   }).then(({data})=>{
    //         const val=this.state.visitorsData.concat(data.getVisitorsByExh)
    //         this.setState({visitorsData:val,dataLength:data.getVisitorsByExh.length, loading:false})
          
    //   }).catch(err => {
    //       this.setState({err: JSON.stringify(err) })
    //       console.log(`Error : ${JSON.stringify(err)}`)
    //   })
    // }

    onValidityDateChange=(date, dateString)=>{
      const epochSec1 = moment(dateString[0], 'YYYY-MM-DD').valueOf()
      const epochSec2 = moment(dateString[1], 'YYYY-MM-DD').valueOf()
      let startDate = new Date(epochSec1).setHours(0, 0, 0)
      let endDate = new Date(epochSec2).setHours(23, 59, 59)
      const sec1 = startDate / 1000
      const sec2 = endDate / 1000
      this.setState({
          validFrom: sec1,
          validTo: sec2,
      })
    }

    enterLoading = ()=>{
      this.setState({
          loading:true
      })
    }

    handleClear = () => {
      this.props.form.resetFields(['name'])
      this.props.form.resetFields(['companyName'])
      this.props.form.resetFields(['city'])
      this.props.form.resetFields(['date'])
      this.props.form.resetFields(['productDealing'])

      this.setState({
        name:'',
        companyName:'',
        city:'',
        validFrom:'',
        productInterestedIn:'',
        onSearch:false,
        visData:[]
      })
    }


    handleSearchQue = (e)=>{
      e.preventDefault();
      this.setState({onSearch:true})
      this.enterLoading()
      let obj = this.state.searchObj;

      if(this.state.companyName != ''){
        obj.$and.push({
          "basicVisitor.companyName":  { "$regex": `.*${this.state.companyName}*.`, $options: "i" } 
        })
      }

      if(this.state.name != ''){
        obj.$and.push({ $or: [ { "basicVisitor.fName": { "$regex": `.*${this.state.name}*.`, $options: "i" }  }, { "basicVisitor.lName": { "$regex": `.*${this.state.name}*.`, $options: "i" } } ] })
      }

      if(this.state.city != ''){
        obj.$and.push({
          "basicVisitor.address.city": { "$regex": `.*${this.state.city}*.`, $options: "i" }
        })
      }

      if(this.state.mob != ''){
        obj.$and.push({
          "_id": { "$regex": `.*${this.state.mob}*.`, $options: "i" }
        })
      }

      if(this.state.validFrom != ''){
        if(this.props.opt == "jewExpo"){
          obj.$and.push({
            "jewExpo.activatedOn":  { $gte: this.state.validFrom ,$lt : this.state.validTo } 
          })
        }else{
          obj.$and.push({
            "ijw19.activatedOn":  { $gte: this.state.validFrom ,$lt : this.state.validTo } 
          })
        }
      }
     
      if(this.state.productInterestedIn.length != 0){
        if(this.props.opt == "jewExpo"){
          obj.$and.push(
            { "jewExpo.productsInterestedIn": { $in: this.state.productInterestedIn } } 
            )
        }else{
          obj.$and.push(
            { "ijw19.productsInterestedIn": { $in: this.state.productInterestedIn } } 
            )
        }
      }

      this.props.client.query({
        query:SearchVisitor,
        fetchPolicy: 'network-only',
        variables: {
           inputs:JSON.stringify(this.state.searchObj),
           exhName: this.props.opt

        }
      }).then(({data})=>{
        let tmp = { "$and":[{"jewExpo": { $exists: true } }]}
        this.setState({
          searchObj: tmp,
          loading:false,
          searchData:data.searchJewExpoVisitors
        })
          // const val=this.state.visitorsData.concat(data.getVisitorsByExh)
          // this.setState({visitorsData:val,dataLength:data.getVisitorsByExh.length, loading:false})
        }).catch(err => {
          this.setState({err: JSON.stringify(err) })
          console.log(`Error : ${JSON.stringify(err)}`)
        })
      }

      handleName = (e) =>{
        this.setState({
          name:e.target.value
        })
      }

      handleCity = (e) =>{
        this.setState({
          city: e.target.value
        })
      }

      handleMobile = (e) =>{
        this.setState({
          mob: e.target.value.trim()
        })
      }

      handleCompany = (e) =>{
        this.setState({
          companyName:e.target.value
        })
      }

      handleProduct=(val)=>{
        this.setState({productInterestedIn:val})
      }

      handleCancelView =()=>{
        this.setState({view:false})
      }

      handleSearch = (selectedKeys, confirm) => {
        confirm();
        this.setState({ searchText: selectedKeys[0] });
      }

      handleReset = (clearFilters) => {
        clearFilters();
        this.setState({ searchText: '' });
      }

      getColumnSearchPropsName = (dataIndex) => ({
        filterDropdown: ({
          setSelectedKeys, selectedKeys, confirm, clearFilters,
        }) => (
          <div style={{ padding: 8 }}>
            <Input
              ref={node => { this.searchInput = node; }}
              placeholder={`Search ${dataIndex}`}
              value={selectedKeys[0]}
              onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
              onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
              style={{ width: 188, marginBottom: 8, display: 'block' }}
            />
            <Button
              type="primary"
              onClick={() => this.handleSearch(selectedKeys, confirm)}
              icon="search"
              size="small"
              style={{ width: 90, marginRight: 8 }}
            >
              Search
            </Button>
            <Button
              onClick={() => this.handleReset(clearFilters)}
              size="small"
              style={{ width: 90 }}
            >
              Reset
            </Button>
          </div>
        ),
        filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: (visible) => {
          if (visible) {
            setTimeout(() => this.searchInput.select());
          }
        },
        render: (text) => (
          <Highlighter
            highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
            searchWords={[this.state.searchText]}
            autoEscape
            textToHighlight={text.toString()}
          />
        ),
      })

      handleDelete = (mob) =>{
        this.props.deleteVisitor({
            variables:{
                // siteId: this.props.customSiteId,
                id:mob
            }
        })
        .then(({ data }) => {
            console.log(`Then: handleFormSubmit: success: ${JSON.stringify(data, null, 2)}`)
            if(data.deleteJewelVisitor){
                let arr = this.state.visitorsData
                let i =  arr.findIndex((val)=>val.id == mob)
                arr.splice(i,1)
                this.setState({visitorsData:arr})
                message.success('Deleted Successfully')
            }
        })
        .catch(res => {
            console.log(`Catch: handleFormSubmit: error: ${JSON.stringify(res, null, 2)}`)            
        });
      }

      parseToCSV = (data) => {
        var arrayObj = data;
        var arr = [];
        
        
        arrayObj.forEach(val => {
          var dateString = this.props.opt=='jewExpo' ? moment.unix(val.jewExpo.activatedOn).format("DD/MM/YYYY") : moment.unix(val.ijw19.activatedOn).format("DD/MM/YYYY") ;
          var regNo = this.props.opt == 'jewExpo' ? val.jewExpo.regNo : val.ijw19.regNo
          let obj = Object.assign({registerNo :regNo ,companyName:val.basicVisitor != null ? val.basicVisitor.companyName : '', firstName:val.basicVisitor != null ? val.basicVisitor.fName : '',lastName:val.basicVisitor != null ? val.basicVisitor.lName : '',city:val.basicVisitor != null && val.basicVisitor.address != null  ? val.basicVisitor.address.city : '' ,mobile:val.id,registeredOn:dateString});
          let values = Object.values(obj)
          arr.push(values)
        });
    
        var data = {
          fields: ["registerNo.","companyName","firstName","lastName","city","mobile","registeredOn"],
          data: arr
        }
    
        try {
          var csv = PapaParse.unparse(data);
          this.setState({
            parsedCSV: csv,
            show:true,
            load:false
          },()=>{
            document.getElementById('down').click()
          })
    
        } catch (error) {
          console.log('err:', error.message)
        }
      }
      
      enterLoad = ()=>{
        this.setState({
          load:true
        })
      }

      downloadCsv = async()=>{
        this.enterLoad()
        let data = []
        let arrayData = []
        let  skipd = 0
        do{
          data = await this.props.client.query({
            query:GetVisitorByExhDownload,
            fetchPolicy:'network-only',
            variables:{
              exhName:this.props.opt,
              limits:limitd,
              skips: skipd,
              status:"APPROVED",
            }
          })

            arrayData.push(data.data.getVisitorsByExh)
            // return false
            skipd=skipd+limitd
          }while(limitd == data.data.getVisitorsByExh.length)
          let visitorData = _.flattenDeep(arrayData) 
          this.setState({visData:visitorData,btnLoading:false},()=>{
            this.parseToCSV(this.state.visData)
          })
      }


    render(){
      let visitorsData =  this.state.visdata.basicVisitor != undefined ? this.state.visdata.basicVisitor : ""
      const productDealingSorted = _.sortBy(productDealing, ['label'])
      const formItemLayout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 16 }
      };
      const { getFieldDecorator } = this.props.form;
      const columns = [
        this.props.opt == "ijw19" ? 

        {
          title: 'Date',
          key: 'ijw19',
          dataIndex:'ijw19.activatedOn',
          sorter: (a, b) => {
              return a.createdAt -  b.createdAt
            },
            render: (date) => {
              var dateString = moment.unix(date).format("DD/MM/YYYY");
              return(
                  <span>{dateString}</span>
              )
          }
        } : {
          title: 'Date',
          key: 'jewExpo',
          dataIndex:'jewExpo.activatedOn',
          sorter: (a, b) => {
              return a.createdAt -  b.createdAt
            },
            render: (date) => {
              var dateString = moment.unix(date).format("DD/MM/YYYY");
              return(
                  <span>{dateString}</span>
              )
          }
        } ,
        {
          title: 'First Name',
          dataIndex: 'basicVisitor.fName',
          key: 'fName',
          // ...this.getColumnSearchPropsName('fName'),
          sorter: (a, b) => {
            return a.fName.localeCompare(
              b.fName
            );
          },
          },{
            title: 'Last Name',
            dataIndex: 'basicVisitor.lName',
            key: 'lname',
          },
           {
              title: 'Mobile No',
              dataIndex: 'id',
              key: 'mobile',
              // ...this.getColumnSearchPropsName('id'),
            },
            // {
            //   title: 'Address',
            //   children: [{
            //     title: 'City',
            //     dataIndex: 'basicVisitor.address.city',
            //     key: 'city',
            //     sorter: (a, b) => {
            //       return a.city.localeCompare(
            //         b.city
            //       );
            //     },
            //   }, {
            //     title: 'State',
            //     dataIndex: 'basicVisitor.address.state',
            //     key: 'state',
            //     sorter: (a, b) => {
            //       return a.state.localeCompare(
            //         b.state
            //       );
            //     },
            //   }],
               {
                  title: 'Company Name',
                  dataIndex: 'basicVisitor.companyName',
                  key: 'companyName',
                },
                {
                  title:'City',
                  dataIndex:'basicVisitor.address.city',
                  key:'city'
                },
                {
                  title:'Action',
                  key:'action',
                  render:(data)=>{
                    return(
                      <>
                        <Button type="primary" ghost icon="eye" onClick={()=>this.showModalView(data)}/>
                          <Link to={{pathname:"/jAdminCreateJewelVisitor",
                                      state:{visitorData:data,mobileNumber:data.id,opt:this.props.opt}
                                  }}>
                          <Button type="primary" ghost icon="edit" style={{ marginLeft:"10px",cursor: "pointer", background:"#389e0d" }}/>
                        </Link>
                        <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(data.id)}>
                          <Button icon="delete" ghost type= "danger"  style={{marginLeft:"10px",cursor: "pointer", background:"#ff0000" }}/>
                        </Popconfirm>
                      </>
                    )
                  }
                }
                
      ]
      
      return(
            <Form onSubmit={this.handleSearchQue}>
              <Row gutter={16}> 
                 <Col xs={{span:24}} sm={{span:24}} md={{span:19}} lg={{span:19}}>
                    <Breadcrumb style={{marginBottom:"20px"}}>
                     <Link to="/">
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                      </Link>
                        <Breadcrumb.Item>Visitor</Breadcrumb.Item>
                    </Breadcrumb>
                   </Col>
                   <Col xs={{span:24}} sm={{span:24}} md={{span:3}} lg={{span:3}}>
                   <Button type="primary" 
                      style={{ background:"#389e0d", color:"#fff" }}
                      loading={this.state.load}
                      onClick={this.downloadCsv}  icon="download" >
                       Download
                    </Button>
                      {/* <Button
                      onClick={this.downloadCsv}
                      icon="download"
                      // type="primary"
                      style={{ background:"#389e0d", color:"#fff",width:'20px' }}
                      /> */}
                   </Col>
                  <Col xs={{span:24}} sm={{span:24}} md={{span:2}} lg={{span:2}}>
                        <Button type="default" onClick={this.showModal} style={{ marginBottom: 16, float: "right", clear:"left", cursor: "pointer", background:"#389e0d", color:"#fff" }} >
                            Create Visitor
                        </Button>
                  </Col>
              </Row>
              {this.state.show && <a id="down"  download='approved-user-data.csv' href={`data:text/csv,${encodeURI(this.state.parsedCSV)}`}></a>}

              <Row gutter={8}>
                <Col span={3}>
                <FormItem label="Enter Name">
                  {getFieldDecorator('name', {
                                  validateTrigger: ['onChange', 'onBlur'],
                                  rules: [
                                  //     {
                                  // required: true,
                                  // message: "Please enter mobile number.",
                                  // },
                                  // {
                                  //     validator:(rule, value, cb)=>{
                                  //         if(isNaN(value.trim()) || value.trim().length != 10){
                                  //           cb('Please enter 10 digit number only')
                                  //         }
                                  //         cb()
                                  // }}
                              ],
                              })(
                    <Input placeholder="Name" onChange={this.handleName} style={{ width: '100%', marginRight: 8 }} />)}
                </FormItem>
                </Col>
                <Col span={4}>
                <FormItem label="Company Name">
                  {getFieldDecorator('companyName', {
                                  validateTrigger: ['onChange', 'onBlur'],
                                  rules: [
                                  //     {
                                  // required: true,
                                  // message: "Please enter mobile number.",
                                  // },
                                  // {
                                  //     validator:(rule, value, cb)=>{
                                  //         if(isNaN(value.trim()) || value.trim().length != 10){
                                  //           cb('Please enter 10 digit number only')
                                  //         }
                                  //         cb()
                                  // }}
                              ],
                              })(
                    <Input placeholder="Company Name" onChange={this.handleCompany} style={{ width: '100%', marginRight: 8 }} />)}
                </FormItem>
                </Col>
                
                <Col span={4}>
                <FormItem
                            label="Select Date (From - To)">
                              {getFieldDecorator(`date`, {
                                  rules: [
                                      {
                                          required: false,
                                          message: "Please select Date.",
                                      }
                              ],
                              })(
                                  <RangePicker onChange={this.onValidityDateChange} 
                                  format={'YYYY-MM-DD'} 
                                  // disabledDate={this.disabledDate}
                                  style={{ width: '100%'}}
                                />
                                  // <DateRange getDates={this.getDates}/>
                              )}
                        </FormItem>
                </Col>
                <Col span={3}>
                <FormItem label="City">
                  {getFieldDecorator('city', {
                                  validateTrigger: ['onChange', 'onBlur'],
                                  rules: [
                                  //     {
                                  // required: true,
                                  // message: "Please enter mobile number.",
                                  // },
                                  // {
                                  //     validator:(rule, value, cb)=>{
                                  //         if(isNaN(value.trim()) || value.trim().length != 10){
                                  //           cb('Please enter 10 digit number only')
                                  //         }
                                  //         cb()
                                  // }}
                              ],
                              })(
                    <Input placeholder="City" onChange={this.handleCity} />)}
                </FormItem>
                </Col>
                <Col span={3}>
                <FormItem label="Mobile Number">
                  {getFieldDecorator('mobileNumber', {
                                  validateTrigger: ['onChange', 'onBlur'],
                                  rules: [
                                  //     {
                                  // required: true,
                                  // message: "Please enter mobile number.",
                                  // },
                                  // {
                                  //     validator:(rule, value, cb)=>{
                                  //         if(isNaN(value.trim()) || value.trim().length != 10){
                                  //           cb('Please enter 10 digit number only')
                                  //         }
                                  //         cb()
                                  // }}
                              ],
                              })(
                    <Input placeholder="Mobile Number" onChange={this.handleMobile} />)}
                </FormItem>
                </Col>
                <Col span={4}>
                <FormItem label='Products Dealing In'  >
                      {getFieldDecorator('productDealing', {
                          validateTrigger: ['onChange', 'onBlur'],
                        //   rules: [{
                        //   required: true,
                        //   message: "Please enter products dealing in.",
                        //   }],
                      })(
                        <Select  
                          mode="multiple"
                          optionFilterProp="children"
                          filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                          placeholder="Please select"
                          onChange={this.handleProduct}
                          style={{ width: '100%',marginRight:"20px"}}
                          >
                          {productDealingSorted.map((c)=>{
                            return(
                              <Option  key={c.value} value={c.value} >{c.label}</Option>
                            )
                          })}
                        </Select>
                      )}
                </FormItem> 
                </Col>
                <Col span={1}>
                        <Button type="primary" ghost htmlType="submit"  style={{marginTop:"40px"}}>Search</Button>
                </Col>
                <Col span={1}>
                        <Button type="danger" ghost onClick={this.handleClear}  style={{marginTop:"40px",marginLeft:"30px"}}>Clear</Button>
                </Col>
              </Row>
              <Modal
                title={<span>{this.state.visdata != null && this.state.visdata.ijw19 != null ? <span> Reg No: {this.state.visdata.ijw19.regNo} </span> : ''} &nbsp;Email: {visitorsData.email}
                       </span>
              }
                visible={this.state.view}
                width="90%"
                onOk={this.handleOk}
                onCancel={this.handleCancelView}
                footer={null}
              >
              <Row>
                <Col span={6}>
                <h3 style={{margin:0,textTransform: 'capitalize',marginBottom:"10px"}}>{visitorsData.fName}&nbsp;{visitorsData.lName}</h3>
                {visitorsData.visitorPhoto != null ?
                <img src={`https://s3.${visitorsData.visitorPhoto.region}.amazonaws.com/${visitorsData.visitorPhoto.bucket}/${visitorsData.visitorPhoto.key}`} width="170px" height="250px"/>:
                <div style={{width:'150px',height:"200px",border: '1px solid #ddd',padding:'43px',textAlign:'center', fontWeight:'bold'}}>No Photo Available</div>
                }
                </Col>
                <Col span={8}>
                {visitorsData.docPhoto != null && <div>
                <h3 style={{margin:0,marginTop:'25px'}}>Document Type</h3>
                <div>GST Certificate / Shop Act License / Association Membership Certificate</div>
                <a href={`https://s3.${visitorsData.docPhoto.region}.amazonaws.com/${visitorsData.docPhoto.bucket}/${visitorsData.docPhoto.key}`} target="_blank">Open</a></div>}
                <h3 style={{margin:0,marginTop:'25px'}}>Mobile Number</h3>
                <div>{this.state.visdata.id}</div>
                <h3 style={{margin:0,marginTop:'25px'}}>Company Name</h3>
                <div>{visitorsData.companyName}</div>
                <h3 style={{margin:0,marginTop:'25px'}}>Designation</h3>
                <div>{visitorsData.designation}</div>
                <h3 style={{margin:0,marginTop:'25px'}}>GST No.</h3>
                <div>{visitorsData.gstNo!=null && visitorsData.gstNo!=undefined ?visitorsData.gstNo:'--'}</div>
                <h3 style={{margin:0, marginTop:'25px'}}>Address 1</h3>
                <div>{visitorsData.address != undefined ? visitorsData.address.addressLineOne :""}</div>
                </Col>
                <Col span={8}>
                <h3 style={{margin:0,marginTop:'25px'}}>Address 2</h3>
                <div>{visitorsData.address != undefined && visitorsData.address.addressLineTwo != null ? visitorsData.address.addressLineTwo :"--"}</div>
                <h3 style={{margin:0,marginTop:'25px'}}>City</h3>
                <div>{visitorsData.address != undefined ? visitorsData.address.city :""}</div>
                <h3 style={{margin:0,marginTop:'25px'}}>State</h3>
                <div>{visitorsData.address != undefined ? visitorsData.address.state :""}</div>
                {/* {} */}


                <h3 style={{margin:0,marginTop:'25px'}}>Products Interested In</h3>
                {this.props.opt == "jewExpo" ?
                
                 <div>
                   { this.state.visdata.jewExpo!=null && this.state.visdata.jewExpo.productsInterestedIn != null && this.state.visdata.jewExpo.productsInterestedIn.length != 0  ?
                    <div style={{width: '200px', height: '80px', overflowY: 'auto'}}>
                    {this.state.visdata.jewExpo!=null && this.state.visdata.jewExpo.productsInterestedIn != null && this.state.visdata.jewExpo.productsInterestedIn.length != 0 && this.state.visdata.jewExpo.productsInterestedIn.map((val, i)=>{
                    return(
                        <div style={{ textTransform: 'capitalize'}}>
                            {val.replace(/(?!^)\_/g, ' ').toLowerCase()}
                            {/* {val.length - 1 != i && ', '} */}
                        </div>
                    )
                    })}</div> 
                    : "--"
                   } 
                   </div>
                   :

                   <div>
                    {this.props.opt == "ijw19" && this.state.visdata.ijw19!=null && this.state.visdata.ijw19.productsInterestedIn != null && this.state.visdata.ijw19.productsInterestedIn.length != 0  ?
                    <div style={{width: '200px', height: '80px', overflowY: 'auto'}}>
                    {this.state.visdata.ijw19!=null && this.state.visdata.ijw19.productsInterestedIn != null && this.state.visdata.ijw19.productsInterestedIn.length != 0 && this.state.visdata.ijw19.productsInterestedIn.map((val, i)=>{
                    return(
                        <div style={{ textTransform: 'capitalize'}}>
                            {val.replace(/(?!^)\_/g, ' ').toLowerCase()}
                        </div>
                    )
                    })}</div> 
                    : "--"
                   } 
                   </div>}
                </Col>
              </Row>
              <Row>
              </Row>
              </Modal>
              <Modal
                visible={this.state.visible}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={null}
              >
                <FormItem label="Mobile Number" {...formItemLayout} >
                {getFieldDecorator('mob', {
                                validateTrigger: ['onChange', 'onBlur'],
                                rules: [
                                //     {
                                // required: true,
                                // message: "Please enter mobile number.",
                                // },
                                {
                                    validator:(rule, value, cb)=>{
                                      if(value != undefined){
                                        if(isNaN(value.trim()) || value.trim().length != 10){
                                          cb('Please enter 10 digit number only')
                                        }
                                        cb()
                                      }
                                    }}
                            ],
                            })(
                  <Input placeholder="Mobile Number" onChange={this.handleChange} style={{ width: '70%', marginRight: 8 }} />)}
                </FormItem>
                <div> 
                  <Row>
                    <Col  offset ={16} xs={{span:24}} sm={{span:24}} md={{span:4}} lg={{span:4}}>
                    {/* <Link to={{pathname:"/jAdminCreateExhibitor",state:{exihibitorData:this.state.exihibitorData}}} > */}
                      <Button style={{marginRight:"8px",background:"#389e0d", color:"#fff"}} disabled={this.state.mobileNumber.length != 10 ? true :false} onClick={this.handleClick} type="default">Check</Button>
                      {/* </Link> */}
                    </Col>
                    <Col  xs={{span:24}} sm={{span:24}} md={{span:2}} lg={{span:2}}>
                      <Button type="default" onClick={this.handleCancel}  style={{background:"#f44336", color:"#fff"}}>Cancel</Button>
                    </Col>
                  </Row>
                </div>
              </Modal>
              {this.state.onSearch ? 
                <Table columns={columns} 
                dataSource={this.state.searchData}
                pagination={false}
                loading={this.state.loading}
                rowKey="id"
              />
              :
              // <Row gutter={20} style={{height:'550px', overflow:'auto'}}>
              //   <InfiniteScroll
              //     initialLoad={false}
              //     loadMore={this.handleInfiniteScroll}
              //     hasMore={!this.state.loading && this.state.hasMore}
              //     useWindow={false}
              //   >
                  <Table columns={columns}
                    loading={this.state.loading}
                    dataSource={this.state.visitorsData}
                    pagination={this.state.pagination}
                    rowKey="id"
                    onChange={this.handleTableChange}
                  />
              //   </InfiniteScroll>
              // </Row>}
              }
              {/* <Query query={GetVisitorByExh} variables={ { exhName:this.props.opt} } fetchPolicy="network-only" >
                {(
                  { loading, error, data, refetch }) => {
                  if (loading) return(
                    <div style={{ textAlign: "center" }}>
                      <Spin size="large"></Spin>
                    </div>
                  );
                  if (error) return `Error!: ${error}`;
                      return( */}
                        {/* <Table columns={columns} bordered 
                        dataSource={this.state.visitorsData}
                        pagination={false}
                        size="middle" /> */}
                      {/* )
                }}
              </Query> */}
              </Form>
        )
    }
}
const WrapVisitorList = Form.create()(Visitor);
const WrapVisitor = compose(
  withApollo,
  graphql(DeleteVisitor,{
      name:'deleteVisitor'
  })
)(WrapVisitorList)
export default withRouter(WrapVisitor)