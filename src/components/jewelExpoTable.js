import React from 'react'
import { Query,withApollo,compose,graphql} from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { Table,Button,Icon,Input , Tooltip ,Switch ,message} from 'antd'
import jewExpo from '../components/queries/getExhibitorList'
import Highlighter from 'react-highlight-words';
import { Link } from 'react-router-dom';
import UpdateBasicInfoExhibitor from '../components/mutations/updateBasicInfoExhibitor'


class JewelExpoTable extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            searchText:'',
            dataSource:this.props.data.getJewelExhibitors
        }
    }

    getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({
          setSelectedKeys, selectedKeys, confirm, clearFilters,
        }) => (
          <div style={{ padding: 8 }}>
            <Input
              ref={node => { this.searchInput = node; }}
              placeholder={`Search ${dataIndex}`}
              value={selectedKeys[0]}
              onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
              onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
              style={{ width: 188, marginBottom: 8, display: 'block' }}
            />
            <Button
              type="primary"
              onClick={() => this.handleSearch(selectedKeys, confirm)}
              icon="search"
              size="small"
              style={{ width: 90, marginRight: 8 }}
            >
              Search
            </Button>
            <Button
              onClick={() => this.handleReset(clearFilters)}
              size="small"
              style={{ width: 90 }}
            >
              Reset
            </Button>
          </div>
        ),
        filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: (visible) => {
          if (visible) {
            setTimeout(() => this.searchInput.select());
          }
        },
        render: (text) => (
          <Highlighter
            highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
            searchWords={[this.state.searchText]}
            autoEscape
            textToHighlight={text.toString()}
          />
        ),
    })

       
    handleSearch = (selectedKeys, confirm) => {
        confirm();
        this.setState({ searchText: selectedKeys[0] });
    }

    handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: '' });
    }

    submitMut = (e,record) =>{
       console.log('record',e,record)
      // record.businessAddress.map((a)=>{
        delete record.businessAddress['__typename']
        if(record.latlng!=null){
          delete record.latlng['__typename']
        }
        if(record.logo!=null){
          delete record.logo['__typename']
        }
      //})
      if (record.exhType == "JEWELLER") {
        console.log("In If JEWELLER",record.productsDealingIn)
        this.props.updateBasicInfoExhibitor({
          variables: {
            id: record.id,
            //exhType: record.exhType,
            companyName: record.companyName,
            ownerName: record.ownerName,
            email: record.email,
            salesHead: record.salesHead,
            compTel: record.compTel == "" ? undefined : record.compTel,
            businessAddress: record.businessAddress,
            enabled: e,
            // {
            //         addressLineOne:record.addressLineOne,
            //         addressLineTwo:record.addressLineTwo == null ? null : record.addressLineTwo,
            //         city: record.city,
            //         state: record.state,
            //         zip: record.pinCode,
            //         country: record.countryCode
            //     },
            latlng: record.latlng,
            target_states: record.target_states,
            otherContact: record.otherContact == "" ? undefined : record.otherContact,
            productsDealingIn: record.productsDealingIn != null && record.productsDealingIn != 0 ? record.productsDealingIn : undefined,
            //machinery: record.productsDealingIn,
            //allied: record.productsDealingIn,
            moreProductDetails: record.moreProductDetails,
            slugCompName: record.slugCompName,
            productsMelting: record.productMelting != null && record.productMelting.length != 0 ? record.productMelting : undefined,
            branchesIn: record.branchesIn != null && record.branchesIn.length != 0 ? record.branchesIn : undefined,
            exhName: "PJC",
            logo: record.logo
          }
        }).then(({ data }) => {
           console.log('dataVal',data)
           this.setState({loading:false})
          let dataVal = [...this.props.data.getJewelExhibitors]
          if (data.updateExhibitorInfo) {
            dataVal.forEach(vals => {
              if (
                vals.id == record.id
              ) {
                 //console.log('vals.id',vals.id)
                vals['enabled'] = e
              }
            });
          }
          message.success('Updated Successfully')
          this.setState({ dataSource: dataVal, loading: false })
        }).catch(res => {
          console.log(
            `Catch: handleFormSubmit: error: ${JSON.stringify(res, null, 2)}`
          );
        });
      }
      else if (record.exhType == "MACHINERY") {
        console.log("In If MACHINERY");
        this.props.updateBasicInfoExhibitor({
          variables: {
            id: record.id,
            //exhType: record.exhType,
            companyName: record.companyName,
            ownerName: record.ownerName,
            email: record.email,
            salesHead: record.salesHead,
            compTel: record.compTel == "" ? undefined : record.compTel,
            businessAddress: record.businessAddress,
            enabled: e,
            // {
            //         addressLineOne:record.addressLineOne,
            //         addressLineTwo:record.addressLineTwo == null ? null : record.addressLineTwo,
            //         city: record.city,
            //         state: record.state,
            //         zip: record.pinCode,
            //         country: record.countryCode
            //     },
            latlng: record.latlng,
            target_states: record.target_states,
            otherContact: record.otherContact == "" ? undefined : record.otherContact,
            //productsDealingIn: record.productsDealingIn,
            machinery: record.machinery != null && record.machinery.length != 0 ? record.machinery : undefined,
            //allied: record.productsDealingIn,
            moreProductDetails: record.moreProductDetails,
            slugCompName: record.slugCompName,
            //productsMelting: record.productMelting != null && record.productMelting.length != 0 ? record.productMelting : undefined,
            branchesIn: record.branchesIn != null && record.branchesIn.length != 0 ? record.branchesIn : undefined,
            exhName: "PJC",
            logo: record.logo
          }
        }).then(({ data }) => {
          console.log("In Update props", data);
          this.setState({ loading: false })
          message.success("Exhibitor updated successfully");
        }).catch(res => {
          console.log(
            `Catch: handleFormSubmit: error: ${JSON.stringify(res, null, 2)}`
          );
        });
      }
      else if (record.exhType == "ALLIED") {
        console.log("In If ALLIED");
        this.props.updateBasicInfoExhibitor({
          variables: {
             id: record.id,
            //exhType: record.exhType,
            companyName: record.companyName,
            ownerName: record.ownerName,
            email: record.email,
            salesHead: record.salesHead,
            compTel: record.compTel == "" ? undefined : record.compTel,
            businessAddress: record.businessAddress,
            enabled: e,
            // {
            //         addressLineOne:record.addressLineOne,
            //         addressLineTwo:record.addressLineTwo == null ? null : record.addressLineTwo,
            //         city: record.city,
            //         state: record.state,
            //         zip: record.pinCode,
            //         country: record.countryCode
            //     },
            latlng: record.latlng,
            target_states: record.target_states,
            otherContact: record.otherContact == "" ? undefined : record.otherContact,
            //productsDealingIn: record.productsDealingIn,
            //machinery: record.machinery != null && record.machinery.length != 0 ? record.machinery : undefined,
            allied: record.allied !=null && record.allied.length != 0 ? record.allied : undefined,
            moreProductDetails: record.moreProductDetails,
            slugCompName: record.slugCompName,
            //productsMelting: record.productMelting != null && record.productMelting.length != 0 ? record.productMelting : undefined,
            branchesIn: record.branchesIn != null && record.branchesIn.length != 0 ? record.branchesIn : undefined,
            exhName: "PJC",
            logo: record.logo
          }
        }).then(({ data }) => {
          console.log("In Update props", data);
          this.setState({ loading: false })
          message.success("Exhibitor updated successfully");
        }).catch(res => {
          console.log(
            `Catch: handleFormSubmit: error: ${JSON.stringify(res, null, 2)}`
          );
        });
      }
      else {
        console.log("In ELSE");
        this.props.updateBasicInfoExhibitor({
          variables: {
             id: record.id,
            //exhType: record.exhType,
            companyName: record.companyName,
            ownerName: record.ownerName,
            email: record.email,
            salesHead: record.salesHead,
            compTel: record.compTel == "" ? undefined : record.compTel,
            businessAddress: record.businessAddress,
            enabled: e,
            // {
            //         addressLineOne:record.addressLineOne,
            //         addressLineTwo:record.addressLineTwo == null ? null : record.addressLineTwo,
            //         city: record.city,
            //         state: record.state,
            //         zip: record.pinCode,
            //         country: record.countryCode
            //     },
            latlng: record.latlng,
            target_states: record.target_states,
            otherContact: record.otherContact == "" ? undefined : record.otherContact,
            productsDealingIn: record.productsDealingIn != null && record.productsDealingIn != 0 ? record.productsDealingIn : undefined,
            machinery: record.machinery != null && record.machinery.length != 0 ? record.machinery : undefined,
            allied: record.allied !=null && record.allied.length != 0 ? record.allied : undefined,
            moreProductDetails: record.moreProductDetails,
            slugCompName: record.slugCompName,
            productsMelting: record.productMelting != null && record.productMelting.length != 0 ? record.productMelting : undefined,
            branchesIn: record.branchesIn != null && record.branchesIn.length != 0 ? record.branchesIn : undefined,
            exhName: "PJC",
            logo: record.logo
          }
        }).then(({ data }) => {
          console.log("In Update props", data);
          this.setState({ loading: false })
          message.success("Exhibitor updated successfully");
        }).catch(res => {
          console.log(
            `Catch: handleFormSubmit: error: ${JSON.stringify(res, null, 2)}`
          );
        });
      }
  }

    
    render(){
      // console.log('dataSource',this.state.dataSource)
      const Editicon = <span>Edit</span>;
        const columns = [
            {
              title: 'Company Name',
              key: 'companyName',
              dataIndex:'companyName',
              ...this.getColumnSearchProps('companyName'),
            },
            {
              title: 'Owner Name',
              dataIndex: 'ownerName',
              key: 'ownerName',
              ...this.getColumnSearchProps('ownerName'),
  
            }, 
            {
              title: 'Mobile Number',
              dataIndex: 'id',
              key: 'id',
              ...this.getColumnSearchProps('id'),
            },
            {
              title: 'Email',
              dataIndex: 'email',
              key: 'email',
            },
            {
              title: 'Active/Inactive',
              key:'action',
              // onFilter: (value, record) =>{
              //     return  record.enabled.toString() == value
              //    },
                
              //    filters : [
              //        {text :"Active",value:true},
              //        {text:"InActive", value:false},
              //    ],
              render:(data,i)=>{
                // console.log('daat',data)
                  return(
                       <div style={{ marginTop: 10 }}>
                          <Switch onChange={(e)=>this.submitMut(e,data,data.id)} checked={data.enabled==true? true :false} />
                          {/* <Icon type="bulb" size="large" theme="filled" style={{ marginLeft:"20px",fontSize: '20px', color: data.enabled ? 'green' : 'red'}} /> */}
                      </div>
                  )

              }

          },
            {
              title:'Action',
              key:'action',
              render: (record)=>{
                {/* <a onClick={()=>this.onView(record)}>
                   View
                 </a>&nbsp;
                 <Divider type="vertical"/> */}
                  return(
                    <div>
                    {this.props.opt == "jewExpo" ? 
                     
                    <Link to={{pathname:"/jAdminCreateExhibitor",state:{exhibitorData:record}}}>
                       <Tooltip placement="top" title={Editicon}> 
                         <Button icon="edit" type="default" style={{cursor: "pointer", background:"#389e0d", color:"#fff" }}/>
                      </Tooltip>
                    </Link> :
                     
                     <Link to={{pathname:"/jAdminCreateIJWExh",state:{exhibitorData:record}}}>
                      <Tooltip placement="top" title={Editicon}>  
                     <Button icon="edit" type="default" style={{cursor: "pointer", background:"#389e0d", color:"#fff" }}/>
                    </Tooltip>
                    </Link>}
                   </div>
                  )
              }
            }
          ];
        return(
            <div>
              <Table 
                columns={columns}
                loading={this.props.data.loading}
                dataSource={this.props.data.getJewelExhibitors}
                rowKey='id'
              />
            </div>
        )
    }
}


const wrapIndex =  compose(
    withApollo,
    graphql(UpdateBasicInfoExhibitor,{
      name:'updateBasicInfoExhibitor'
    }),
    graphql(jewExpo ,{
        options: props => ({
            variables:{exhName:"PJC"},
            fetchPolicy: "network-only"
        })
    }),
  )(JewelExpoTable)

export default withRouter(wrapIndex)