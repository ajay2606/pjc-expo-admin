import "../src/assets/custom.min.css";
import "antd-mobile/dist/antd-mobile.min.css"
import { SelectDropContext } from '../src/context/SelectContext';

import { Col, Icon, Layout, Menu, Row } from "antd";

import  { Auth } from "aws-amplify";
import { Link } from "react-router-dom";
import MLayout from "../src/components/m/mLayout"
import MediaQuery from 'react-responsive';
import React from "react";
import { withRouter } from "react-router";

// Amplify.configure(aws_exports);

const { Header, Sider, Content } = Layout;
const text = <span>Admin</span>;

class JewelAdmin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      activemenuKey: 0,
      domain:"",
    };
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  handleSignOut = () => {
    Auth.signOut()
      .then(() => {
        window.location=window.location.origin
        // window.location.reload(true)
      })
      .catch(err => console.log(err));
  };

  static getDerivedStateFromProps(nextProps, nextState) {
    if((nextProps.match.url == "/jAdminVisitorApproval" || (nextProps.match.url == "/jAdminVisitorList"))  && nextProps.opt == "ijf19"){
      nextProps.history.push('/jAdminExhibitorList')
    }
    const getActiveMenuId = JewelAdmin.showMenuSelected(nextProps.match.url);
    return {
      activemenuKey: getActiveMenuId
    };
  }
  static showMenuSelected(url) {
    const pathArr = url.split("/").filter(pathVal => pathVal != "");
    let pathName = pathArr[0];
    let activeKey = "0";

    // if(this.props.opt == "ijw19" && pathName == "jAdminVisitorApproval"){

    // }
    switch (pathName) {
      case undefined:
      activeKey = "1";
      break;
        case "jAdminExhibitorList":
        activeKey = "1";
        break;
        case "jAdminCreateExhibitor":
        activeKey = "1";
        break;
        case "jAdminCreateIJWExh":
        activeKey = "1";
        break;
        case "jAdminCreateIJFExhibitor":
        activeKey = "1";
        break;
        case "jAdminVisitorList":
        activeKey = "2";
        break;
        case "jAdminCreateJewelVisitor":
        activeKey = "2";
        break;
        case "jAdminCreateJewelVisitor":
        activeKey = "2";
        break;
        case "jACouponList":
        activeKey = "3";
        break;
        case "jACouponSearch":
        activeKey = "4";
        break;
        case "jAdminVisitorApproval":
        activeKey = "5";
        break;
        case "jAdminSeller":
        activeKey = "6";
        break;
        case "jAdminEditSeller":
        activeKey = "6";
        break;
        case 'jAdminCreateSeller':
        activeKey = "6";
        break;
      default:
        activeKey = "0";
    }
    return activeKey;
  }

  // reloadPage(){
  //   window.location.reload()
  // }

  // millisToMinutesAndSeconds(millis) {
  //   var minutes = Math.floor(millis / 60000);
  //   var seconds = ((millis % 60000) / 1000).toFixed(0);
  //   return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
  // }

  render() {
    //console.log("props",this.props)
    const { authorized, children } = this.props;
    const { activemenuKey } = this.state;
    return (
      <MediaQuery minDeviceWidth={700}>
      {(matches) => {
        if (matches) {
          return (
      <Layout style={{ height: "100vh", background: "#001529" }}>
        <Sider
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
          style={{ height: "100vh",background:"#001529"}}
        >
          <div
            className="logo"
            style={{
              height: "32px",
              background: "#001529",
              margin: "16px"
            }}
          >{
            this.state.collapsed ? 
          <img src="https://www.recaho.com/img/xyz/logo.png" className="header-logo" style={{width:"30px"}} /> : <img src="https://www.recaho.com/img/recaho2.png" style={{width:"150px",height:"35px"}} />}</div>


          <Menu theme="dark" mode="inline" selectedKeys={[activemenuKey]}>

            {/* <Menu.Item key="5">
                <Link to="/sites" >
                <Icon type="credit-card" />
                  <span className="nav-text">Site Management</span>
                </Link>
              </Menu.Item> */}
            {/* <Menu.Item key="0">
            <Link to="/jADashboard">
            <Icon type="team"/>
              <span className="nav-text">Dashboard</span>
            </Link>
            </Menu.Item> */}
            {/* {(this.props.match.url == "/jAdminVisitorApproval"  && this.props.opt == "ijf19") || (this.props.match.url == "/jAdminVisitorList"  && this.props.opt == "ijf19") ? 
               <Menu.Item key="1">
               <Link to="/jAdminExhibitorList">
               <Icon type="team"/>
                 <span className="nav-text">Exhibitor</span>
               </Link>
               </Menu.Item>
               
               
               : */}
            
            <Menu.Item key="1">
            <Link to="/jAdminExhibitorList">
            <Icon type="team"/>
              <span className="nav-text">Exhibitor</span>
            </Link>
            </Menu.Item>

            {(this.context == "jewExpo" || this.context == "PJC" ) && <Menu.Item key="2">
            <Link to="/jAdminVisitorList">
            <Icon type="team"/>
              <span className="nav-text">Visitors</span>
            </Link>
          </Menu.Item> }
            {this.context == "ijf19" &&  <Menu.Item key="3">
            <Link to="/jACouponList">
            <Icon type="snippets" />
              <span className="nav-text">Coupon</span>
            </Link>
            </Menu.Item>
            }
            {this.context == "ijf19" && <Menu.Item key="4">
            <Link to="/jACouponSearch">
            <Icon type="search" />
              <span className="nav-text">Search Coupon</span>
            </Link>
            </Menu.Item>}
            {(this.context == "jewExpo" || this.context == "PJC") && <Menu.Item key="5">
            <Link to="/jAdminVisitorApproval">
            <Icon type="search" />
              <span className="nav-text">Pending Approval</span>
            </Link>
            </Menu.Item> 
            }
            {this.context == "ijw19" && <Menu.Item key="6">
            <Link to="/jAdminSeller">
            <Icon type="team" />
              <span className="nav-text"> Manage Team</span>
            </Link>
            </Menu.Item>}
            <Menu.Item key="7">
            <Icon type="logout" />
            <span onClick={this.handleSignOut} className="nav-text">Logout</span>
            </Menu.Item>
            
            
          </Menu>
        </Sider>
        <Layout style={{ background: "#fff", height: "100vh" }}>
          <Header
            style={{
              position: "fixed",
              padding: 0,
              zIndex: 1,
              width: "100%",
              background: "#fff",
              borderBottom: "12px solid #f0f2f5"
            }}
          >
            <Row gutter={16}>
              <Col span={1}>
                <Icon
                  style={{
                    fontSize: "18px",
                    paddingLeft: "18px",
                    lineHeight: "64px",
                    cursor: "pointer",
                    transition: "color .3s"
                  }}
                  type={this.state.collapsed ? "menu-unfold" : "menu-fold"}
                  onClick={this.toggle}
                />
              </Col>              
                  <Col span={6} >
                    <h2>{this.props.header}</h2>
                  </Col>
              <Col span={6} offset={9}>
                   {this.props.compHeader}
                </Col>
            </Row>
          </Header>

          <Content
            style={{ margin: "50px 16px",marginTop:"80px", background: "#fff" }}
          >
            {children}
            {/* {!authorized &&
              <div>Not authorized</div>
            } */}
          </Content>

        </Layout>
      </Layout>
          )
          } 
          else {
            return (
              <MLayout>
                {children}
              </MLayout>
            );
          }
        }}
      </MediaQuery>
    );
  }
}
JewelAdmin.contextType = SelectDropContext
export default withRouter(JewelAdmin);
